import React, {useContext} from "react";
import './App.css';
import {Link, Redirect, Route, Switch} from "react-router-dom";
import Home from "./pages/Home";
import Team from "./pages/Team";
import Contact from "./pages/Contact";
import FAQ from "./pages/FAQ";

import MapSearch from "./pages/map/MapSearch";
import Details from "./pages/Details";
import {NavigationBar} from "./components/Navbar/NavigationBar";
import Signup from "./pages/userManagement/userLogin/Signup";
import Signin from "./pages/userManagement/userLogin/Signin";
import {firebaseAuth} from "./provider/AuthProvider";
import Terms from "./pages/Terms";
import AccountPage from "./pages/userManagement/AccountPage";
import FormPlace from "./pages/FormPlace";

import EditForm from "./pages/EditForm";
import {ThemeContext, themes} from "./context/ThemeContext";
import SurfaceCalculator from "./pages/SurfaceCalculator";
import LocationForm from "./pages/LocationForm";
import {UserLocationContext} from "./context/UserLocationContext";
import PriceCalculator from "./pages/PriceCalculator";
import {Error404} from "./components/Error404";
import {AdminPage} from "./pages/AdminPage";
import Contract from "./assets/rental_contract/Contract";
import ConfirmationPage from "./pages/ConfirmationPage"

function App() {
    //Contexts
    const themeContext = useContext(ThemeContext);
    const {user} = useContext(firebaseAuth);
    const {latLng} = useContext(UserLocationContext);

    if(user?.role==='administrator'){
        return <AdminPage/>
    }

    return (
        <div className="App">
            <NavigationBar/>
            <header className="App-header" style={{
                backgroundColor: themes[themeContext.theme].background,
                color: themes[themeContext.theme].foreground
            }}>
                <Switch>
                    {/*Public access*/}
                    <Route exact path="/">
                        <Redirect to="/home"/>
                    </Route>
                    <Route path="/home" exact component={Home}/>
                    <Route path="/team" component={Team}/>
                    <Route path="/contact" component={Contact}/>
                    <Route path="/faq" component={FAQ}/>
                    <Route path="/map" component={MapSearch}/>
                    <Route path="/details/:slugPlace" component={Details}/>
                    <Route path='/terms-of-use' component={Terms}/>
                    <Route path='/edit/:slugIdPlace' component={EditForm}/>
                    <Route path='/calculator' component={PriceCalculator}/>
                    <Route path='/surface_calculator' component={SurfaceCalculator}/>
                    <Route path='/locationForm/:slugPlace' component={LocationForm}/>
                    <Route path='/account/my-rentals/contract/:slugPlace' component={Contract}/>
                    <Route path='/confirmation' component={ConfirmationPage}/>

                    {/*Cannot access if connected*/}
                    <Route path='/signin' render={() => !user ? <Signin/> : <Redirect to="/home"/>}/>
                    <Route path='/signup' render={() => !user ? <Signup/> : <Redirect to="/home"/>}/>

                    {/*Restricted*/}
                    <Route path='/account' render={() => !user ? <Redirect to="/signin"/> : <AccountPage/>}/>
                    <Route path="/form" render={() => !user ? <Redirect to="/signin"/> : <FormPlace/>}/>

                    {/*Error 404 is triggered if no above route has match*/}
                    {/*Always place it at the end of the switch*/}
                    <Route component={Error404}/>
                </Switch>

            </header>

            <footer style={{
                backgroundColor: themes[themeContext.theme].navbar,
                color: themes[themeContext.theme].foreground,
                textAlign: "center",
            }}>
                <h6>©2021 - Stock Ici Dev Team</h6>
                <Link
                    style={{paddingRight: "2em"}}
                    to="/terms-of-use"
                >Terms of use
                </Link>
            </footer>

        </div>
    );
}

export default App;
