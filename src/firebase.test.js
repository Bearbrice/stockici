import firebase, { config } from "./firebase";
import React, { useContext, useEffect, useState } from "react";
import { v4 as  uuidv4 } from "uuid"; 
import MapSearch from "../src/pages/map/MapSearch"


const projectId = "stockici-62665"
process.env.REACT_APP_PROJECT_ID = projectId
let app;
let refMyCollection = firebase.firestore().collection("users").doc("NclNlwRzU1R7NAiI4Asp6SKmvoy1");


// initialize app
if (!firebase.apps.length) {
    app = firebase.initializeApp({ projectId })
} else {
    app = firebase.app();
}


describe('Firebase Firestore test', () => {
         
    test('Test if App has been initialize', () => {
        expect(firebase.apps.length).toBeGreaterThan(0);
    });

    test('Test if collection is defined', () => {
        expect(refMyCollection).toBeDefined();
    });
});


