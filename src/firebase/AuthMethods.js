/**
 * Resources
 * https://dev.to/itnext/user-auth-with-firebase-and-react-1725
 */

import firebase, {auth, providers} from "../firebase"
import {deleteUserData} from "../services/StoreMethods";

export const authMethods = {
    deleteAccount: (password, setErrors, photoURL) => {
        const uid = auth.currentUser.uid;
        const credential = firebase.auth.EmailAuthProvider.credential(auth.currentUser.email, password);

        /// 1. Re-authenticate 2. Delete authentication account 3. Delete user data

        // --- 1. Re-authenticate ---
        auth.currentUser.reauthenticateWithCredential(credential)
            .then(async res => {
                console.log(res);

                // --- 2. Delete authentication account ---
                auth.currentUser.delete()
                    .then(async res2 => {
                        console.log(res2)
                        alert.show("Compte supprimé");

                        // --- 3. Delete user data ---
                        deleteUserData(uid, photoURL)
                            .then(async r => alert.show("Données supprimées"))
                            .catch(err => setErrors(prev => ([...prev, err.message])))

                    })
                    .catch(err => setErrors(prev => ([...prev, err.message])))
            }).catch(err => setErrors(prev => ([...prev, err.message])))
    },
    updateEmail: (newEmail, password, setErrors) => {
        //change from create users to...
        const credential = firebase.auth.EmailAuthProvider.credential(auth.currentUser.email, password);
        auth.currentUser.reauthenticateWithCredential(
            credential
        ).then(async res => {
            console.log(res);
            auth.currentUser.updateEmail(newEmail)
                .then(async res2 => {

                    console.log(res2)

                })
                .catch(err => {
                    setErrors(prev => ([...prev, err.message]))
                })
        }).catch(err => {
            setErrors(prev => ([...prev, err.message]))
        })
    },
    signInWithGoogle: (setErrors) => {
        auth.signInWithPopup(providers.googleProvider)
            .then(async res => {
                // const token = await Object.entries(res.user)[5][1].b
                //set token to localStorage
                // await localStorage.setItem('token', token)
                // setToken(token)
                //grab token from local storage and set to state.
                console.log(res)
            })
            .catch(err => {
                setErrors(prev => ([...prev, err.message]))
            })
    },
    // firebase helper methods go here...
    signup: (email, password, setErrors) => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            //make res asynchonous so that we can make grab the token before saving it.
            .then(async res => {
                console.log("RES")
                console.log(res)
            })
            .catch(err => {
                setErrors(prev => ([...prev, err.message]))
            })
    },
    signin: (email, password, setErrors) => {
        //change from create users to...
        firebase.auth().signInWithEmailAndPassword(email, password)
            //everything is almost exactly the same as the function above
            .then(async res => {
                console.log("RES")
                console.log(res);
            })
            .catch(err => {
                setErrors(prev => ([...prev, err.message]))
            })
    },
    //no need for email and password
    signout: (setErrors, setUser) => {
        // signOut is a no argument function
        firebase.auth().signOut().then(res => {
            setUser(null)
        })
            .catch(err => {
                //if there is an error
                setErrors(prev => ([...prev, err.message]))
                console.error(err.message)
            })
    },
}