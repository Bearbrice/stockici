// Team page
import React, {useContext} from "react";
import PersonIcon from '@material-ui/icons/Person';
import {Card, CardContent} from "@material-ui/core";
import {CardSubtitle, CardTitle} from "reactstrap";
import {ThemeContext, themes} from "../context/ThemeContext";
import benoit from "../assets/benoit gremaud.jpg";
import abetare from "../assets/Abetare Mehmetaj.jpeg";
import maxime from "../assets/Maxime Cherix.jpeg";
import melissa from "../assets/Mélissa Casoni.jpeg";
import nathan from "../assets/Nathan Disner.jpeg";




const descriptionMelissa = "Étudiante à la HES-SO Valais/Wallis en dernière année d’un BSc. en gestion & Tourisme. Responsable du lien entre l’équipe développement du site ww.stock-ici.ch. Passionnée de digitalisation d’art et de gamification. Elle aime mettre à profit sa créativité et son leadership organisationnel au sein de cette équipe interdisciplinaire.";
const descriptionBenoit = "Actuellement en fin de formation à la Haute École d’Ingénierie du valais, orientation technologie alimentaire, se lancer dans Stock ici a été un vrai défi pour lui. Motivé à en apprendre plus dans le monde de l’entrepreneuriat, le projet Stock ici répond pleinement à ses attentes. Son esprit de synthèse et sa détermination ont parfaitement complété les qualités de l’équipe. ";
const descriptionNathan = "Étudiant en économie d’entreprise HES, il apporte ses compétences financières, d’innovation et management afin de participer au bon développement de Stock-Ici. Ce projet moderne et durable correspond à ses valeurs et lui permet de découvrir le monde tant convoité d’Elon Musk ou Steve Jobs : l’entrepreneuriat. Motivé et dynamique, il s’épanouit dans un team polyvalent et pragmatique comme Stock-Ici.";
const descriptionMaxime = "En dernière année d’un BSc. en économie d’entreprise à la HES-SO Valais/Wallis, il est responsable financier de Stock-ici. Il aime donc mettre ses capacités analytiques au profit de l’entreprise. Amateur d’innovations financières et digitales, Stock-ici correspond à ses valeurs économiques collaboratives et responsables. ";
const descriptionAbetare = "Je gère ma propre entreprise depuis neuf ans, je cumule les compétences professionnelles liées à quatre années de bagage académique à temps partiel à l’Haute Ecole de Gestion de Genève. Je recrute, forme et dirige une équipe de 12 personnes, en les motivant en m’occupant de leur bien-être au sein de l’entreprise.  J’aime les activités sociales et culturelles, je suis passionnée de voyage et de découvertes de cultures nouvelles. Je me décris comme une leader dynamique, curieuse et organisée, qui fait preuve d’empathie, d’esprit critique et atteignant constamment ses objectifs.";

export default function Team() {

    const themeContext = useContext(ThemeContext);

    return (
        <div className="teamPage">
            <div className="faqTitle" style={{backgroundColor: themes[themeContext.theme].cardContent, boxShadow: themes[themeContext.theme].cardShadowTitle}}>
                <h1 className="title1">Notre Team</h1>
            </div>
            <div className="intro">
                <Card style={{backgroundColor: themes[themeContext.theme].cardContent, boxShadow: themes[themeContext.theme].cardShadow}}>
                    <CardTitle className="title1">
                        Notre Start-up
                    </CardTitle>
                    <CardSubtitle>
                        Révolutionnaires & Solidaires
                    </CardSubtitle>
                    <CardContent>
                        <p className="teamCard-content-center">Grâce à notre plateforme unique en Suisse, nous cherchons à
                            optimiser les espaces inutilisés en mettant en liens les particuliers et entreprise qui
                            cherchent un espace de stockage. Fondée en 2020 notre équipe pluridisciplinaire ont
                            travaillé sans relâche pour mettre la plateforme Stock ici, Rent Easy au profit d'une d'une
                            économie plus solidaire</p>
                        <p className="teamCard-content-center">Afin de fournir d'excellentes solutions de co-stockage, nous
                            consacrons notre temps et nos ressources à la recherche des besoins du marché et des
                            habitudes et des intérêts de nos clients. Nous continuerons à tout mettre en œuvre pour
                            devenir la référence dans ce domaine, en offrant diverses solutions de stockage non
                            seulement approuvées, mais aussi pour le besoin des PME comme par exemple dans le domaine de
                            l'hôtellerie de la gastronomie ou de l'agriculture avec des espaces réfrigérés</p>
                    </CardContent>
                </Card>
            </div>
            <div className="teamDescription">
                <Card className="teamCard" style={{backgroundColor: themes[themeContext.theme].cardContent, boxShadow: themes[themeContext.theme].cardShadow}}>
                    <img alt="Aperçu" height="200px" width="200px" src={melissa}/>
                    <CardTitle>
                        Melissa Casoni
                    </CardTitle>
                    <CardContent>
                        <p className="teamCard-content">{descriptionMelissa}</p>
                    </CardContent>
                </Card>
                <Card className="teamCard" style={{backgroundColor: themes[themeContext.theme].cardContent, boxShadow: themes[themeContext.theme].cardShadow}}>
                <img alt="Aperçu" height="200px" width="200px" className="mx-auto" src={maxime}/>
                    <CardTitle>
                        Maxime Cherix
                    </CardTitle>
                    <CardContent>
                        <p className="teamCard-content">{descriptionMaxime}</p>
                    </CardContent>
                </Card>
                <Card className="teamCard" style={{backgroundColor: themes[themeContext.theme].cardContent, boxShadow: themes[themeContext.theme].cardShadow}}>
                <img alt="Aperçu" height="200px" width="200px" src={nathan}/>
                    <CardTitle>
                        Nathan Disner
                    </CardTitle>
                    <CardContent>
                        <p className="teamCard-content">{descriptionNathan}</p>
                    </CardContent>
                </Card>
                <Card className="teamCard" style={{backgroundColor: themes[themeContext.theme].cardContent, boxShadow: themes[themeContext.theme].cardShadow}}>
                <img alt="Aperçu" height="200px" width="200px" src={benoit}/>
                    <CardTitle>
                        Benoît Gremaud
                    </CardTitle>
                    <CardContent>
                        <p className="teamCard-content">{descriptionBenoit}</p>
                    </CardContent>
                </Card>
                <Card className="teamCard" style={{backgroundColor: themes[themeContext.theme].cardContent,boxShadow: themes[themeContext.theme].cardShadow}}>
                <img alt="Aperçu" height="200px" width="200px" src={abetare}/>
                    <CardTitle>
                        Abetare Mehmetaj
                    </CardTitle>
                    <CardContent>
                        <p className="teamCard-content">{descriptionAbetare}</p>
                    </CardContent>
                </Card>
            </div>


        </div>
    )
}