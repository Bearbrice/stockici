// plus page for all other information
import React from "react";
import Card from "@material-ui/core/Card";
import CardTitle from "reactstrap/es/CardTitle";

const Faq = () => {
    return (
        <div>
            <Card className="faq-organisation">
                <CardTitle className="faq-text-organisation">
                    <h1 className="faq-espace-titre">Questions — Réponses FAQ </h1>
                    <h3 className="faq-espace-titre"><b>Qui sommes-nous ?</b></h3>
                    <h5>Stock ici propose des solutions de stockage faciles. Notre plateforme de co-stockage aide nos
                        partenaires qui veulent entreposer, louer ou optimiser de la place libre. En toute confiance,
                        Stock ici souhaite valoriser la proximité, la durabilité et le partage avec vous. Rent easy,
                        Stock ici!
                        <br/><br/>
                        Nous sommes une plateforme communautaire de location et réservation d’espaces de stockage.
                        <br/><br/>
                        L’inscription, les demandes de réservation et le dépôt d’annonces sont gratuits et se font par
                        le biais d’une inscription sur notre site ! </h5>
                    <h3 className="faq-espace-titre"><b>Quels types d’espace peut-on trouver en location sur Stock ici
                        ? </b></h3>
                    <h5>
                        Les espaces mis en ligne sont aussi variés que les biens à stocker. Les espaces sont des espaces
                        de co-stockage et non des boxes. Nous recevons fréquemment des demandes pour des locations de
                        <b> caves</b>, de <b>garages</b> ou <b>place de parc</b>. Les <b>hangars, ateliers,
                        entrepôts</b> ou <b>pièces vides</b> de votre
                        maison peuvent aussi être mis en location.
                    </h5>
                    <h3 className="faq-espace-titre"><b>Comment déposer une annonce sur stock ici ?
                    </b></h3>
                    <h5>
                        Rendez-vous sur la page d’accueil et cliquez sur le bouton J’ai un espace à louer. Remplissez le
                        formulaire en ligne, avec une description précise et des photos de votre espace de stockage.
                    </h5>
                    <h3 className="faq-espace-titre"><b>Qui peut déposer une annonce ?
                    </b></h3>
                    <h5>
                        Toute personne âgée de 18 ans et plus peut déposer une annonce. Vous n’avez rien à payer,
                        l’inscription et la mise en ligne d’annonces sont gratuites. Si vous n’êtes <b>pas
                        propriétaire</b> de
                        l’espace que vous souhaitez mettre en ligne, vous devez vous assurer de bénéficier de <b>l’accord
                        express et écrit du propriétaire.</b>
                    </h5>
                    <h3 className="faq-espace-titre"><b>Combien coûte le dépôt d’une annonce ?
                    </b></h3>
                    <h5>
                        Déposer une annonce est parfaitement gratuit tout comme l’inscription sur le site.
                    </h5>
                    <h3 className="faq-espace-titre"><b>Comment les tarifs garde-meuble sont-ils calculés ?
                    </b></h3>
                    <h5>
                        es prix sont librement <b>fixés par les propriétaires.</b> Vous connaissez l’espace que vous
                        mettez en
                        location, nous vous laissons arbitrer cette question.<br/><br/>

                        Vous devez déterminer un prix garde-meuble au mois, quelle que soit la durée minimale et
                        maximale de location que vous fixez par ailleurs. Pour vous aider, nous vous indiquons un prix
                        moyen en <b>fonction de la situation géographique</b> de votre espace au moment du dépôt
                        d’annonce.
                        Vous pouvez également consulter notre page tarif garde-meuble.
                    </h5>
                    <h3 className="faq-espace-titre"><b>Combien peut-on gagner grâce à la location ?
                    </b></h3>
                    <h5>
                        À titre indicatif, nous vous proposons une <b>grille tarifaire mensuelle moyenne</b> selon votre
                        région des précédentes annonces sur notre site.
                    </h5>

                    <table>
                        <thead>
                        <tr>
                            <th>item</th>
                            <th>Surface</th>
                            <th>Volume</th>
                            <th>Prix</th>
                            <th>Grandes villes</th>
                            <th>Zones rurales</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Cave, grenier, atelier, débarras</td>
                            <td>1 à 2m²</td>
                            <td>2 à 6m³</td>
                            <td>40 CHF</td>
                            <td>20 à 40€</td>
                            <td>10€</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3 à 4m²</td>
                            <td>7 à 12m³</td>
                            <td>60 CHF</td>
                            <td>40 à 50€</td>
                            <td>30€</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5 à 7m²</td>
                            <td>13 à 20m³</td>
                            <td>90CHF</td>
                            <td>60€</td>
                            <td>40€</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>8 à 12m²</td>
                            <td>21 à 35m³</td>
                            <td>130CHF</td>
                            <td>80 à 90€</td>
                            <td>70€</td>
                        </tr>
                        <tr>
                            <td>Garage</td>
                            <td></td>
                            <td></td>
                            <td>130CHF</td>
                            <td>80 à 90€</td>
                            <td>70€</td>
                        </tr>
                        </tbody>
                    </table>


                </CardTitle>
            </Card>
        </div>

    )
}

export default Faq