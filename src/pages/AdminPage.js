import {ThemeContext, themes} from "../context/ThemeContext";
import {Link} from "react-router-dom";
import React, {useContext} from "react";
import {AdminNavigationBar} from "../components/admin/AdminNavigationBar";
import {AdminTabs} from "../components/admin/AdminTabs";

export function AdminPage() {
    const themeContext = useContext(ThemeContext);

    return (
        <div className="App">
            <AdminNavigationBar/>
            <header className="App-header" style={{
                backgroundColor: themes[themeContext.theme].background,
                color: themes[themeContext.theme].foreground,
                height: 100
            }}>

                <AdminTabs/>

            </header>

            <footer style={{
                backgroundColor: themes[themeContext.theme].background,
                color: themes[themeContext.theme].foreground,
                textAlign: "center"
            }}>
                <h6>©2021 - Stock Ici Dev Team</h6>
                <Link
                    style={{paddingRight: "2em"}}
                    to="/terms-of-use"
                >Terms of use
                </Link>
            </footer>

        </div>
    );
}