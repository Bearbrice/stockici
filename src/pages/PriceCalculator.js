import React, {useContext, useEffect, useState} from "react";
import {Form, Spinner} from "react-bootstrap";
import {Grid, Slider} from "@material-ui/core";
import {cantonData} from "../assets/price_calculator/canton";
import {categoriesData} from "../assets/price_calculator/categories";
import {ThemeContext, themes} from "../context/ThemeContext";

export default function PriceCalculator() {
    const [canton, setCanton] = useState("");
    const [value, setValue] = useState(50);
    const [type, setType] = useState("");
    const [price, setPrice] = useState(0);
    const [calculing, setCalculing] = useState(false);
    const themeContext = useContext(ThemeContext);

    useEffect(() => {
        calculate();
    }, [canton]);

    useEffect(() => {
        calculate();
    }, [type]);

    useEffect(() => {
        calculate();
    }, [value]);

    const handleSliderChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleInputChange = (event) => {
        setValue(event.target.value === '' ? '' : Number(event.target.value));
    };

    const handleBlur = () => {
        if (value < 1) {
            setValue(1);
        } else if (value > 100) {
            setValue(100);
        }
    };

    function GetCantons() {
        const data = cantonData.sort((a, b) => a.canton.localeCompare(b.canton))
        return (
            <>
                {data.map((item) => {
                    return (<option key={item.canton} value={item.canton}>{item.canton}</option>);
                })}
            </>
        );
    }

    function GetCategories() {
        const data = categoriesData.sort((a, b) => a.type.localeCompare(b.type))
        return (
            <>
                {data.map((item) => {
                    return (<option key={item.type} value={item.type}>{item.type}</option>);
                })}
            </>
        );
    }

    function handleCanton(e) {
        setCanton(e.target.value);
    }

    function handleType(e) {
        setType(e.target.value);
    }

    function GetEstimation() {
        if (price === 0) {
            return <h3>Sélectionnez tous les critères pour obtenir une estimation</h3>
        } else {
            return <div>
                <h5>Estimation pour {type} à {canton} pour {value} m<sup>3</sup></h5>
                <h1>Estimation: {price} CHF/mois</h1>
            </div>
        }
    }

    // o = objet - p = price
    function calculate() {
        setCalculing(true);

        // Set a timeout to show the spinner
        setTimeout(function () {
            let pType;
            let pCanton;

            if (type === "" || canton === "") {
                setCalculing(false);
                setPrice(0);
                return;
            }

            let oType = categoriesData.find(category => category.type === type);
            pType = oType.price;

            let oCanton = cantonData.find(category => category.canton === canton);
            pCanton = oCanton.percentage;

            // m3 * price of the type * percentage of the canton
            let basePrice = value * pType;

            let finalPrice;
            if (pCanton < 0) {
                finalPrice = (basePrice - (basePrice * pCanton / 100))
            } else {
                finalPrice = (basePrice + (basePrice * pCanton / 100))
            }

            setPrice(finalPrice);
            setCalculing(false);
        }, 200);
    }

    return (
        <div className="priceCalculatorPage">
            <div className="faqTitle" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadowTitle
            }}>
                <h1 className="title1">Estimez le prix de votre emplacement !</h1>
            </div>
            <div className="calculator" style={{
                boxShadow: themes[themeContext.theme].cardShadow,
                marginTop: '30px'
            }}>
                <Form>
                    <div className="filtersBox card" style={{backgroundColor: themes[themeContext.theme].cardContent}}>
                        <div className="filters" style={{textAlign: "center"}}>
                            <div className="form-group mr-5">
                                <Form.Group>
                                    <Form.Label>Canton</Form.Label>
                                    <Form.Control
                                        style={{marginTop: "2.5em"}}
                                        as="select"
                                        defaultValue={"Test"}
                                        value={canton}
                                        onChange={handleCanton}
                                        placeHolder={"Test"}
                                    >
                                        <option style={{color: '#919399'}}>Sélectionner</option>
                                        <GetCantons/>
                                    </Form.Control>
                                </Form.Group>
                            </div>
                            <div className="form-group mr-5">
                                <Form.Group>
                                    <Form.Label>Type</Form.Label>
                                    <Form.Control style={{marginTop: "2.5em"}}
                                                  as="select"
                                                  onChange={handleType}
                                                  value={type}
                                    >
                                        <option value="" key="" style={{color: '#919399'}}>Sélectionner</option>
                                        <GetCategories/>
                                    </Form.Control>
                                </Form.Group>
                            </div>
                            <div className="form-group mr-5 mb-0">
                                <Form.Group>
                                    <Form.Label>Mètres cubes</Form.Label>
                                    <Grid container spacing={1} alignItems="center" style={{marginTop: "1em"}}>
                                        {/*<Grid item style={{width: "60%", marginTop: "15px"}}>*/}
                                        <Slider
                                            onChange={handleSliderChange}
                                            value={typeof value === 'number' ? value : 0}
                                            valueLabelDisplay="on"
                                            defaultValue={20}
                                            style={{color: "#52af77"}}
                                            min={1}
                                        />
                                        <Form.Row className="align-items-center"
                                                  style={{justifyContent: "center", marginTop: "0.2em"}}>
                                            <Form.Control style={{width: "50%", marginRight: "1em"}}
                                                          type="number"
                                                          min="1"
                                                          max="100"
                                                          value={value}
                                                          onChange={handleInputChange}
                                                          onBlur={handleBlur}
                                            />
                                            <Form.Text>m<sup>2</sup></Form.Text>
                                        </Form.Row>
                                    </Grid>
                                </Form.Group>
                            </div>
                        </div>
                    </div>
                </Form>

                <div className="filtersBox card"
                     style={{textAlign: "center", backgroundColor: themes[themeContext.theme].cardContent}}>
                    {calculing ? <Spinner variant="success" animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                        :
                        <GetEstimation/>
                    }
                </div>
            </div>
        </div>
    )
}