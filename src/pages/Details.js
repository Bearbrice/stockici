import {Card, CardContent, IconButton, Tooltip} from "@material-ui/core";
import {CardTitle} from "reactstrap";
import {useHistory, useParams} from "react-router";
import React, {useContext, useEffect, useState} from "react";
import firebase from "../firebase";
import 'react-day-picker/lib/style.css';
import {
    CheckCircleOutline,
    Edit,
    Event,
    Home,
    ImportExport,
    Payment,
    QuestionAnswer,
    Room,
    SpaceBar,
    Videocam,
    VideocamOff,
} from "@material-ui/icons";
import {SplashScreen} from "../components/SplashScreen";
import Button from "@material-ui/core/Button";
import moment from "moment";
import {CircleMarker, MapContainer, TileLayer} from "react-leaflet";
import DayPicker, {DateUtils} from "react-day-picker";
import {Carousel} from "react-responsive-carousel";
import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    TwitterIcon,
    TwitterShareButton,
    WhatsappIcon,
    WhatsappShareButton
} from "react-share";
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";
import {Modal} from "react-bootstrap";
import {firebaseAuth} from "../provider/AuthProvider";
import {ThemeContext, themes} from "../context/ThemeContext";


const Details = () => {

    //Data for DatePicker//
    const MONTHS = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre',
    ];
    const WEEKDAYS_LONG = [
        'Lundi',
        'Mardi',
        'Mercredi',
        'Jeudi',
        'Vendredi',
        'Samedi',
        'Dimanche',
    ];
    const WEEKDAYS_SHORT = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
    //End//


    const themeContext = useContext(ThemeContext);
    const {user} = useContext(firebaseAuth);
    let {slugPlace} = useParams();
    let history = useHistory();

    //current url
    const currentPage = window.location.href;

    //useState for the data
    const [place, setPlace] = useState([]);
    const [comments, setComments] = useState([]);
    const [loading, setLoading] = useState(false);
    const [address, setAddress] = useState();
    const [duration, setDuration] = useState();
    const [minDuration, setMinDuration] = useState();
    const [rate, setRate] = useState();

    //useState for datePicker
    const [from, setFrom] = useState(undefined);
    const [to, setTo] = useState(undefined);
    const [range, setRange] = useState({from: undefined, to: undefined})
    const [isDateOK, setIsDateOK] = useState(false);
    const [disabledDays, setDisabledDays] = useState(false);

    //useState for the modal
    const [isOpen, setIsOpen] = useState(false);

    //settings for datePicker
    const modifiers = {
        start: from,
        end: to,
        pastDays: {before: new Date()},
        disabled: disabledDays,
    };

    //Database reference
    const refPlace = firebase.firestore().collectionGroup("places");
    const refRentals = firebase.firestore().collection("rentals");
    const refComments = firebase.firestore().collectionGroup("comments");


    //GET FUNCTION

    //Get the current place
    function getPlace() {
        const items = [];
        refPlace.onSnapshot((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                if (doc.id === slugPlace) {
                    items.push(doc.data());

                    //Get the city in the address
                    let ad = items[0].address;
                    let temp = [];
                    temp = ad.split(" ");
                    ad = temp[temp.length - 1];
                    setAddress(ad);
                }
            });
            setPlace(items);
            setDuration(items[0].duration);
            setMinDuration((items[0].minDuration));
        });


    }

    //GET FUNCTION
    //Get all the comments of the current place
    async function getAllCommentForOnePlace() {
        const items = [];
        let somme = 0;
        let moyenne = 0;
        let cpt = 0;
        const snapshot = await refComments.where('placeId', '==', slugPlace).where('isValidate', '==', true).get();

        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }
        snapshot.forEach((doc) => {
            items.push(doc.data());
            //average rate
            if (doc.data().rating !== 0) {
                cpt++;
                somme = somme + doc.data().rating;
            }

        });
        setComments(items);

        moyenne = somme / cpt;
        setRate(Math.round(moyenne));

    }

    //functions for the modal
    const showModal = () => {
        setIsOpen(true);
    };

    const hideModal = () => {
        setIsOpen(false);
    };


    //GET FUNCTION
    //Get existing reservation
    async function getRentals() {
        const snapshot = await refRentals.where('placeId', '==', slugPlace).get();
        const items = [];
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }

        snapshot.forEach(doc => {
            items.push({from: doc.data().startDate.toDate(), to: doc.data().endDate.toDate()});
        });
        setDisabledDays(items);
    }


    //function to rent the place
    const rent = (from, to, place) => {
        //if the user is the owner of the pklace, he can't rent it

        if (user.uid === place[0].owner) {
            showModal();
        } else {
            history.push({
                pathname: `/locationForm/${slugPlace}`,
                state: {from: from, to: to, place: place}
            })
        }
    }

    //FUNCTIONS FOR THE DATE PICKER
    //function to reset the date picker
    const handleResetClick = () => {
        setFrom(undefined);
        setTo(undefined);
    }

    //function to handle the days on the date picker
    const handleDayClick = (day, modifiers = {}) => {
        //verify if the clicked day is available
        if (modifiers.disabled) {
            return;
        } else {
            const newRange = DateUtils.addDayToRange(day, range);
            setRange(newRange);
            setFrom(newRange.from);
            setTo(newRange.to);

            if (newRange.to !== undefined) {
                //d = minimum number of days for this location
                //calculate the duration in day
                let d;
                if (duration === "semaine(s)") {
                    d = minDuration * 7;
                } else if (duration === "mois") {
                    d = minDuration * 30;
                } else if (duration === "année(s)") {
                    d = minDuration * 365;
                } else {
                    d = minDuration;
                }

                //Get the nb of days between the start date and the end date
                let diff;

                let a = moment(newRange.from.toLocaleDateString(), 'DD/MM/YYYY');
                let b = moment(newRange.to.toLocaleDateString(), 'DD/MM/YYYY');
                diff = b.diff(a, 'days');

                setIsDateOK(true);
                if (diff >= d) {
                    console.log("Date okay");
                    setIsDateOK(true);
                } else {
                    console.log("Date pas okay");
                }
            }

        }
    }

    useEffect(() => {
        getPlace();
        getRentals();
        getAllCommentForOnePlace();
        const timer = setTimeout(() => {
            setLoading(true);
        }, 1000);
        return () => clearTimeout(timer);

    }, []);


    return (
        <div className="detailsPage">
            {loading ?
                <>
                    <div className="leftContainer"/>
                    {place.map((p) => (
                        <>
                            <div className="detailsContainer"
                                 style={{boxShadow: themes[themeContext.theme].cardShadow}}>
                                <Card style={{backgroundColor: themes[themeContext.theme].cardContent}}>
                                    <CardTitle className="d-flex flex-row">
                                        <div className="titleLeft"/>
                                        <div className="titleDetails">
                                            <h1 className="title1">{p.titleOffer}</h1>
                                        </div>
                                        <div className="titleLeft">
                                            <Tooltip title={<h4>Emplacement certifié par notre équipe !</h4>}
                                                     placement="top">
                                                <IconButton aria-label="certified">
                                                    <CheckCircleOutline className="circleIcon"
                                                                        style={{width: '80px', height: '80px'}}/>
                                                </IconButton>
                                            </Tooltip>
                                        </div>
                                    </CardTitle>
                                    <Carousel infiniteLoop="true" renderThumbs={function () {
                                        return null
                                    }}>
                                        {p.photoURLs.map((pic) => (
                                            <img src={pic} className="detailsImages" alt="lieu"/>
                                        ))}
                                    </Carousel>
                                    <CardContent>
                                        <div className="d-flex flex-row">
                                            <div className="shareContainer">
                                                <WhatsappShareButton
                                                    url={currentPage}
                                                    className="mr-3">
                                                    <WhatsappIcon size={40} round={true}/>
                                                </WhatsappShareButton>
                                                <FacebookShareButton
                                                    url={currentPage}
                                                    title="Voici un lien qui pourrait t'intéresser !"
                                                    className="mr-3">
                                                    <FacebookIcon size={40} round={true}/>
                                                </FacebookShareButton>
                                                <TwitterShareButton
                                                    url={currentPage}
                                                    quote={p.titleOffer}
                                                    className="mr-3">
                                                    <TwitterIcon size={40} round={true}/>
                                                </TwitterShareButton>
                                                <EmailShareButton
                                                    url={currentPage}
                                                    subject={p.titleOffer}
                                                    body="Voici un lien qui pourrait t'intéresser !"
                                                    className="mr-3">
                                                    <EmailIcon size={40} round={true}/>
                                                </EmailShareButton>
                                            </div>
                                            <div className="ratingContainer">
                                                <Box component="fieldset" mb={3} borderColor="transparent">
                                                    <Rating
                                                        value={rate}
                                                        readOnly
                                                        size='large'
                                                    />
                                                </Box>
                                            </div>
                                        </div>
                                        <div className="infoCaraContainer">
                                            <div className="infoBox">
                                                <h4>Informations</h4>
                                                <div>
                                                    <Room/>
                                                    <p>{address}</p>
                                                </div>
                                                <div>
                                                    <Payment/>
                                                    <p>{p.price} CHF / {p.duration}</p>
                                                </div>
                                                <div>
                                                    <SpaceBar/>
                                                    <p>{p.areaAvailable} m2</p>
                                                </div>
                                            </div>
                                            <div className="characteristicsBox">
                                                <h4>Caractéristiques</h4>
                                                <div>
                                                    <Home/>
                                                    <p>Étage : {p.floor}</p>
                                                </div>
                                                <div>
                                                    <ImportExport/>
                                                    {
                                                        p.hasElevator ?
                                                            <p>Ascenseur disponible</p>
                                                            :
                                                            <p>Pas d'ascenseur</p>
                                                    }
                                                </div>
                                                {
                                                    p.isSecured ?
                                                        <div>
                                                            <Videocam/>
                                                            <p>Emplacement surveillé</p>
                                                        </div>
                                                        :
                                                        <div>
                                                            <VideocamOff/>
                                                            <p>Emplacement sans surveillance</p>
                                                        </div>
                                                }
                                                <div>
                                                    <Event/>
                                                    <p>Durée minimum de location : {p.minDuration} {p.duration}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            p.description === "" ?
                                                null
                                                :
                                                <div className="descriptionBox">
                                                    <div>
                                                        <h4>Description</h4>
                                                        <Edit/>
                                                    </div>
                                                    <p>{p.description}</p>
                                                </div>
                                        }
                                        <div className="descriptionBox">
                                            <div>
                                                <h4>Commentaires</h4>
                                                <QuestionAnswer/>
                                            </div>
                                            <div className="d-flex flex-column">
                                                {comments.map((com) => (
                                                    <div className="commentBox">
                                                        <p style={{
                                                            fontSize: '20px',
                                                            paddingTop: '2px',
                                                            marginRight: '10px',
                                                            marginLeft: '5px'
                                                        }}><i>{com.writtenDate} - </i></p>
                                                        <p style={{fontSize: '20px'}}>{com.comment} </p>
                                                        {com.rating > 0 ?
                                                            <>
                                                                <p style={{fontSize: '20px'}}>&nbsp;- </p>
                                                                <Rating
                                                                    value={com.rating}
                                                                    readOnly
                                                                    size='large'
                                                                />
                                                            </>
                                                            : null}
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                        <div className="descriptionBox">
                                            <h4>Localisation</h4>
                                            <MapContainer className="mapSearch"
                                                          center={[p.lat, p.lng]}
                                                          zoom={13}
                                            >
                                                <TileLayer
                                                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                                                <CircleMarker center={[p.lat, p.lng]} radius={100}/>
                                            </MapContainer>
                                        </div>

                                    </CardContent>
                                </Card>
                            </div>
                            <div className="datePickerContainer">
                                <Card style={{backgroundColor: themes[themeContext.theme].cardContent}}>
                                    <CardContent>
                                        <div className="bookingBox1">
                                            <p>{p.price} CHF / {p.duration}</p>
                                            <p>{p.type} de {p.areaAvailable} m2</p>

                                        </div>
                                        <div className="bookingBox2">
                                            <label className="labelFilters">
                                                {!from && !to && 'Sélectionnez une date de début.'}
                                                {from < new Date() && 'Cette est passée. Sélectionnez en une autre.'}
                                                {from && !to && from > new Date() && 'Sélectionnez une date de fin.'}
                                                {from &&
                                                to && from > new Date() &&
                                                `Louer du ${from.toLocaleDateString()} au ${to.toLocaleDateString()}`}{' '}
                                            </label>
                                            {from && to && (
                                                <Button className="bg-info" onClick={handleResetClick}>
                                                    Annuler la sélection
                                                </Button>
                                            )}
                                            <DayPicker
                                                className="Selectable"
                                                selectedDays={[from, {from, to}]}
                                                modifiers={modifiers}
                                                onDayClick={handleDayClick}
                                                months={MONTHS}
                                                weekdaysLong={WEEKDAYS_LONG}
                                                weekdaysShort={WEEKDAYS_SHORT}
                                                showOutsideDays

                                            />
                                        </div>
                                        <div className="bookingBox3">
                                            <Button className="bg-success"
                                                    onClick={() => rent(from, to, place)}
                                                //disabled={!from || !to }
                                                    disabled={!isDateOK}
                                            >Demande de
                                                location</Button>
                                        </div>
                                    </CardContent>
                                </Card>
                            </div>
                            <>
                                <Modal show={isOpen} onHide={hideModal} centered>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Attention !</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>Cet emplacement vous appartient ! Pas besoin de le louer, réservez vos
                                        dates dans votre
                                        profile. </Modal.Body>
                                    <Modal.Footer>
                                        <Button className="bg-primary mr-4" onClick={hideModal}>
                                            Ok
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                        </>
                    ))}
                </>
                :
                <div className="text-center">
                    <SplashScreen/>
                </div>
            }
        </div>

    )
}

export default Details;
