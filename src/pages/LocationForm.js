import {useHistory, useParams} from "react-router";
import dateFormat from 'dateformat';
import React, {useContext, useState} from "react";
import {Card, CardContent} from "@material-ui/core";
import {Formik} from "formik";
import label, {Form} from "react-bootstrap";
import {firebaseAuth} from "../provider/AuthProvider";
import Button from "@material-ui/core/Button";
import firebase from "../firebase";
import {addNotification} from "../services/User/NotificationsController";
import {Carousel} from "react-responsive-carousel";
import {Link} from "react-router-dom";
import {ThemeContext, themes} from "../context/ThemeContext";

const LocationForm = props => {

    const themeContext = useContext(ThemeContext);

    let {slugPlace} = useParams();
    let history = useHistory();
    const {user} = useContext(firebaseAuth);

    //data
    let from = props.location.state.from;
    let fromFormat = dateFormat(props.location.state.from, "dd/mm/yyyy");
    let to = props.location.state.to;
    let toFormat = dateFormat(props.location.state.to, "dd/mm/yyyy");
    let place = props.location.state.place;

    //database reference
    const refAllRentals = firebase.firestore().collection("rentals");

    //useState for the user message
    const [message, setMessage] = useState("");


    //initial value of the form are current value of the user
    let initialValues = {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        city: user.city,
        npa: user.NPA,
        address: user.address,
        message: ""
    }

    //function to rsend a request to the owner
    async function rentalApplication(userInfo) {

        //data to send
        const newRental = {
            endDate: firebase.firestore.Timestamp.fromDate(to),
            startDate: firebase.firestore.Timestamp.fromDate(from),
            owner: place[0].owner,
            placeId: slugPlace,
            renter: user.uid,
            isAccepted: false,
            asked: firebase.firestore.Timestamp.fromDate(new Date())
        };

        await refAllRentals
            .doc(newRental.id)
            .set(newRental);

        //message for the notification
        let msg = "Un client souhaite louer votre " + place[0].titleOffer;
        addNotification(place[0].owner, "Nouvelle demande de location", msg, 'places');

    }


    return (
        <div className="formLocationPage">
            <div className="w-25"/>
            <Card className="divCoordonnees" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadow
            }}>
                <CardContent>
                    <h2 className="formLocationTitle title2">Mes coordonnées</h2>
                    <p className="termsParagraph">Merci de vérifier les informations suivantes. Si un ou plusieurs
                        champs, merci de les corriger
                        dans&nbsp;
                        <Link
                            to="/account/profile"
                        >votre profil.
                        </Link>
                    </p>
                    <Formik initialValues={initialValues}>
                        <Form>
                            <div className="row">
                                <Form.Group className="column2">
                                    <label>Prénom*</label>
                                    <Form.Control
                                        type="text"
                                        name="firstname"
                                        value={user.firstname}
                                        disabled="true"
                                    />
                                </Form.Group>
                                <Form.Group className="column2">
                                    <label>Nom*</label>
                                    <Form.Control
                                        type="text"
                                        name="lastname"
                                        value={user.lastname}
                                        disabled="true"
                                    />
                                </Form.Group>
                            </div>
                            <div className="row">
                                <Form.Group className="column1">
                                    <label>Email*</label>
                                    <Form.Control
                                        type="email"
                                        name="email"
                                        value={user.email}
                                        disabled="true"
                                    />
                                </Form.Group>
                            </div>

                            <div className="row">
                                <Form.Group className="column3">
                                    <label>Adresse*</label>
                                    <Form.Control
                                        type="text"
                                        name="address"
                                        disabled="true"
                                        value={user.address}
                                    />
                                </Form.Group>
                                <Form.Group className="column3">
                                    <label>NPA*</label>
                                    <Form.Control
                                        type="number"
                                        name="npa"
                                        disabled="true"
                                        value={user.NPA}
                                    />
                                </Form.Group>
                                <Form.Group className="column3">
                                    <label>Ville*</label>
                                    <Form.Control
                                        type="text"
                                        disabled="true"
                                        name="city"
                                        value={user.city}
                                    />
                                </Form.Group>
                            </div>
                            <p>*Votre adresse ne sera utilisée que lors de la signature du contrat</p>
                            <Form.Group className="column1">
                                <label>Message</label>
                                <Form.Control as="textarea"
                                              type="text"
                                              name="message"
                                              placeholder="Posez ici vos questions au propriétaire !"
                                              value={message}
                                              onChange={(e) => setMessage(e.target.value)}
                                />
                            </Form.Group>
                            <Button className="bg-success" type="submit"
                                    onClick={async function (event) {
                                        event.preventDefault();
                                        await rentalApplication({
                                            firstname: user.firstname,
                                            lastname: user.lastname,
                                            email: user.email,
                                            city: user.city,
                                            message,
                                            npa: user.NPA,
                                            address: user.address
                                        });
                                        history.push('/confirmation');
                                    }
                                    }
                            >
                                Confirmer
                            </Button>
                        </Form>
                    </Formik>
                </CardContent>
            </Card>
            <div className="divRecap">
                <Card style={{
                    backgroundColor: themes[themeContext.theme].cardContent,
                    boxShadow: themes[themeContext.theme].cardShadow
                }}>
                    <h2 className="title2 px-3 mt-3 mb-0">Récapitulatif</h2>
                    <CardContent>
                        <div className="recapBox1">
                            <p>{place[0].titleOffer}</p>
                            <Carousel infiniteLoop="true" renderThumbs={function () {
                                return null
                            }}>
                                {place[0].photoURLs.map((pic) => (
                                    <img src={pic} className="recapImg" alt="pic"/>
                                ))}
                            </Carousel>

                        </div>
                        <div className="recapBox2">
                            <p>Location du :</p>
                            <p className="text-center">{fromFormat} au {toFormat}</p>
                        </div>
                        <div className="recapBox1">
                            <p>Prix : </p>
                            <p>{place[0].price} CHF / {place[0].duration}</p>
                        </div>
                    </CardContent>
                </Card>
            </div>
        </div>
    )
}
export default LocationForm;
