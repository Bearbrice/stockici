import React, {useContext, useState} from "react";
import {Card, CardContent, Tooltip} from "@material-ui/core";
import {CardTitle} from "reactstrap";
import {
    AirportShuttle,
    CardGiftcard,
    DirectionsBike,
    DriveEta,
    EventSeat,
    GridOn,
    Hotel,
    Info,
    Motorcycle,
    TableChart
} from "@material-ui/icons";
import {Button, Form} from "react-bootstrap";
import {objectsData} from "../assets/surface_calculator/objects";
import {ThemeContext, themes} from "../context/ThemeContext";

const SurfaceCalculator = () => {
    const themeContext = useContext(ThemeContext);

    const [surfacem2, setSurfacem2] = useState(0);
    const [surfacem3, setSurfacem3] = useState(0);
    const [nbBed1, setNbBed1] = useState(0);
    const [nbBed2, setNbBed2] = useState(0);
    const [nbChair, setNbChair] = useState(0);
    const [nbTable, setNbTable] = useState(0);
    const [nbShelf, setNbShelf] = useState(0);
    const [nbBox, setNbBox] = useState(0);
    const [nbCar, setNbCar] = useState(0);
    const [nbBike, setNbBike] = useState(0);
    const [nbMotorbike, setNbMotorbike] = useState(0);
    const [nbBus, setNbBus] = useState(0);
    const [nbTruck, setNbTruck] = useState(0);

    const calculate = (e) => {
        e.preventDefault();

        let estimationm2 = nbBed1 * objectsData[0].sizem2 + nbBed2 * objectsData[1].sizem2 + nbChair * objectsData[2].sizem2 + nbTable * objectsData[3].sizem2 + nbShelf * objectsData[4].sizem2 + nbBox * objectsData[5].sizem2 + nbBike * objectsData[6].sizem2 + nbMotorbike * objectsData[7].sizem2 + nbCar * objectsData[8].sizem2 + nbBus * objectsData[9].sizem2 + nbTruck * objectsData[10].sizem2;
        let estimationm3 = nbBed1 * objectsData[0].sizem3 + nbBed2 * objectsData[1].sizem3 + nbChair * objectsData[2].sizem3 + nbTable * objectsData[3].sizem3 + nbShelf * objectsData[4].sizem3 + nbBox * objectsData[5].sizem3 + nbBike * objectsData[6].sizem3 + nbMotorbike * objectsData[7].sizem3 + nbCar * objectsData[8].sizem3 + nbBus * objectsData[9].sizem3 + nbTruck * objectsData[10].sizem3;

        if (estimationm2 !== 0) {
            estimationm2 += 2;
            setSurfacem2(estimationm2);
            setSurfacem3(estimationm3.toFixed(2));
        }
    }

    return (
        <div className="surfaceCalculatorPage">
            <div className="faqTitle" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadowTitle
            }}>
                <h1 className="title1">Estimez l'espace dont vous avez besoin !</h1>
            </div>
            <form onSubmit={calculate}>
                <Card className="surfaceCard" style={{
                    backgroundColor: themes[themeContext.theme].cardContent,
                    boxShadow: themes[themeContext.theme].cardShadow
                }}>
                    <CardTitle>J'ai besoin de stocker :</CardTitle>
                    <CardContent className="surfaceContentCard">
                        <div className="firstRow">
                            <div className="container1">
                                <h4>Meubles</h4>
                                <div className="furnitureBox">
                                    <div className="box">
                                        <Hotel className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                        <div className="d-flex flex-row justify-content-between">
                                            <p>Lit(s) 1 personne </p>
                                            <Tooltip title={<h5>Taille standard 90x190x20 cm</h5>}
                                                     placement="top"
                                                     className="mt-3"><Info/></Tooltip>
                                        </div>
                                        <Form.Control
                                            type="number"
                                            name="nbBed"
                                            value={nbBed1}
                                            onChange={(e) => setNbBed1(e.target.value)}
                                        />
                                        <div className="d-flex flex-row justify-content-between">
                                            <p>Lit(s) 2 personnes </p>
                                            <Tooltip title={<h5>Taille standard 140x190x20 cm</h5>}
                                                     placement="top"
                                                     className="mt-3"><Info/></Tooltip>
                                        </div>
                                        <Form.Control
                                            type="number"
                                            name="nbBed"
                                            value={nbBed2}
                                            onChange={(e) => setNbBed2(e.target.value)}
                                        />
                                    </div>
                                    <div className="box">
                                        <EventSeat className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                        <div className="d-flex flex-row justify-content-between">
                                            <p>Chaise(s)</p>
                                            <Tooltip title={<h5>Taille standard 35x30x57 cm</h5>}
                                                     placement="top"
                                                     className="mt-3"><Info/></Tooltip>
                                        </div>
                                        <Form.Control
                                            type="number"
                                            name="nbChair"
                                            value={nbChair}
                                            onChange={(e) => setNbChair(e.target.value)}
                                        />
                                    </div>
                                    <div className="box">
                                        <TableChart className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                        <div className="d-flex flex-row justify-content-between">
                                            <p>Tables(s)</p>
                                            <Tooltip title={<h5>Taille standard 180x90x77 cm</h5>}
                                                     placement="top"
                                                     className="mt-3"><Info/></Tooltip>
                                        </div>
                                        <Form.Control
                                            type="number"
                                            name="nbTable"
                                            value={nbTable}
                                            onChange={(e) => setNbTable(e.target.value)}
                                        />
                                    </div>
                                    <div className="box">
                                        <GridOn className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                        <div className="d-flex flex-row justify-content-between">
                                            <p>Étagère(s)</p>
                                            <Tooltip title={<h5>Taille standard 124x183x38 cm</h5>}
                                                     placement="top"
                                                     className="mt-3"><Info/></Tooltip>
                                        </div>
                                        <Form.Control
                                            type="number"
                                            name="nbShelf"
                                            value={nbShelf}
                                            onChange={(e) => setNbShelf(e.target.value)}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="container2">
                                <h4>Cartons</h4>
                                <div className="boxBox">
                                    <div className="box">
                                        <CardGiftcard className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                        <div className="d-flex flex-row justify-content-between">
                                            <p>Carton(s)</p>
                                            <Tooltip title={<h5>Taille standard 40x60x40 cm</h5>}
                                                     placement="top"
                                                     className="mt-3"><Info/></Tooltip>
                                        </div>
                                        <Form.Control
                                            type="number"
                                            name="nbBox"
                                            value={nbBox}
                                            onChange={(e) => setNbBox(e.target.value)}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container3">
                            <h4>Véhicules</h4>
                            <div className="vehicleBox">
                                <div className="box">
                                    <DirectionsBike className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                    <div className="d-flex flex-row justify-content-between">
                                        <p>Vélo(s)</p>
                                        <Tooltip title={<h5>Taille standard 30x180x90 cm</h5>}
                                                 placement="top"
                                                 className="mt-3"><Info/></Tooltip>
                                    </div>
                                    <Form.Control
                                        type="number"
                                        name="nbBike"
                                        value={nbBike}
                                        onChange={(e) => setNbBike(e.target.value)}
                                    />
                                </div>
                                <div className="box">
                                    <Motorcycle className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                    <div className="d-flex flex-row justify-content-between">
                                        <p>Moto(s)</p>
                                        <Tooltip title={<h5>Taille standard 45x208x125cm</h5>}
                                                 placement="top"
                                                 className="mt-3"><Info/></Tooltip>
                                    </div>
                                    <Form.Control
                                        type="number"
                                        name="nbMotorbike"
                                        value={nbMotorbike}
                                        onChange={(e) => setNbMotorbike(e.target.value)}
                                    />
                                </div>
                                <div className="box">
                                    <DriveEta className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                    <div className="d-flex flex-row justify-content-between">
                                        <p>Voiture(s)</p>
                                        <Tooltip title={<h5>Taille standard 197x506x174 cm</h5>}
                                                 placement="top"
                                                 className="mt-3"><Info/></Tooltip>
                                    </div>
                                    <Form.Control
                                        type="number"
                                        name="nbCar"
                                        value={nbCar}
                                        onChange={(e) => setNbCar(e.target.value)}
                                    />
                                </div>
                                <div className="box">
                                    <AirportShuttle className="mx-auto" style={{width: '60px', height: '60px'}}/>
                                    <div className="d-flex flex-row justify-content-between">
                                        <p>Bus(s)</p>
                                        <Tooltip title={<h5>Taille standard 1250x250x430 cm</h5>}
                                                 placement="top"
                                                 className="mt-3"><Info/></Tooltip>
                                    </div>
                                    <Form.Control
                                        type="number"
                                        name="nbBus"
                                        value={nbBus}
                                        onChange={(e) => setNbBus(e.target.value)}
                                    />
                                </div>
                            </div>
                        </div>
                        <Button className="w-25 mx-auto" type="submit">Calculer</Button>
                        {surfacem2 !== 0 ?
                            <div className="vehicleBox mt-4">
                                <p>Vous avez besoin d'environ {surfacem2} m2, soit {surfacem3} m3</p>
                            </div>
                            :
                            null
                        }
                    </CardContent>
                </Card>
            </form>
        </div>
    )
}

export default SurfaceCalculator;