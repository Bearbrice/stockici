// Contact page
import React, {useContext} from "react";
import carton from "../assets/carton.png";
import label, {Button} from "react-bootstrap";
import emailjs from "emailjs-com";
import "../App.css";
import {ThemeContext, themes} from "../context/ThemeContext";

export default function ContactUs() {
    const themeContext = useContext(ThemeContext);

    function sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('gmail', 'template_1d9pfzs', e.target, 'user_OT64GWBNhUZ3T3v56R1AW')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
        e.target.reset()
    }


    return (
        <div className="contactPage">
            <img id="imgCarton" src={carton} alt="cartons"/>
            <div className="card contactForm" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadow
            }}>
                <h1 className="title1">Contactez-nous !</h1>
                <form onSubmit={sendEmail} className="form">
                    <div className="rowContact">
                        <div className="form-group contactFormGroup">
                            <label className="labelFilters" htmlFor="nameLabel">Nom</label>
                            <input className="form-control" id="nameInput" name="lastname"/>
                        </div>
                        <div className="form-group contactFormGroup">
                            <label className="labelFilters" htmlFor="firstnameLabel">Prénom</label>
                            <input className="form-control" id="firstnameInput" name="firstname"
                            />
                        </div>
                    </div>
                    <div className="rowContact">
                        <div className="form-group contactFormGroup">
                            <label className="labelFilters" htmlFor="emailLabel">Email</label>
                            <input className="form-control " id="emailInput" type="email" name="email"
                            />
                        </div>
                        <div className="form-group contactFormGroup">
                            <label className="labelFilters" htmlFor="objectLabel">Objet</label>
                            <input className="form-control" id="objectInput" name="subject"/>
                        </div>
                    </div>
                    <div className="form-group contactMessageFormGroup h-25">
                        <label className="labelFilters" htmlFor="messageLabel">Message</label>
                        <textarea className="form-control" id="messageInput" type="textarea" rows={4}
                                  style={{resize: 'none'}} name="message"/>
                    </div>
                    <br/>
                    <Button type="submit">Envoyer</Button>
                </form>
            </div>


        </div>
    )

}