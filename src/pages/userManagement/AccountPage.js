import React, {useContext} from "react";
import {firebaseAuth} from "../../provider/AuthProvider";
import {Tabs} from "react-bootstrap";
import {Container} from "../../components/Styled-Reusable/FormStyle";
import styled from "styled-components";
import {Fab} from "@material-ui/core";
import {ErrorView} from "../../components/Error";
import {ProfileTab} from "./profile/ProfileTab";
import {MyPlaces} from "./MyPlaces";
import {Route, useHistory} from "react-router-dom";
import {MyRentals} from "./MyRentals";
import {ThemeContext, themes} from "../../context/ThemeContext";

export const CustomTabs = styled(Tabs)`
  a {
    color: #7ed957;
  }

  a:hover {
    color: yellowgreen;
  }
`;
const AccountPage = () => {
        const themeContext = useContext(ThemeContext);
        const {user, initializing} = useContext(firebaseAuth);
        const history = useHistory();
        if (initializing) {
            return <p>Loading</p>
        }
        const showMyProfile = () => {
            history.push("/account/profile");
        }
        const showMyPlaces = () => {
            history.push("/account/my-places");
        }
        const showMyRentals = () => {
            history.push("/account/my-rentals");
        }
        return (
            <Container className="profileContainer">
                {user ?
                    <div className="divMenu flex-fill">
                        <div className="menuProfilePage">
                            <Fab style={{backgroundColor: themes[themeContext.theme].green}} variant="extended"
                                 size="medium"
                                 className="mb-3" onClick={showMyProfile}>Mon
                                profil</Fab>
                            <Fab style={{backgroundColor: themes[themeContext.theme].green}} variant="extended"
                                 size="medium"
                                 className="mb-3" onClick={showMyPlaces}>Mes
                                emplacements à louer</Fab>
                            {/*<Fab color="primary" variant="extended" size="medium" className="mb-3" onClick={showMyRequests}>Valider*/}
                            {/*    les demandes de locations</Fab>*/}
                            <Fab style={{backgroundColor: themes[themeContext.theme].green}} variant="extended"
                                 size="medium"
                                 onClick={showMyRentals}>Les
                                emplacements loués</Fab>
                        </div>
                        <Route path='/account/profile' component={ProfileTab}/>
                        <Route path='/account/my-places' component={MyPlaces}/>
                        {/*<Route path='/account/my-requests' component={MyRequests}/>*/}
                        <Route path='/account/my-rentals' component={MyRentals}/>
                    </div>
                    :
                    <ErrorView error={401} message={"Accès refusé, aucun utilisateur connecté."}/>
                }
            </Container>
        )
    }
;
export default AccountPage;