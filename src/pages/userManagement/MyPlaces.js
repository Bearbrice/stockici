import Button from "@material-ui/core/Button";
import {Badge as Ba, Modal, Spinner} from "react-bootstrap";
import React, {useContext, useEffect, useState} from "react";
import firebase from "../../firebase";
import {useAlert} from "react-alert";
import {firebaseAuth} from "../../provider/AuthProvider";
import {Carousel} from "react-responsive-carousel";
import {useHistory} from "react-router-dom";
import {Badge, Tooltip} from "@material-ui/core";
import {deleteRental, validateRental} from "../../services/RentalsController";
import {ThemeContext, themes} from "../../context/ThemeContext";
import {addNotification} from "../../services/User/NotificationsController";
import {sendPlaceToValidation} from "../../services/PlaceController2";

export function MyPlaces() {
    const history = useHistory();
    const themeContext = useContext(ThemeContext);
    const [idPlace, setIdPlace] = useState();
    const [places, setPlaces] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [loading, setLoading] = useState(true);
    const [idRental, setIdRental] = useState();
    const [rentals, setRentals] = useState([]);
    const [deletedPlace, setDeletedPlace] = useState();
    const {user} = useContext(firebaseAuth);
    const alert = useAlert();
    const ref = firebase.firestore().collection('users').doc(user.uid).collection('places');
    const refRentalsFromUser = firebase.firestore().collection("rentals");

    useEffect(() => {
        getAllMyPlaces().then(() => setLoading(false))
    }, []);

    useEffect(() => {
        setIsOpen(true);
    }, [deletedPlace]);

    const showModal = (place) => {
        //setIsOpen(true);
        console.log(place);
        setDeletedPlace(place);
    };

    const hideModal = () => {
        setIsOpen(false);
    };

    //Get Rentals from user connected
    async function getAllRentalsFromUser() {
        const snapshot = await refRentalsFromUser.where('owner', '==', user.uid).get();
        const items = [];
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }
        snapshot.forEach(doc => {
            setIdRental(doc.id);
            items.push({id: doc.id, data: doc.data()});
        });
        setRentals(items);
    }


    //GET FUNCTION
    async function getAllMyPlaces() {
        const snapshot = await ref.where('owner', '==', user.uid).get();
        const items = [];
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }

        snapshot.forEach(doc => {

            setIdPlace(doc.id);
            items.push({id: doc.id, data: doc.data()});
        });
        setPlaces(items);
    }

    //DELETE FUNCTION
    function deletePlace(place) {
        setIsOpen(false);

        ref
            .doc(place)
            .delete()
            .catch((err) => {
                console.error(err);
            });
        window.location.reload(true);

        // Simulate submitting to database, shows us values submitted, resets form
        setTimeout(() => {
            alert.show("L'emplacement a été supprimé.");
        }, 500);
    }

    useEffect(() => {
        getAllRentalsFromUser().then();
    }, []);

    if (loading) {
        return (
            <Spinner variant="success" animation="grow" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        );
    }

    function acceptRental(rental) {
        //firestore
        validateRental(rental.id);

        const data = [...rentals];
        const idx = data.findIndex(function (c) {
            return c.id === rental.id;
        });

        data[idx].data.accepted = firebase.firestore.Timestamp.fromDate(new Date());
        data[idx].data.isAccepted = true;

        setRentals(data);

        //Notify the user that the rental has been accepted
        addNotification(
            rental.data.renter,
            "Demande de location acceptée",
            "Votre demande de location a été acceptée, rendez-vous sur la page emplacements loués pour en savoir plus.",
            "locations",
        );
    }

    function refuseRental(rental) {
        //firestore
        deleteRental(rental.id);

        const data = [...rentals];
        const newData = data.filter(r => r !== rental);
        setRentals(newData);
    }

    function checkNew(id) {
        return rentals
            .filter(rental => rental.data.placeId === id)
            .some(item => false === item.data.isAccepted);
    }

    function checkHowManyNew(id) {
        return rentals
            .filter(rental => rental.data.placeId === id)
            .filter(rental => rental.data.isAccepted === false).length
    }



    return (
        <div className="listLocation">
            {places && places.length > 0 ?
                <>
                    {places.map((place) => (
                        <div className="listPlaces" style={{
                            backgroundColor: themes[themeContext.theme].cardContent,
                            boxShadow: themes[themeContext.theme].cardShadow
                        }}>
                            <div className="placeInfoContainer">
                                <Carousel className="myPlacesCarousel" infiniteLoop="true" renderThumbs={function () {
                                    return null
                                }}>
                                    {place.data.photoURLs.map((pic) => (
                                        <img src={pic} className="myplacesImg" alt="lieu"/>
                                    ))}
                                </Carousel>

                                <div className="requestContainer">
                                    <div>
                                        <h3 className="title3_myplaces"
                                            onClick={() => history.push("/details/" + place.id)}>{place.data.titleOffer} </h3>
                                        <h5 className="card-content text-left ">{place.data.price} CHF/{place.data.duration}, {place.data.areaAvailable} m2</h5>
                                        {place.data.isRefused ?
                                            <h6 className="card-content text-left" style={{color: "red"}}>Raison du
                                                refus: {place.data.comment}</h6>
                                            : null}
                                        {(place.data.comment && !place.data.isRefused) || (!place.data.isVerified && !place.data.isRefused) ?
                                            <h6 className="card-content text-left" style={{color: "orangered"}}>Info:
                                                Emplacement en
                                                attente d'approbation</h6>
                                            : null}
                                        <div className="d-flex flex-row mt-5">
                                            <Button className="bg-primary mr-3"
                                                    href={`/edit/${place.id}`}>Modifier</Button>
                                            <Button className="bg-danger mr-3"
                                                    onClick={() => showModal(place)}>Supprimer</Button>
                                            {place.data.isRefused === true ? <Button className="bg-warning mr-3"
                                                                                     onClick={() => {
                                                                                         sendPlaceToValidation(place.data.owner, place.id);
                                                                                         alert.info("Votre place a été soumise pour approbation");
                                                                                     }}>Soumettre</Button> : null}
                                        </div>
                                    </div>

                                    {checkNew(place.id) ?
                                        <div className="requestBadge">
                                            <Badge
                                                badgeContent={checkHowManyNew(place.id)} color="secondary">
                                                <h4><Ba pill variant="info">Demandes</Ba></h4>
                                            </Badge>
                                        </div>
                                        : null}
                                </div>

                            </div>
                            <div>
                                {rentals && rentals.length > 0 ?
                                    <>
                                        <ul className="list-group list-group-flush bg-success">
                                            {rentals
                                                .filter(rental => rental.data.placeId === place.id)
                                                // .sort()
                                                .map((rental, index) => (
                                                    // <div>
                                                    <li className="list-group-item" key={index}
                                                        style={{
                                                            backgroundColor: themes[themeContext.theme].cardContent
                                                        }}>
                                                        <div style={{
                                                            borderColor: themes[themeContext.theme].green,
                                                            borderWidth: "thin",
                                                            borderStyle: "none none solid none"
                                                        }}>
                                                            {window.innerWidth < 460 ?
                                                                <h5 className="mb-0">Location n°{index + 1} | Réf.
                                                                    #{rental.id.substr(0, 3)}...
                                                                    <Tooltip
                                                                        style={{padding: "auto"}}
                                                                        title={<h5>Copier dans le
                                                                            Presse-papiers</h5>}
                                                                        placement="top"
                                                                    ><Button style={{minWidth: "auto"}} onClick={() => {
                                                                        navigator.clipboard.writeText(rental.id).then()
                                                                    }}>📋</Button>
                                                                    </Tooltip>
                                                                </h5> :
                                                                <h5 className="mb-0">Location n°{index + 1} | Réf.
                                                                    #{rental.id}
                                                                    <Tooltip
                                                                        style={{padding: "auto"}}
                                                                        title={<h5>Copier dans le
                                                                            Presse-papiers</h5>}
                                                                        placement="top"
                                                                        // className="mt-0"
                                                                    ><Button style={{minWidth: "auto"}} onClick={() => {
                                                                        navigator.clipboard.writeText(rental.id).then()
                                                                    }}>📋</Button>
                                                                    </Tooltip>
                                                                </h5>
                                                            }
                                                        </div>
                                                        <h5 className="card-content text-left mt-3">Du {rental.data.startDate.toDate().toLocaleDateString('en-GB')} au {rental.data.endDate.toDate().toLocaleDateString('en-GB')}</h5>
                                                        {rental.data.asked ? <h6>Demandé
                                                            le: {rental.data.asked.toDate().toLocaleDateString('en-GB')}</h6> : null}
                                                        {rental.data.isAccepted ? <h6>Approuvé
                                                                le: {rental.data.accepted.toDate().toLocaleDateString('en-GB')}
                                                            </h6> :
                                                            <div className="d-flex flex-row mt-3">
                                                                <Button className="bg-success mr-3 mb-2"
                                                                        onClick={() => acceptRental(rental)}>Accepter</Button>
                                                                <Button className="bg-danger mr-3 mb-2"
                                                                        onClick={() => refuseRental(rental)}>Refuser</Button>
                                                            </div>}
                                                    </li>
                                                ))}
                                        </ul>
                                    </>
                                    :
                                    null
                                }
                            </div>
                            <>
                                {deletedPlace ?
                                <Modal show={isOpen} onHide={hideModal} centered>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Attention !</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>Êtes-vous sur de vouloir supprimer cet emplacement
                                        ? {deletedPlace.data.titleOffer}, {deletedPlace.data.address} </Modal.Body>
                                    <Modal.Footer>
                                        <Button className="bg-primary mr-4" onClick={hideModal}>
                                            Annuler
                                        </Button>
                                        <Button className="bg-danger" onClick={async function () {
                                            await deletePlace(deletedPlace.id)
                                        }}>
                                            Supprimer
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                                    : null}
                            </>
                        </div>
                    ))}
                </>
                :
                <>
                    <h2>Vous n'avez aucune demande ou location confirmée pour cet
                        emplacement</h2>
                </>
            }
        </div>
    );
}