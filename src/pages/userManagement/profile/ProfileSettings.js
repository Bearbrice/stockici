import {Button, ButtonGroup, Col, Form, InputGroup, Modal} from "react-bootstrap";
import {Formik} from "formik";
import React, {useContext, useEffect, useState} from "react";
import * as Yup from "yup";
import {firebaseAuth} from "../../../provider/AuthProvider";
import {useAlert} from "react-alert";
import styled from "styled-components";
import {auth} from "../../../firebase.js"
import {FormControlLabel, Switch} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles';
import {green} from "@material-ui/core/colors";

const Span = styled.span`
  cursor: pointer;

  &:hover {
    background: lightgray;
  }
`

const GreenSwitch = withStyles({
    switchBase: {
        color: green[300],
        '&$checked': {
            color: green[500],
        },
        '&$checked + $track': {
            backgroundColor: green[500],
        },
    },
    checked: {},
    track: {},
})(Switch);

export const ProfileSettings = () => {
    const {
        handleSignout,
        user,
        handleEmailChanging,
        inputs,
        setInputs,
        errors,
        updateUser,
        handleDeleteAccount
    } = useContext(firebaseAuth);
    const alert = useAlert();
    const {email, provider} = user;
    const [disableField, setDisableField] = useState(true);
    const [showForm, setShowForm] = useState(false);
    const [show, setShow] = useState(false);
    const [disablePWD, setDisablePWD] = useState(true);

    function clearPwd() {
        setInputs(
            prevState => ({
                ...prevState,
                password: ''
            })
        );
    }

    /* --- MODAL --- */
    const handleClose = () => {
        setShow(false)
        clearPwd();
    }
    const handleShow = () => {
        setShow(true);
        if (!disableField) {
            setDisableField(!disableField)
        }
        clearPwd();
    }

    const handleValidate = async () => {
        handleDeleteAccount();
        setShow(false);
    }

    //Switch
    function handleSwitch() {
        setDisablePWD(!disablePWD);
    }

    /* --- FORM --- */
    useEffect(() => {
        if (provider === "password") {
            setShowForm(true);
        }

        inputs.email = auth.currentUser.email;

    }, []);

    const handleSubmit = () => {
        handleEmailChanging()
    }
    const handleChange = e => {
        const {name, value} = e.target
        setInputs(prev => ({...prev, [name]: value}))
    }


    /**
     * Validation schema for Yup (Validation for the form with Formik)
     */
    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email("Doit être un email")
            .max(45, "Doit être de 45 caractères ou moins")
            .required("Svp entrez une valeur correcte"),
    });

    return (
        <>
            <Formik
                initialValues={{
                    email: email,
                }}
                validationSchema={validationSchema}
                onSubmit={async (values, {setSubmitting, resetForm}) => {

                    await updateUser('email', inputs.email);
                    await handleSubmit();

                    // When button submits form and form is in the process of submitting, submit button is disabled
                    setSubmitting(true);

                    // Simulate submitting to database, shows us values submitted, resets form
                    setTimeout(() => {
                        // alert.show(JSON.stringify(values, null, 2));
                        alert.show("Email modifié");
                        setDisableField(true);
                        // resetForm();
                        setSubmitting(false);
                    }, 500);
                }}
            >
                {({
                      errors,
                      touched,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                  }) => (
                    <div>
                        <h1>Réglages du compte</h1>
                        <Form onSubmit={handleSubmit} style={{width: "100%"}}>

                            {showForm ? null :
                                <div>
                                    <h2>Vous êtes connectés avec:</h2>
                                    <h3 style={{textAlign: "center"}}>{provider}</h3>
                                </div>
                            }

                            {showForm ?
                                <>
                                    {/**EMAIL*/}
                                    <Form.Row className="align-items-center" style={{
                                        alignItems: "center",
                                        justifyContent: "center", marginTop: "40px"
                                    }}>
                                        <Col xs="auto">
                                            <Form.Label htmlFor="inlineFormInputGroup" srOnly>
                                                Username
                                            </Form.Label>
                                            <InputGroup className="mb-2">
                                                <Form.Control
                                                    style={{textAlign: "center"}}
                                                    type="email"
                                                    name="email"
                                                    placeholder="Adresse email"
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={inputs.email}

                                                    className={touched.email && errors.email ? "has-error" : null}
                                                    disabled={disableField}
                                                />

                                                <div className="input-group-prepend" onClick={() => {
                                                    setDisableField(!disableField)
                                                }}>
                                                    <Span className="input-group-text">✏️</Span>
                                                </div>
                                            </InputGroup>
                                            {touched.email && errors.email ? (
                                                <div className="error-message">{errors.email}</div>
                                            ) : null}
                                        </Col>
                                    </Form.Row>

                                    {/**PWD*/}
                                    {!disableField ?
                                        <Form.Row className="align-items-center" style={{
                                            alignItems: "center",
                                            justifyContent: "center",
                                            marginTop: "20px"
                                        }}>
                                            <Col xs="auto">
                                                <Form.Label htmlFor="inlineFormInputGroup" srOnly>
                                                    Username
                                                </Form.Label>
                                                <InputGroup className="mb-2">
                                                    <Form.Control
                                                        style={{textAlign: "center"}}
                                                        type="password"
                                                        name="password"
                                                        placeholder="Mot de passe"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={inputs.password}
                                                        className={touched.password && errors.password ? "has-error" : null}
                                                        disabled={disableField}
                                                    />
                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text">🔒</span>
                                                    </div>
                                                </InputGroup>
                                                {touched.password && errors.password ? (
                                                    <div className="error-message">{errors.password}</div>
                                                ) : null}
                                            </Col>
                                        </Form.Row> : null}
                                </> : null}

                            <div style={{textAlign: "center", marginTop: "20px",}}>
                                <ButtonGroup vertical style={{alignSelf: "center"}}>
                                    {showForm ?
                                        <Button variant="success" type="submit" disabled={isSubmitting || disableField}>
                                            Changer d'e-mail
                                        </Button> : null}
                                    <Button variant="dark" style={{
                                        marginBottom: "20px",
                                        marginTop: "20px",
                                    }}
                                            onClick={handleShow}
                                    >
                                        Supprimer compte
                                    </Button>
                                    <Button variant="danger"
                                            onClick={() => {
                                                handleSignout();
                                                alert.success('Vous êtes déconnecté')
                                            }}>
                                        Se déconnecter
                                    </Button>
                                </ButtonGroup>
                            </div>
                        </Form>
                    </div>
                )}
            </Formik>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Suppression du compte</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Row className="align-items-center" style={{
                        alignItems: "center",
                        justifyContent: "center", marginTop: "40px"
                    }}>
                        <h4><b>Important</b></h4>
                        <p>Si vous supprimez votre compte vous n'aurez plus accès au site et toutes vos données seront
                            supprimées et donc perdues.</p>
                        <h4 style={{color: "orangered"}}><b>Attention</b></h4>
                        <p style={{color: "red"}}>Cette action est <u>irréversible</u>. Votre compte et toutes vos
                            données seront supprimées sur le champ et vous ne pourrez plus y accéder. Nous serons dans
                            l'incapacité de les récupérer.</p>
                        <FormControlLabel
                            value="top"
                            control={
                                <GreenSwitch
                                    color="secondary"
                                    checked={!disablePWD}
                                    onChange={handleSwitch}
                                />
                            }
                            label="J'ai pris connaissance des avertissements"
                            labelPlacement="top"
                        />
                        <Col xs="auto">

                            <InputGroup className="mb-2">
                                <Form.Control
                                    style={{textAlign: "center"}}
                                    type="password"
                                    name="password"
                                    placeholder="Mot de passe"
                                    onChange={handleChange}
                                    value={inputs.password}
                                    disabled={disablePWD}
                                />
                                <div className="input-group-prepend">
                                    <span className="input-group-text">🔒</span>
                                </div>
                            </InputGroup>
                            {errors.length > 0 ? errors.map(error => <p style={{color: 'red'}}>{error}</p>) : null}
                        </Col>
                    </Form.Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Annuler
                    </Button>
                    <Button variant="success" onClick={handleValidate}>
                        Confirmer
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}