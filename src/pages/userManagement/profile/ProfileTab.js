import {Tab} from "react-bootstrap";
import {ProfileInfo} from "./ProfileInfo";
import {ProfileSettings} from "./ProfileSettings";
import React, {useContext} from "react";
import {CustomTabs} from "../AccountPage";
import {ThemeContext, themes} from "../../../context/ThemeContext";

export function ProfileTab() {
    const themeContext = useContext(ThemeContext);
    return (
        <div className="profileTab">
            <CustomTabs defaultActiveKey="profile" id="custom-tabs">
                <Tab eventKey="profile" title="Profil">
                    <div
                        style={{backgroundColor: themes[themeContext.theme].cardContent}}
                        className="flex border flex-col items-center md:flex-row md:items-start border-blue-400 px-3 py-4">
                        <ProfileInfo/>
                    </div>
                </Tab>
                <Tab eventKey="user" title="Utilisateur">
                    <div
                        style={{backgroundColor: themes[themeContext.theme].cardContent}}
                        className="flex border flex-col items-center md:flex-row md:items-start border-blue-400 px-3 py-2">
                        <ProfileSettings/>
                    </div>
                </Tab>
            </CustomTabs>
        </div>
    );
}