import {Button, ButtonGroup, Col, Form, Modal} from "react-bootstrap";
import {Formik} from "formik";
import React, {useContext, useState} from "react";
import * as Yup from "yup";
import {firebaseAuth} from "../../../provider/AuthProvider";
import {useAlert} from "react-alert";
import ImageUploader from 'react-images-upload';
import firebase from "../../../firebase";
import "./ProfileInfo.css"
import {deleteProfilPicture} from "../../../services/StoreMethods";
import {Checkbox, FormControlLabel, Tooltip, withStyles} from "@material-ui/core";
import {Info} from "@material-ui/icons";
import {green} from "@material-ui/core/colors";

const GreenCheckbox = withStyles({
    root: {
        color: green[400],
        '&$checked': {
            color: green[600],
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

export const ProfileInfo = () => {
    const {user, updateUser, updateUserInfo} = useContext(firebaseAuth);
    const alert = useAlert();
    const {photoURL, firstname, lastname, address, NPA, city, iban, bank} = user;
    const [show, setShow] = useState(false);
    const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
    const [pictures, setPictures] = useState([]);
    const [preview, setPreview] = useState();

    const handleClose = () => {
        setShow(false)
        clearImage()
    }
    const handleCloseDelete = () => {
        setShowDeleteConfirmation(false)
    }
    const handleShow = () => setShow(true);

    const handleDeletePicture = async () => {
        await deleteProfilPicture(photoURL);
        await updateUser('photoURL', "");
        setShowDeleteConfirmation(false);
        alert.success("La photo a été supprimée")
    }

    async function updateImage() {
        const storageRef = firebase.storage().ref('users')
        const type = pictures[0].type
        const extension = type.substring(6, type.length)
        const fileName = user.uid + '.' + extension;

        const fileRef = storageRef.child(fileName)
        fileRef
            .put(pictures[0])
            .then(async (snap) => {
                console.log('upload successful', snap)

                const url = await fileRef.getDownloadURL()
                await updateUser('photoURL', url);

            })
            .catch((err) => console.error('error uploading file', err))
    }

    function clearImage() {
        setPictures([]);
        setPreview(null);
    }

    const handleValidate = async () => {
        await updateImage().then();

        setShow(false);
        alert.info("Votre photo a bien été mise à jour");
        clearImage()
    }

    const onDrop = picture => {
        setPictures(picture);
        setPreview(URL.createObjectURL(picture[0]));
    };

    /**
     * Validation schema for Yup (Validation for the form with Formik)
     */
    const validationSchema = Yup.object().shape({
        firstname: Yup.string()
            .max(20, "Doit être de 20 caractères ou moins")
            .required("Svp entrez une valeur correcte"),
        lastname: Yup.string()
            .max(20, "Doit être de 20 caractères ou moins")
            .required("Svp entrez une valeur correcte"),
        address: Yup.string()
            .max(30, "Doit être de 30 caractères ou moins")
            .required("Svp entrez une valeur correcte"),
        NPA: Yup.number()
            .min(1000, 'Doit contenir 4 chiffres')
            .max(9999, 'Doit contenir 4 chiffres')
            .required("Svp entrez une valeur correcte"),
        city: Yup.string()
            .max(20, "Doit être de 20 caractères ou moins")
            .required("Svp entrez une valeur correcte"),
        checked: Yup.boolean(),
        iban: Yup.string()
            .when("checked", (checked) => {
                if (checked)
                    return Yup.string()
                        .required("Valeur manquante")
                        .min(16, "Doit être de 16 caractères ou plus")
                        .max(26, "Doit être de 26 caractères ou moins")
            }),
        bank: Yup.string()
            .when("checked", (checked) => {
                if (checked)
                    return Yup.string()
                        .required("Valeur manquante")
                        .min(4, "Doit être de 4 caractères ou plus")
                        .max(26, "Doit être de 26 caractères ou moins")
            }),
    });

    return (
        <>
            <div style={{textAlign: "center"}}>
                <div
                    style={{
                        backgroundImage: `url(${photoURL || 'https://img.icons8.com/dusk/512/000000/question--v1.png'})`,
                        backgroundRepeat: "no-repeat",
                        backgroundSize: "contain",
                        height: "200px",
                        width: "300px",
                        display: "inline-block"
                    }}
                />
                <div>
                    {photoURL === "" || photoURL == null ?
                        <div><h6 style={{color: "darkred"}}>Svp, ajoutez votre photo</h6></div> :
                        <Button variant="danger" onClick={() => {
                            setShowDeleteConfirmation(true)
                        }}>
                            <span>❌</span>
                        </Button>}
                    <Button variant="success" onClick={handleShow} style={{textAlign: "center", marginLeft: "10px"}}>
                        <span>📥</span>
                    </Button>
                </div>
            </div>

            <Modal show={showDeleteConfirmation} onHide={handleCloseDelete}>
                <Modal.Header>Êtes-vous sûr de vouloir supprimer votre photo ?</Modal.Header>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseDelete}>
                        Annuler
                    </Button>
                    <Button variant="outline-warning" onClick={handleDeletePicture}>Confirmer</Button>
                </Modal.Footer>
            </Modal>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Choississez une photo</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {!(pictures && pictures.length) ?
                        <ImageUploader
                            withIcon={true}
                            onChange={onDrop}
                            imgExtension={[".jpg", ".png"]}
                            maxFileSize={5242880}
                            fileSizeError="Le fichier est trop gros"
                            singleImage={true}
                            buttonText="Choisir une image"
                            label={<>
                                <p>Taille maximum de fichier acceptée: 5mb.</p>
                                <p> Acceptés : JPG et PNG </p><p>Un seul fichier autorisé</p></>}
                        />
                        : null}
                    {pictures && pictures.length ?
                        <div style={{textAlign: "center"}}>
                            <img alt="Aperçu" height="200px" width="200px" src={preview}/>
                            <h3>{pictures[0].name}</h3>
                            <Button variant="outline-danger" onClick={clearImage}>
                                <span>❌</span>
                            </Button>
                        </div>
                        : null}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Annuler
                    </Button>
                    <Button variant="success" onClick={handleValidate}>
                        Confirmer
                    </Button>
                </Modal.Footer>
            </Modal>
            <Formik
                initialValues={{
                    firstname: firstname,
                    lastname: lastname,
                    address: address,
                    NPA: NPA,
                    city: city,
                    iban: iban,
                    bank: bank,
                    checked: !!iban
                }}
                validationSchema={validationSchema}
                onSubmit={async (values, {setSubmitting, resetForm}) => {

                    await updateUserInfo(values.firstname, values.lastname, values.address, values.NPA, values.city, values.iban, values.bank, true)

                    // When button submits form and form is in the process of submitting, submit button is disabled
                    setSubmitting(true);

                    // Simulate submitting to database, shows us values submitted, resets form
                    setTimeout(() => {

                        // alert.show(JSON.stringify(values, null, 2));
                        alert.show("Vos modifications ont été enregistrées");
                        // alert.show(user.toString());
                        // resetForm();
                        setSubmitting(false);
                    }, 500);
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                  }) => (

                    <Form onSubmit={handleSubmit} style={{width: "100%", textAlign: "center"}}>

                        {/*Firstname*/}
                        <Form.Row>
                            <Col>
                                <Form.Group controlId="formFirstname">
                                    <Form.Label style={{color: 'black'}}>Prénom</Form.Label>
                                    <Form.Control
                                        style={{textAlign: "center"}}
                                        type="text"
                                        name="firstname"
                                        placeholder="Prénom"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.firstname}
                                        className={touched.firstname && errors.firstname ? "has-error" : null}
                                    />
                                    {touched.firstname && errors.firstname ? (
                                        <div className="error-message">{errors.firstname}</div>
                                    ) : null}
                                </Form.Group>
                            </Col>
                            <Col>
                                {/*Lastname*/}
                                <Form.Group controlId="formLastname">
                                    <Form.Label style={{color: 'black'}}>Nom</Form.Label>
                                    <Form.Control
                                        style={{textAlign: "center"}}
                                        type="text"
                                        name="lastname"
                                        placeholder="Nom de famille"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.lastname}
                                        className={touched.lastname && errors.lastname ? "has-error" : null}
                                    />
                                    {touched.lastname && errors.lastname ? (
                                        <div className="error-message">{errors.lastname}</div>
                                    ) : null}
                                </Form.Group>
                            </Col>
                        </Form.Row>

                        {/*Address*/}
                        <Form.Group controlId="formAddress">
                            <Col style={{padding: "0px"}}>
                                <Form.Label style={{color: 'black'}}>Adresse
                                    <Tooltip title={<h5>Votre adresse ne sera divulguée que lors de réservation</h5>}
                                             placement="top"
                                             enterTouchDelay="50"
                                             className="ml-2">
                                        <Info/>
                                    </Tooltip>
                                </Form.Label>
                                <Form.Control
                                    style={{textAlign: "center"}}
                                    type="text"
                                    name="address"
                                    placeholder="Adresse"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.address}
                                    className={touched.address && errors.address ? "has-error" : null}
                                />
                                {touched.address && errors.address ? (
                                    <div className="error-message">{errors.address}</div>
                                ) : null}</Col>
                        </Form.Group>

                        <Form.Row>
                            <Col>
                                {/*NPA*/}
                                <Form.Group controlId="formNPA">
                                    <Form.Label style={{color: 'black'}}>NPA (CH)
                                    </Form.Label>
                                    <Form.Control
                                        style={{textAlign: "center"}}
                                        type="number"
                                        name="NPA"
                                        placeholder="Code postal"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.NPA}
                                        className={touched.NPA && errors.NPA ? "has-error" : null}
                                    />
                                    {touched.NPA && errors.NPA ? (
                                        <div className="error-message">{errors.NPA}</div>
                                    ) : null}
                                </Form.Group>
                            </Col>
                            <Col>
                                {/*city*/}
                                <Form.Group controlId="formCity">
                                    <Form.Label style={{color: 'black'}}>Ville/Village</Form.Label>
                                    <Form.Control
                                        style={{textAlign: "center"}}
                                        type="text"
                                        name="city"
                                        placeholder="Ville/Village"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.city}
                                        className={touched.city && errors.city ? "has-error" : null}
                                    />
                                    {touched.city && errors.city ? (
                                        <div className="error-message">{errors.city}</div>
                                    ) : null}
                                </Form.Group>
                            </Col>
                        </Form.Row>

                        <FormControlLabel
                            control={<GreenCheckbox checked={values.checked}
                                                    onChange={handleChange}
                                                    name="checked"/>}
                            label="J'ai des biens à disposition"
                        />

                        {values.checked ?
                            <Form.Row>
                                <Col>
                                    <Form.Group controlId="formIban">
                                        <Form.Label style={{color: 'black'}}>IBAN
                                            <Tooltip
                                                title={<h5>C'est là que vous recevrez l'argent de vos locations</h5>}
                                                placement="top"
                                                enterTouchDelay="50"
                                                className="ml-2">
                                                <Info/>
                                            </Tooltip>
                                        </Form.Label>
                                        <Form.Control
                                            style={{textAlign: "center"}}
                                            type="text"
                                            name="iban"
                                            placeholder="IBAN"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.iban}
                                            className={touched.iban && errors.iban ? "has-error" : null}
                                        />
                                        {touched.iban && errors.iban ? (
                                            <div className="error-message">{errors.iban}</div>
                                        ) : null}
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group controlId="formBank">
                                        <Form.Label style={{color: 'black'}}>Banque
                                            <Tooltip title={<h5>Seuls les IBANs et banques suisses sont acceptés</h5>}
                                                     placement="top"
                                                     enterTouchDelay="50"
                                                     className="ml-2">
                                                <Info/>
                                            </Tooltip>
                                        </Form.Label>
                                        <Form.Control
                                            style={{textAlign: "center"}}
                                            type="text"
                                            name="bank"
                                            placeholder="Nom de la banque"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.bank}
                                            className={touched.bank && errors.bank ? "has-error" : null}
                                        />
                                        {touched.bank && errors.bank ? (
                                            <div className="error-message">{errors.bank}</div>
                                        ) : null}
                                    </Form.Group>
                                </Col>
                            </Form.Row> : null}

                        <div style={{textAlign: "center"}}>
                            <ButtonGroup vertical style={{alignSelf: "center"}}>
                                <Button variant="success" type="submit" disabled={isSubmitting}
                                        style={{
                                            marginTop: "40px",
                                        }}>Valider les
                                    modifications
                                </Button>
                            </ButtonGroup>
                        </div>
                    </Form>
                )}
            </Formik>
        </>
    );
}