// add useContext
import React, {useContext, useEffect, useState} from 'react';
import {firebaseAuth} from "../../../provider/AuthProvider";
import {ButtonGoogle} from "../../../components/Styled-Reusable/GoogleButton";
import {withRouter} from "react-router-dom";
import {Button, Spinner} from "react-bootstrap";

const Signup = (props) => {

    const {handleSignup, inputs, setInputs, errors, user} = useContext(firebaseAuth)
    const [loading, setLoading] = useState(false);

    /* --- FORM --- */
    useEffect(() => {
        // Push home only if the registration is successful
        if (user !== undefined) {
            props.history.push('/home')
        }
    }, [user]);

    const handleSubmit = async (e) => {
        setLoading(true);
        e.preventDefault()
        //wait to signup
        await handleSignup()
    }
    const handleChange = e => {
        const {name, value} = e.target
        setInputs(prev => ({...prev, [name]: value}))
    }

    return (
        <>
            <h1>Inscription</h1>
            {!loading ?
                <>
                    <form onSubmit={handleSubmit} className="text-center">
                        {/* make inputs  */}
                        <div className="form-group">
                            <input type="email" className="form-control" onChange={handleChange} name="email"
                                   placeholder='email'
                                   value={inputs.email}/>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" onChange={handleChange} name="password"
                                   placeholder='password' value={inputs.password}/>
                        </div>
                        <Button variant="success" type="submit">Sign up</Button>
                        {errors.length > 0 ? errors.map(error => <p style={{color: 'red'}}>{error}</p>) : null}
                    </form>

                    <ButtonGoogle label={"S'inscrire avec Google"}/>
                </>
                :
                <>{errors.length > 0 ? setLoading(false) :
                    <>
                        <h2>Chargement en cours...</h2>
                        <Spinner animation="grow" variant="success" role="status"
                                 style={{width: "8rem", height: "8rem"}}>
                            <span className="sr-only">Chargement...</span>
                        </Spinner></>}
                </>
            }
        </>
    );
};

export default withRouter(Signup);