import React, {useContext, useState} from 'react';
import {firebaseAuth} from "../../../provider/AuthProvider";
import {Link} from "react-router-dom";
import {ButtonGoogle} from "../../../components/Styled-Reusable/GoogleButton";
import {Button, Spinner} from "react-bootstrap";

const Signin = () => {
    const {handleSignin, inputs, setInputs, errors} = useContext(firebaseAuth)
    const [loading, setLoading] = useState(false);

    // Everytime the user is updated we write it to the DB
    // useEffect(() => {
    //     if (!(token === null)) {
    //         setLoading(false);
    //     } else {
    //         setLoading(true);
    //     }
    // }, [token]);

    const handleSubmit = (e) => {
        setLoading(true);
        e.preventDefault()
        handleSignin()
    }
    const handleChange = e => {
        const {name, value} = e.target
        setInputs(prev => ({...prev, [name]: value}))
    }

    return (
        <>
            <h1>Se connecter</h1>
            {/*{!loading ?*/}
            {/*    <div className="text-center">*/}
            {!loading ?
                <>
                    <form onSubmit={handleSubmit} id="signIn" className="text-center">
                        {/* make inputs  */}
                        <div className="form-group">
                            <input type="email" className="form-control" onChange={handleChange} name="email"
                                   placeholder='email'
                                   value={inputs.email}/></div>

                        <div className="form-group">
                            <input type="password" className="form-control" onChange={handleChange} name="password"
                                   placeholder='password' value={inputs.password}/>
                        </div>
                        <Button variant="success" type="submit">Sign in</Button>
                        {errors.length > 0 ? errors.map(error => <p style={{color: 'red'}}>{error}</p>) : null}
                    </form>

                    {/*<ButtonGoogle/>*/}
                    <ButtonGoogle label={"Se connecter avec Google"}/>

                    <p>Pas encore de compte ?</p>
                    <Link
                        to="/signup"
                    >Inscrivez-vous !
                    </Link>
                </>
                :

                <>{errors.length > 0 ? setLoading(false) :
                    <>
                        <h2>Chargement en cours...</h2>
                        <Spinner animation="grow" variant="success" role="status"
                                 style={{width: "8rem", height: "8rem"}}>
                            <span className="sr-only">Chargement...</span>
                        </Spinner></>}
                </>
            }
        </>
    );
};

export default Signin;