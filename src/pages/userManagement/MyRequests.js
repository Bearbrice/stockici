import React, {useContext, useEffect, useState} from "react";
import {firebaseAuth} from "../../provider/AuthProvider";
import {useAlert} from "react-alert";
import firebase from "../../firebase";
import {Modal, Spinner} from "react-bootstrap";
import {Carousel} from "react-responsive-carousel";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router-dom";

export function MyRequests() {
    const [place, setPlace] = useState(null);
    const [idPlace, setIdPlace] = useState();
    const [places, setPlaces] = useState([]);
    const [comments, setComments] = useState([]);

    const [comment, setComment] = useState("");

    const [idRental, setIdRental] = useState();
    const [rentals, setRentals] = useState([]);
    const [isOpen, setIsOpen] = useState(false);

    const [loading, setLoading] = useState(true);
    const {user} = useContext(firebaseAuth);
    const alert = useAlert();
    const history = useHistory();

    //reference to the collections in firebase
    const refRentalsFromUser = firebase.firestore().collection("rentals");
    const refPlaces = firebase.firestore().collectionGroup("places");

    useEffect(() => {
        getAllRentalsFromUser();
    }, []);

    useEffect(() => {
        getAllPlacesFromRentals().then(() => {

                setLoading(false);
            }
        )
    }, [rentals]);

    useEffect(() => {
        setIsOpen(true);
    }, [place]);


    function handleCommentChange(e) {
        setComment(e.target.value);
    }

    const showModal = (place) => {
        setPlace(place);
    };

    const hideModal = () => {
        setIsOpen(false);
    };

    //Get Rentals from user connected
    async function getAllRentalsFromUser() {
        const snapshot = await refRentalsFromUser.where('owner', '==', user.uid).get();
        const items = [];
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }
        snapshot.forEach(doc => {
            setIdRental(doc.id);
            items.push({id: doc.id, data: doc.data()});
        });
        setRentals(items);
    }

    //Get Places from rental
    async function getAllPlacesFromRentals() {
        const items = [];
        await Promise.all(rentals.map(async (rental) => {

                const snapshot = await refPlaces.where('id', '==', rental.data.placeId).get();

                if (snapshot.empty) {
                    console.log('No matching documents.');
                    return;
                }
                snapshot.forEach(doc => {
                    setIdPlace(doc.id);
                    items.push({id: doc.id, data: doc.data()});
                });
            }
        ))

        setPlaces(items);
    }

    if (loading) {
        return (
            <Spinner variant="success" animation="grow" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        );
    }


    return (
        <div className="listLocation">
            {!loading ?
                <>
                    {rentals && rentals.length > 0 ?
                        <>
                            {rentals.map((rental) => (
                                <div className="card listPlaces d-flex flex-row">
                                    <Carousel className="mr-4 w-25" infiniteLoop="true" renderThumbs={function () {
                                        return null
                                    }}>
                                    </Carousel>
                                    <div>
                                        <h3 className="btn-link"
                                            onClick={() => history.push("/details/" + rental.data.placeId)}>{rental.data.placeId}</h3>
                                        <h5 className="card-content text-left ">Du {rental.data.startDate.toString()} au {rental.data.endDate.toString()}</h5>
                                        <div className="d-flex flex-row mt-5">

                                            <Button className="bg-success mr-3 mb-2"
                                                    onClick={() => showModal(place)}>Accepter</Button>
                                            <Button className="bg-danger mr-3 mb-2"
                                                    onClick={() => showModal(place)}>Refuser</Button>
                                            <Button className="bg-warning mr-3 mb-2"
                                                    onClick={() => console.log(places)}>Log places</Button>
                                        </div>
                                    </div>
                                </div>

                            ))}
                        </>
                        :
                        <>
                            <h2 style={{backgroundColor: 'transparent'}}>Vous n'avez loué aucun emplacement !</h2>
                        </>
                    }
                </>
                : null}

            {place ?

                <Modal show={isOpen} onHide={hideModal} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Commenter: {place.data.titleOffer}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <textarea
                            className="form-control"
                            onChange={handleCommentChange}
                        />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="bg-primary mr-4" onClick={async function (event) {
                            comments.push(comment);
                        }}>
                            Confirmer
                        </Button>
                        <Button className="bg-danger" onClick={hideModal}>
                            Annuler
                        </Button>
                    </Modal.Footer>
                </Modal>

                : null}
        </div>
    );
}