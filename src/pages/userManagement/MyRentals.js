import Button from "@material-ui/core/Button";
import {Modal, Spinner} from "react-bootstrap";
import React, {useContext, useEffect, useState} from "react";
import firebase from "../../firebase";
import {useAlert} from "react-alert";
import {firebaseAuth} from "../../provider/AuthProvider";
import {Carousel} from "react-responsive-carousel";
import {useHistory} from "react-router";
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import {ThemeContext, themes} from "../../context/ThemeContext";

export function MyRentals() {
    const themeContext = useContext(ThemeContext);
    let history = useHistory();
    let currentPage = window.location.href;
    const [place, setPlace] = useState(null);
    const [idPlace, setIdPlace] = useState();
    const [places, setPlaces] = useState([]);
    const [comments, setComments] = useState([]);
    const [comment, setComment] = useState("");
    const [idRental, setIdRental] = useState();
    const [rentals, setRentals] = useState([]);
    const [isOpen, setIsOpen] = useState(false);

    const [loading, setLoading] = useState(true);
    const {user} = useContext(firebaseAuth);
    const alert = useAlert();
    const [value, setValue] = useState(0);

    //Get current date
    var q = new Date();
    var m = q.getMonth();
    var d = q.getDate();
    var y = q.getFullYear();
    var currentDate = new Date(y, m, d);

    //Get end date from rental
    var rentalEndDate1MonthLater = new Date();

    //and after 1 month
    var rentalDateEnd = new Date();

    //references to the collections in firebase
    const refRentalsFromUser = firebase.firestore().collection("rentals");
    const refPlaces = firebase.firestore().collectionGroup("places");
    const refComments = firebase.firestore().collectionGroup("comments");

    useEffect(() => {
        getAllRentalsFromUser();
    }, []);

    useEffect(() => {
        getAllCommentsFromOnePlace();
    }, [comments]);

    useEffect(() => {
        getAllPlacesFromRentals().then(() => setLoading(false))
    }, [rentals]);

    useEffect(() => {
        setIsOpen(true);
    }, [place]);


    function handleCommentChange(e) {
        setComment(e.target.value);
    }

    const showModal = (place) => {
        setPlace(place);
    };

    const hideModal = () => {
        setIsOpen(false);
    };

    const pdfGenerator = (id, data) => {
        let url = currentPage + "/contract/" + id;
        window.open(url, '_blank', "width=1400,height=1000,location=no,menubar=no,status=no,titilebar=no,resizable=no")
    }

    //Get Rentals from user connected
    async function getAllRentalsFromUser() {
        const snapshot = await refRentalsFromUser.where('renter', '==', user.uid).where('isAccepted', '==', true).get();
        const items = [];
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }
        snapshot.forEach(doc => {
            setIdRental(doc.id);
            items.push({ id: doc.id, data: doc.data() });
        });
        setRentals(items);
    }

    //Get Places from rental
    async function getAllPlacesFromRentals() {
        const items = [];

        //Have to use Promise to wait until the rentals array is read
        await Promise.all(rentals.map(async (rental) => {

                const snapshot = await refPlaces.where('id', '==', rental.data.placeId).get();

                if (snapshot.empty) {
                    console.log('No matching documents.');
                    return;
                }
                snapshot.forEach(doc => {
                    setIdPlace(doc.id);
                    items.push({id: doc.id, data: doc.data()});
                });
            }
        ))
        setPlaces(items);
    }

    //Get comments from oneplace
    async function getAllCommentsFromOnePlace() {
        const items = [];

        //Have to use Promise to wait until the place array is read
        await Promise.all(places.map(async (place) => {

                const snapshot = await refComments.where('placeId', '==', place.data.id).get();

                if (snapshot.empty) {
                    console.log('No matching documents.');
                    return;
                }
                // console.log("comments: ")
                snapshot.forEach(doc => {
                    setIdPlace(doc.id);
                    items.push({id: doc.id, data: doc.data()});
                    // console.log(doc.data());
                });
            }
        ))
        setComments(items);
    }

    async function sendComment(place, id) {
        setIsOpen(false);
        const newComment = {
            comment: comment,
            isValidate: false,
            writtenDate: new Date().toLocaleDateString(),
            validateDate: '-',
            writterId: user.uid,
            placeOwner: place.owner,
            placeId: id,
            rating: value
        };

        const refComments = firebase.firestore().collection("users").doc(place.owner).collection("places").doc(id).collection("comments");

        await refComments.doc(newComment.id).set(newComment);

        setTimeout(() => {
            alert.show("Commentaire ajouté");
        }, 500);
    }

    if (loading) {
        return (
            <Spinner variant="success" animation="grow" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        );
    }

    return (
        <div className="listLocation">
            {!loading ?
                <>
                    {places && places.length > 0 ?
                        <>
                            {places.map((place) => (
                                <div className="listPlaces" style={{
                                    backgroundColor: themes[themeContext.theme].cardContent,
                                    boxShadow: themes[themeContext.theme].cardShadow
                                }}>
                                    <div className="placeInfoContainer">
                                        <Carousel className="myPlacesCarousel" infiniteLoop="true"
                                                  renderThumbs={function () {
                                                      return null
                                                  }}>
                                            {place.data.photoURLs.map((pic) => (
                                                <img src={pic} className="myplacesImg" alt="lieu"/>
                                            ))}
                                        </Carousel>

                                        <div>
                                            <h3 className="card-title">{place.data.titleOffer}</h3>
                                            <h5 className="card-content text-left ">{place.data.price} CHF/{place.data.duration}, {place.data.areaAvailable} m2</h5>
                                            {rentals.map((rental) => {
                                                //Create the date format for all dates objects
                                                rentalEndDate1MonthLater = new Date(rental.data.endDate.toDate());
                                                rentalDateEnd = new Date(rental.data.endDate.toDate());

                                                //Set the hours to 0, in order to compare properly the dates
                                                currentDate.setHours(0, 0, 0, 0);
                                                rentalDateEnd.setHours(0, 0, 0, 0);
                                                rentalEndDate1MonthLater.setHours(0, 0, 0, 0);

                                                //Add 1 month to the rentalEndDate
                                                rentalEndDate1MonthLater.setDate(rentalEndDate1MonthLater.getDate() + 30);


                                                //Calculate the difference to see if the current date is bigger or not
                                                var diff = (rentalEndDate1MonthLater - currentDate);

                                                if (rental.data.placeId === place.data.id)
                                                    return [
                                                        <h5 className="card-content text-left">
                                                            Du {rental.data.startDate.toDate().toLocaleDateString('en-GB')} &nbsp;
                                                            au &nbsp;
                                                            {rental.data.endDate.toDate().toLocaleDateString('en-GB')}
                                                        </h5>,

                                                        //After the rental end date has passed
                                                        (rentalDateEnd <= currentDate &&
                                                            //During 1month
                                                            diff >= 0) &&
                                                        <>
                                                            {/* After the dates are okay, it will check if the user has already comment this place or not*/}
                                                            {comments
                                                                .filter(com =>
                                                                    (com.data?.writterId === user.uid && place.data.id === com.data?.placeId)).length > 0 ?
                                                                <h5>[Vous avez déjà commenté]</h5>
                                                                :
                                                                <Button className="bg-success mr-3 mb-2"
                                                                        onClick={() => showModal(place)}>Commenter
                                                                </Button>
                                                            }
                                                        </>
                                                    ];
                                            })}
                                            <Button className="bg-info mr-3 mb-2"
                                                    onClick={() => pdfGenerator(place.id, place.data)}>Afficher le
                                                contrat</Button>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </>
                        :
                        <>
                            <h2 style={{backgroundColor: 'transparent'}}>Vous n'avez loué aucun emplacement !</h2>
                        </>
                    }
                </>
                : null}

            {place ?

                <Modal show={isOpen} onHide={hideModal} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Commenter: {place.data.titleOffer}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <textarea
                            className="form-control"
                            onChange={handleCommentChange}
                        />

                        <Box component="fieldset" mb={3} borderColor="transparent">
                            <Rating
                                name="simple-controlled"
                                value={value}
                                onChange={(event, newValue) => {
                                    setValue(newValue);
                                }}
                            />
                        </Box>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="bg-primary mr-4" onClick={async function (event) {
                            comments.push(comment);
                            await sendComment(place.data, place.data.id);
                        }}>
                            Confirmer
                        </Button>
                        <Button className="bg-danger" onClick={hideModal}>
                            Annuler
                        </Button>
                    </Modal.Footer>
                </Modal>

                : null}
        </div>
    );
}