import React from "react";
import LogoHES from "../assets/HESLOGO.png"; // with import
import {Card, CardContent} from "@material-ui/core";
import {CardTitle} from "reactstrap";

export default function Terms() {
    return (
        <div className="terms">
            <Card className="cardTerms">
                <CardTitle>
                    <h1 className="title1 termsTitle">Conditions Générales d’Utilisation et de Vente du site
                        stock-ici.ch</h1>
                </CardTitle>
                <CardContent>
                    <ol className="termsNumerotation">
                        <li className="title2">Objet et acceptation des Conditions Générales d’Utilisation</li>
                        <p className="termsParagraph">Stock-ici.ch est un site internet (le Site) mettant en relation
                            deux
                            utilisateurs
                            disposant d'un compte
                            sur le site (les Utilisateurs):</p>

                        <p className="termsParagraph">D’un côté, un Utilisateur (le Loueur) souhaitant soit :</p>
                        <ul className="termsList">
                            <li>Recevoir de façon temporaire des biens (au moyen d'un Contrat de Dépôt)</li>
                            <li>Mettre à disposition de façon temporaire un espace destiné à l’entreposage (au moyen
                                d'un
                                Contrat de
                                Location)
                            </li>
                        </ul>

                        <p className="termsParagraph">D’un autre côté, un Utilisateur (le Stockeur) souhaitant soit
                            :</p>
                        <ul className="termsList">
                            <li>Déposer des biens de façon temporaire (le Dépôt)</li>
                            <li>Occuper de façon temporaire un espace destiné à l’entreposage (la Location)
                            </li>
                        </ul>

                        <p className="termsParagraph">Stock-ici n’est partie à aucune transaction entre le loueur et le
                            stockeur. Le Contrat
                            de Location ou de
                            Dépôt est directement conclu respectivement entre le loueur et le stockeur.</p>

                        <p className="termsParagraph">Le rôle de stock-ci est exclusivement :</p>
                        <ul className="termsList">
                            <li>De créer et maintenir le site www.stock-ici.ch qui permet de mettre en relation, par
                                l’intermédiaire
                                du Site, le Loueur et le Stockeur
                            </li>
                            <li>De réaliser, en collaboration avec son prestataire de paiement X, le paiement entre les
                                Utilisateurs
                            </li>
                            <li>D'avoir souscrit une assurance pour le compte du stockeur, qui permet à ces derniers
                                d’être
                                garantis
                                dans les conditions et limites du contrat.
                            </li>
                            <li>Sur demande expresse du Loueur, et après acceptation par stock-ici, cette dernière
                                pourra
                                répondre
                                pour lui aux demandes, signer le contrat de location ou de dépôt, selon les spécificités
                                convenues
                                entre le Loueur et le Stockeur, remettre les clefs, et dresser un état des lieux
                                d’entrée.
                                Cette
                                prestation pourra être soumise à facturation de la part de stock-ic.ch au Loueur.
                                Stock-ici
                                pourra
                                également faire appel à un sous-traitant pour satisfaire cette demande.
                            </li>
                        </ul>

                        <p className="termsParagraph">L'utilisation du Site est subordonnée à l'acceptation des
                            présentes
                            Conditions
                            Générales.
                        </p>
                        <p className="termsParagraph">
                            Au moment de la création du compte utilisateur sur le Site, les Utilisateurs sont tenus de
                            les accepter.
                        </p>
                        <p className="termsParagraph">
                            Seule l'acceptation de ces CGU permet aux Utilisateurs d'accéder aux services proposés par
                            le Site.
                        </p>
                        <p className="termsParagraph">
                            L'acceptation des présentes CGU est entière et forme un tout indivisible, et les
                            Utilisateurs ne peuvent
                            choisir de voir appliquer une partie des CGU seulement ou encore formuler des réserves.
                        </p>
                        <p className="termsParagraph">
                            En cas de manquement à l'une des obligations prévues par les présentes, Stock-ici se réserve
                            la
                            possibilité de supprimer le compte de l'utilisateur concerné.
                        </p>
                        <p className="termsParagraph">
                            L’adhésion aux présentes CGU emporte adhésion pleine et entière au contrat de mise à
                            disposition
                            d’espace, disponible dans la section 2.O, ci-dessous.
                        </p>

                        <li className="title2">Fonctionnement du site.</li>
                        <h3 className="title3">A. Inscription</h3>
                        <p className="termsParagraph">Les conditions cumulatives suivantes doivent impérativement être
                            remplies pour utiliser
                            les services du Site :</p>
                        <p className="termsParagraph">a) Stockeur :</p>
                        <ul className="termsList">
                            <li>
                                L’inscription sur le Site du Stockeur doit être faite sous sa véritable identité.
                            </li>
                            <li>
                                Le Stockeur doit être une personne physique majeure, ou une personne morale dûment
                                représentée.
                            </li>
                            <li>
                                Un Utilisateur ne pourra disposer de plusieurs comptes
                            </li>
                        </ul>

                        <p className="termsParagraph">b) Loueur :</p>
                        <ul className="termsList">
                            <li>
                                L’inscription sur le site du Loueur doit être faite sous sa véritable identité.
                            </li>
                            <li>
                                Le Loueur doit être une personne physique majeure, ou une personne morale dûment
                                représentée.
                            </li>
                            <li>
                                Un utilisateur pourra disposer de plusieurs comptes s'il prouve qu'il s'agit de sociétés
                                différentes
                                ou mise à disposition d'espaces de stockage à des adresses différentes.
                            </li>
                        </ul>

                        <p className="termsParagraph">Le Loueur a toute liberté pour décider de la personne à laquelle
                            il
                            accepte de mettre à
                            disposition son
                            espace ou dont il accepte de recevoir les biens. La personne du Stockeur est une condition
                            essentielle à
                            la réalisation du Contrat de location ou de dépôt. <br/>
                            Le Loueur doit être attentif à procéder à toutes les vérifications avant la remise des clefs
                            au
                            Stockeur
                            dans le cas d'un Contrat de Location, ou avant de recevoir les biens du Stockeur dans le cas
                            d’un
                            Contrat de dépôt, et en particulier vérifier : l’identité du Stockeur, l’adresse du
                            Stockeur, et
                            la
                            conformité de l’identité de la personne se présentant avec celle déclarée en ligne ainsi que
                            la
                            nature
                            des biens stockés afin de vérifier qu'ils soient bien en conformité avec les biens couverts
                            par
                            l'assurance.<br/>
                            Les services de stock-ici sont réservés aux personnes physiques, juridiquement capables ou
                            autorisées à
                            souscrire des Contrats de location ou de dépôt, ou aux personnes morales dûment
                            représentées.
                            Les
                            services stock-ici ne sont pas disponibles pour les mineurs. De plus, sans exclure d’autres
                            actions,
                            stock-ici se réserve le droit de suspendre ou de clôturer unilatéralement et sans préavis le
                            compte d’un
                            Membre dont serait constatée une déloyauté dans l’utilisation du Site, ou pour tous les
                            motifs
                            visés
                            ci-dessous :
                        </p>
                        <ul className="termsList">
                            <li>
                                Violation d’une disposition des Conditions Générales ainsi que des autres conditions ou
                                règlements
                                de stock-ici
                            </li>
                            <li>
                                Authentification ou vérification impossible de l’origine d’une offre de Location ou de
                                Dépôt
                            </li>
                            <li>
                                Données personnelles fausses
                            </li>
                            <li>
                                Diffusion de contenu illégal (notamment pornographie, diffamation, atteinte au droit à
                                l’image,
                                discriminations, incitation à la violence ou à la haine raciale, religieuse ou ethnique)
                            </li>
                            <li>
                                Atteinte aux bonnes mœurs
                            </li>
                            <li>
                                Utilisation, copie ou diffusion d’œuvres protégées par les droits d’auteur sans
                                autorisation
                                expresse
                            </li>
                            <li>
                                Utilisation abusive ou frauduleuse des outils de collaboration (spam de commentaires,
                                spam
                                de clics,
                                etc.)
                            </li>
                            <li>
                                La fermeture d’un compte ne peut donner lieu à aucune indemnité quels que soient les
                                éventuels
                                dommages occasionnés par la fermeture de ce compte. Le compte membre est personnel et
                                non
                                cessible.
                            </li>
                            <li>
                                La suspension temporaire ou définitive d’un compte membre empêche toute utilisation des
                                services de
                                stock-ici
                            </li>
                            <li>
                                Le statut de Membre permet, quant à lui, d’accéder à l’espace privé du site grâce à un
                                mot
                                de passe.
                            </li>
                        </ul>

                        <h3 className="title3">B. Modes de transaction et durées de location</h3>
                        <p className="termsParagraph">2 modes de transaction sont disponibles :</p>

                        <ul className="termsList">
                            <li>
                                La Location permet au Loueur de mettre à disposition d’un Stockeur un espace entier, en
                                lui
                                remettant de façon exclusive la clé du lieu.
                            </li>
                            <li>
                                Le Dépôt permet au Stockeur de confier des biens au Loueur, sous la garde de ce dernier.
                                Un
                                Loueur
                                peut accueillir à un même moment les biens de plusieurs Stockeur.
                            </li>
                            <li>
                                Le contrat entre les parties diffèrent entre les deux modes de transaction.
                            </li>
                        </ul>

                        <p className="termsParagraph">Chaque Location ou Dépôt est également conclu(e) aux conditions
                            spécifiques de l’annonce
                            qui prévaudront sur les présentes conditions générales d’utilisation et de vente.</p>


                        <h3 className="title3">C. Disponibilités de l’espace</h3>

                        <p className="termsParagraph">Le Loueur indique le lieu, la date de disponibilité de son bien,
                            et
                            décide du tarif de
                            Location ou de Dépôt, qu’il souhaite obtenir. Il indique également la surface disponible,
                            calculé à
                            l’aide d’un outil fourni sur le site Internet de stock-ici.</p>

                        <p className="termsParagraph">Le Loueur s’engage sous son exclusive responsabilité à ne mettre à
                            disposition sur le
                            site Internet que des espaces dont il est propriétaire, ou qu’il est autorisé à réserver à
                            cet
                            usage.
                            Ces espaces sont en bon état et fournissent des conditions propres à l’entreposage de biens.
                            Le
                            Loueur
                            devra en particulier veiller à l’état des installations électriques, au taux d’humidité, et
                            aux
                            conditions d’accès et de sécurité. Le bien doit être conforme en tout point à la description
                            faite sur
                            le site.</p>

                        <h3 className="title3">D. Réservation et Paiement</h3>

                        <p className="termsParagraph"><b>a) Au moment de la réservation</b></p>
                        <p className="termsParagraph">Le Stockeur trouve les espaces disponibles à la Location ou au
                            Dépôt
                            en entrant la
                            localisation de son choix sur le site de stock-ici. Il réserve l’espace qui l'intéresse et
                            fait
                            une
                            autorisation de paiement par carte bancaire ou prélèvement. Cette autorisation génère
                            automatiquement la
                            création d'un portefeuille électronique auprès de X.<br/>
                            Le Loueur reçoit par email la demande de réservation, à laquelle il doit répondre dans les
                            48h.
                            En
                            l’absence de réponse, la demande est considérée comme rejetée et l’argent n’est pas débité.
                            Si la demande de réservation est refusée, le Stockeur est prévenu mais n'est pas
                            débité.<br/>
                            Si le Loueur accepte la demande de réservation, la Location ou le Dépôt sont confirmés. Le
                            Stockeur sera
                            débité 48h avant le début du contrat, et le Stockeur est prélevé. X transfère le montant des
                            frais de
                            service à stock-ici. Sauf cas particuliers, ce montant est de 20% du montant total de la
                            réservation et
                            inclut tous les frais et coûts liés aux prestations de stock-ici telles que définies à
                            l'article
                            1.
                        </p>

                        <p className="termsParagraph"><b>b) Au moment de l’entrée dans les lieux</b></p>

                        <p className="termsParagraph">Afin de protéger les deux parties, stock-ici, via son partenaire
                            X,
                            bloque l’argent de
                            la transaction jusqu’à 7 jours après le début de la transaction.<br/>
                            Stock-ici, via X, opère alors à destination du portefeuille du Loueur le transfert du
                            montant de
                            la
                            Location ou du Dépôt correspondant au tarif mensuel fixé par celui-ci et cela tous les mois
                            jusqu'à
                            interruption du contrat via la plateforme par une des deux parties.
                        </p>

                        <h3 className="title3">E. Arrivée dans les lieux</h3>

                        <p className="termsParagraph">Le jour prévu de la remise des clés ou du Dépôt, Stockeur et
                            Loueur
                            signent le contrat
                            que le Loueur aura
                            imprimé, et la Location ou le Dépôt peut commencer.<br/>
                            L’identité de la personne se présentant pour récupérer les clés ou déposer ses biens doit
                            correspondre à
                            celle déclarée sur le site, à celle du payeur de la Location ou du Dépôt, et à celle du
                            signataire du
                            contrat de Location ou de Dépôt. Dans le cas contraire, la personne doit être dûment
                            autorisée
                            et être
                            munie d’une procuration.
                        </p>

                        <h3 className="title3">F. Objets non autorisés</h3>

                        <p className="termsParagraph">Le Stockeur peut stocker les biens à l'exception des biens
                            dangereux,
                            illicites,
                            inflammables, contaminants, toxiques, radioactifs, explosifs, périssables, colorants.<br/>
                            Pour la liste des objets non assurés, se rapporter au paragraphe 3 - Assurance
                        </p>

                        <h3 className="title3">G. Evaluation</h3>
                        <p className="termsParagraph">Une semaine après le début du Costockage ainsi qu'à l’issue de la
                            Location ou du Dépôt,
                            le Stockeur évalue son Loueur.</p>

                        <h3 className="title3">H. Annulation</h3>
                        <p className="termsParagraph">
                            a) Annulation à l’initiative du Stockeur ou du Loueur, survenant jusqu’à 3 jours avant la
                            date de début du contrat : <br/>
                            Il est possible pour le Stockeur d’annuler une Location ou un Dépôt jusqu'à 3 jours avant la
                            date de
                            début du contrat, sans frais retenus par stock-ici.
                            Après cette date, les frais de service de stock-ici seront dus, proportionnellement à la
                            durée
                            du
                            préavis.
                            <br/>
                            L’annulation s'effectue par l’utilisateur directement dans son compte, depuis la page
                            d’échange
                            avec le
                            propriétaire.
                            <br/>
                            Le Loueur ne peut annuler qu'en cas de force majeure (espace devenu impropre au stockage,
                            vendu,
                            déménagement (Rajouter le motif d'incompatibilité d’humeur), et ce avec deux mois de
                            préavis. Il
                            peut
                            également interrompre le contrat dans le cadre du paragraphe "Résiliation du contrat" situé
                            dans
                            le
                            Contrat.
                            <br/>
                            Si le propriétaire ne respecte ce délai, le locataire aura le droit de demander la gratuité
                            des
                            mensualités restantes.
                            <br/><br/>
                            L’annulation s'effectue par l'envoi d'un email à l'adresse support@stock-ici.ch
                            <br/><br/>
                            En cas d’annulation de la transaction par le Loueur après acceptation par ce dernier de la
                            demande de
                            réservation du Stockeur, une proposition d’un local de remplacement sera effectuée par
                            stock-ici
                            au
                            Stockeur, si celui-ci existe et est disponible.
                        </p>
                        <p className="termsParagraph">
                            b) Annulation à l’initiative du Stockeur, survenant à une date postérieure à 3 jours avant
                            le
                            début du
                            contrat :
                            <br/><br/>
                            Si le Stockeur annule moins de 3 jours avant la date de début du contrat, les frais de
                            service
                            de
                            Costockage sont dûs, proportionnellement à la durée du préavis. Il doit également un
                            dédommagement
                            correspondant à 2 semaines de transaction au Loueur. Ces dédommagements ne seront néanmoins
                            pas
                            dus au
                            propriétaire si le locataire a confirmé une réservation sur un autre espace sur le site.
                            <br/><br/>
                            En conséquence, le Stockeur sera remboursé du solde, soit la somme payée diminuée des frais
                            de
                            service
                            de Costockage et du dédommagement du Loueur. Si l'espace de stockage du Loueur est impropre
                            au
                            stockage
                            ou non conforme à la description de l'annonce, l'annulation s'effectue sans frais pour le
                            Stockeur.<br/>
                            L’annulation s'effectue par l’utilisateur directement dans son compte, depuis la page
                            d’échange
                            avec le
                            propriétaire.
                        </p>

                        <p className="termsParagraph">
                            c) Annulation ou Interruption à l’initiative du Loueur, survenant à une date postérieure à 3
                            jours avant
                            la date de début du contrat :
                            <br/><br/>
                            Le Loueur est engagé pour toute la durée du contrat de Location ou de Dépôt. Il ne peut
                            annuler,
                            sauf en
                            cas de manquement aux obligations de la part du Stockeur, ou en cas de force majeur (espace
                            devenu
                            impropre au stockage, vendu, déménagement).<br/>
                            En cas d’annulation de la transaction par le Loueur après acceptation par ce dernier de la
                            demande de
                            réservation du Stockeur, une proposition d’un local de remplacement sera effectuée par
                            stock-ici
                            au
                            Stockeur, si celui-ci existe et est disponible.<br/>
                            En cas de problème lié à une annulation, les membres peuvent contacter directement stock-ici
                            à
                            l’adresse
                            support@stock-ici.ch<br/>
                            Stock-ici se réserve le droit d’accepter ou de refuser le recours d’un membre.
                        </p>

                        <h3 className="title3">
                            I. Modification du contrat (Contrat de dépôt)
                        </h3>
                        <p className="termsParagraph">
                            La surface indiquée sur le contrat lors de la réservation peut être modifié par les parties.
                            Le
                            Loueur
                            en informera stock-ici par email en mettant le Stockeur en copie de ces échanges.<br/>
                            En cas de litige, les mesures de l'espace occupé (longueur et largeur) devront être
                            précisées.<br/>
                            Le contrat sera alors réédité avec la surface et les loyers adéquats. En cas de besoin, les
                            paiements
                            seront ajustés de manière rétroactive.
                        </p>

                        <h3 className="title3">
                            J. Durée du contrat
                        </h3>
                        <p className="termsParagraph">
                            Sauf stipulation contraire dans les conditions de location, le contrat est conclu pour pour
                            une
                            période
                            initiale d'au minimum 1 mois.<br/>
                            Après cette période initiale, le contrat se poursuivra pour une période indéterminée et
                            pourra
                            être
                            résilié à tout moment via le bouton "Interrompre mon contrat" dans l'onglet "Mes locations"
                            avec
                            un
                            préavis de 15 jours.<br/>
                            Le propriétaire de l'espace de stockage se réserve le droit de revoir le loyer de l'espace
                            de
                            stockage
                            dès 6 mois de location dans la limite de 20% d'augmentation.<br/>
                            La révision s'applique dans les 30 jours suivant la réception du mail via la plateforme
                            stock-ici de la
                            part du propriétaire de l'espace de stockage.
                        </p>

                        <h3 className="title3">
                            K. Parrainage
                        </h3>
                        <p className="termsParagraph">
                            Ce programme s’adresse à tous les Utilisateurs. Il permet de récompenser financièrement
                            toute
                            personne
                            qui invite des contacts à créer des annonces sur le site, lorsque ces annonces sont
                            réservées.
                            <br/><br/>
                            a) Description du parrainage
                            <br/><br/>
                            Chaque Utilisateur peut parrainer un ou plusieurs filleul(s), qui déposeraient une annonce
                            pour
                            louer de
                            l’espace. Il suffit pour cela d’indiquer l’email de son ou ses filleul(s) depuis la page
                            “Parrainage” de
                            son espace ou de leur transmettre son lien personnel de parrainage, également disponible sur
                            cette page.
                            Pour que le parrainage soit effectif, le filleul doit finaliser son inscription sur le site,
                            après avoir
                            cliqué sur ce lien et proposer son espace à la location (cave, garage, grenier, chambre…).
                            Lors
                            de la
                            première location du filleul, en tant que Loueur, sur le site stock-ici, 20CHF sont offerts
                            au
                            parrain.
                            <br/><br/>
                            Les conditions de la location sont les suivantes :
                        </p>

                        <ul className="termsList">
                            <li>
                                Un parrain et un filleul ne peuvent pas se louer mutuellement l’espace,
                            </li>
                            <li>
                                La location doit être d’un montant minimum de 50CHF.
                            </li>
                        </ul>

                        <p className="termsParagraph">
                            Si les conditions sont respectées, le parrain est alors crédité de 20CHF sur la page
                            "Parrainage"
                            accessible sur son compte stock-ici.ch dès le début de la location.
                            <br/><br/>
                            b) Récupération de l’argent offert
                            <br/><br/>
                            L’argent cumulé grâce au parrainage est récupérable par virement bancaire, à tout moment,
                            quel
                            que soit
                            le montant dont le membre dispose depuis l’onglet “Parrainage” du tableau de bord.
                        </p>

                        <h3 className="title3">
                            L. Rémunération de Costockage
                        </h3>
                        <p className="termsParagraph">
                            L’accès, l’inscription et l’utilisation du site sont gratuits.<br/>
                            Les frais de service sont dûs lorsqu’une réservation de Location ou de Dépôt est validée
                            entre
                            un
                            Stockeur et un Loueur.<br/>
                            Le prix affiché sur le site est le prix total à payer par le Stockeur, incluant tous les
                            frais
                            et coûts
                            liés à la prestation de Costockage telle que définie à l’article 1.<br/><br/>
                            Ces frais de service sont dûs par le Client. Ils s'élèvent à 20% du montant total de la
                            réservation.
                            Droit de rétractation relatif au service Costockage :
                            <br/>
                            L’utilisateur dispose d’un délai de quatorze jours pour exercer son droit de rétractation
                            concernant
                            l’inscription au service Costockage, sans avoir à justifier de motifs, ni à payer de
                            pénalités.
                            Ce délai
                            court à compter de la souscription du service.<br/>
                            Dans tous les cas, si ce délai expire un samedi, un dimanche ou un jour férié ou chômé, il
                            est
                            prorogé
                            jusqu’au premier jour ouvrable suivant.
                        </p>

                        <h3 className="title3">
                            M. Modalités de paiement
                        </h3>
                        <p className="termsParagraph">
                            Tout paiement entre le Loueur et le Stockeur doit être réalisé par l’intermédiaire de
                            stock-ici
                            via
                            l’utilisation des portefeuilles créés à cet effet par notre partenaire X (voir section 15
                            pour
                            les
                            Conditions Générales d’Utilisation de la monnaie électronique X).
                        </p>

                        <h3 className="title3">
                            N. Contrats de Location et de Dépôt
                        </h3>
                        <p className="termsParagraph">
                            Stock-ici n’est pas partie au contrat de Location ou de Dépôt. Les Utilisateurs sont seuls
                            responsables
                            de la négociation, de la documentation et de la conclusion de toutes les transactions, ainsi
                            que
                            de la
                            légalité, de la validité et de l'exécution des contrats relatifs à ces
                            transactions.<br/><br/>
                            Les modèles de contrats de Location et de Dépôt disponibles sur le site www.stock-ici.ch ne
                            constituent
                            que des modèles et les Utilisateurs sont libres de les amender et les compléter. Le contrat
                            type
                            est
                            fourni à titre indicatif ci-dessous.<br/>
                            L’adhésion aux présentes Conditions Générales emporte adhésion pleine et entière du contrat
                            de
                            Location
                            ou de dépôt, sauf dans le cas ou un autre modèle de contrat a été choisi par les
                            parties.<br/><br/>
                            Toute transaction (hors dépôt de garantie ou caution éventuellement demandé par le Loueur)
                            entre
                            les
                            parties mises en présence grâce au site ne peut se faire que via le site par le biais de la
                            demande de
                            réservation et de son règlement aux échéances prévues.<br/><br/>
                            Le Loueur est libre de choisir avec qui il souhaite conclure son Contrat de Location ou de
                            Dépôt. Dès
                            l’acceptation de la demande de réservation par le Loueur, et le Contrat ainsi conclu, les
                            deux
                            parties
                            au Contrat sont seules responsables de la détermination de la loi applicable et du tribunal
                            compétent.
                            Au besoin, elles doivent se faire conseiller par un professionnel. Stock-ici n’assure pas de
                            conseil
                            juridique.
                        </p>

                        <h3 className="title3">
                            O. Exemple de Contrat de Location et de Dépôt
                        </h3>
                        <div className="termContractEx">
                            <p className="align-content-center">
                                <b>Le Contrat de Stock-ici.ch</b>
                            </p>
                            <p className="termsParagraph">
                                Dernière mise à jour : le 08 mars 2021<br/><br/>
                                <b>Entendons-nous sur les mots</b><br/><br/>
                                Le propriétaire réel des lieux ou la personne physique ou morale dûment habilitée par le
                                propriétaire,
                                et auprès de qui s’effectue la location ou le dépôt, sera désigné par la suite par le
                                terme
                                «
                                Loueur
                                ».<br/><br/>
                                La personne réservant un espace complet au titre d’un bail de location standard ou
                                déposant
                                des
                                affaires
                                dans un espace de costockage au titre d’un dépôt sera désignée par la suite par le terme
                                «
                                Stockeur
                                ».<br/><br/>
                                L’acceptation des CGU de stock-ici.ch vaut signature du Contrat entre le Loueur et le
                                Stockeur
                                (sauf
                                dans le cas ou un autre modèle de contrat a été choisi par les parties), Contrat qui
                                pourra
                                néanmoins
                                être signé par les parties.<br/><br/>
                                Qu’il s’agisse d’une Location ou d’un Dépôt, un contrat est proposé automatiquement afin
                                de
                                simplifier
                                et sécuriser le costockage. Ce contrat pré-rempli est téléchargeable dans l’onglet « Mes
                                Locations »
                                accessible depuis le compte de l’utilisateur, une fois l’espace réservé.<br/><br/>
                                Le Loueur doit imprimer le contrat en 2 exemplaires, qui seront signés par les 2 parties
                                au
                                plus
                                tard le
                                premier jour de costockage. Il est important que les informations fournies sur le site
                                soient
                                exactes
                                pour que l'assurance soit activée. N’oubliez pas de conserver le contrat au moins 12
                                mois
                                après
                                la fin
                                du costockage !<br/><br/>
                                <b>Clauses du contrat pour une Location ou un Dépôt</b>
                            </p>
                            <p className="termsSubtitle">
                                Location :
                            </p>
                            <p className="termsParagraph">
                                Le Stockeur est le seul à pouvoir utiliser l’espace de stockage.<br/>
                                En contrepartie de la Location de l’espace de costockage, le Stockeur s’engage à payer
                                un
                                loyer
                                au
                                Fournisseur, via le site stock-ici.ch. Ce loyer inclut toutes charges, impôts et taxes
                                afférant
                                à
                                l’espace de stockage.
                                <br/><br/>
                                Seul le Stockeur est responsable de l’espace de stockage et de ses biens costockés. Il
                                s’engage
                                par
                                ailleurs à n’exercer aucune activité commerciale dans l’espace de costockage et ne peut
                                utiliser
                                l’adresse de ce dernier dans une optique commerciale, pour y domicilier une entreprise
                                ou
                                pour y
                                recevoir du courrier.
                            </p>

                            <p className="termsSubtitle">
                                Dépôt :
                            </p>
                            <p className="termsParagraph">
                                Le Loueur est responsable de la garde des biens costockés. L’espace peut être partagé
                                avec
                                d’autres
                                stockeurs, ou avec le Loueur.
                                <br/>
                                En contrepartie du dépôt de ses affaires dans l’espace de costockage, le Stockeur
                                s’engage à
                                payer au
                                Loueur la redevance convenue, via le site stock-ici.ch.<br/><br/>
                                Le Stockeur s’engage à être aussi diligent et prudent dans la garde des biens des
                                Clients
                                que
                                pour celle
                                de ses propres biens afin de conserver les biens costockés dans l’état dans lequel ils
                                ont
                                été
                                déposés.
                            </p>
                            <h3 className="title3"><b>Etat de l’espace</b></h3>

                            <p className="termsSubtitle">
                                En Location :
                            </p>
                            <p className="termsParagraph">Au moment de la signature du Contrat et après avoir adhéré aux
                                Conditions Générales
                                d’Utilisation, le Stockeur reconnaît avoir visité l’espace de costockage. Il s'engage à
                                utiliser
                                l’espace en l’état et à le restituer lors de la fin du costockage dans le même état
                                qu'au
                                jour
                                de la
                                signature du contrat.</p>

                            <p className="termsSubtitle">
                                Dépôt :
                            </p>
                            <p className="termsParagraph">
                                Au moment de la signature du contrat et après avoir adhéré aux Conditions Générales
                                d’Utilisation, le
                                Stockeur peut demander à visiter l’espace de costockage. Il s'engage à utiliser l’espace
                                en
                                l’état et
                                renonce à exiger du Loueur un quelconque aménagement de l’espace de costockage. Il
                                s’engage
                                également à
                                ne pas dégrader l’espace de costockage lors du dépôt ou du retrait de ses biens.
                            </p>

                            <h3 className="title3"><b>Accès à l’espace</b></h3>
                            <p className="termsSubtitle">
                                En Location :
                            </p>
                            <p className="termsParagraph">
                                Le Stockeur, avec le Loueur, sont les seuls détenteurs de la clé qui verrouille l’espace
                                mis
                                à
                                sa
                                disposition.<br/>
                                L’accès à l’espace personnel de costockage se fera exclusivement aux conditions
                                spécifiques
                                convenues
                                avec le Loueur.<br/>
                                Le Loueur n'est pas responsable de l'accès à l’espace par une autre personne que le
                                Stockeur, ni
                                d'éventuelles disparitions de biens que le Stockeur aurait pu constater.<br/>
                                Le Loueur peut procéder à l'ouverture de l’espace de costockage sans accord préalable du
                                Stockeur dans
                                les cas suivants :
                            </p>
                            <ul className="termsList">
                                <li>
                                    requête de la Police, des Pompiers, de la Gendarmerie, des Douanes ou sur décision
                                    de
                                    justice,
                                </li>
                                <li>
                                    en cas de force majeure (Cf. Article Cas de force majeure)
                                </li>
                            </ul>

                            <p className="termsSubtitle">
                                En Dépôt :
                            </p>
                            <p className="termsParagraph">
                                Le Loueur, ou son représentant, est le seul détenteur de la clé de la porte qui
                                verrouille
                                l’espace de
                                costockage. Elle ne pourra être confiée au Stockeur. Loueur et Stockeur pourront signer
                                un
                                document
                                listant l’inventaire simplifié des biens costockés. Cet inventaire sera annexé au
                                contrat et
                                mis
                                à jour
                                dans le temps si nécessaire.
                            </p>

                            <h3 className="title3"><b>Dommages causés par le Client</b></h3>
                            <p className="termsSubtitle">
                                En Location :
                            </p>
                            <p className="termsParagraph">
                                Lors de la signature du contrat, le Stockeur doit se renseigner auprès du Loueur de la
                                limite de
                                surcharge au sol supportée par l’espace de costockage. Si le Stockeur ne respecte pas la
                                limite,
                                il est
                                alors tenu pour seul responsable des dommages occasionnés par le dépassement de la
                                charge
                                autorisée : il
                                assume seul les éventuelles réparations de l’espace.<br/><br/>
                                Le Stockeur s'engage également à :
                            </p>
                            <ul className="termsList">
                                <li>
                                    informer le Loueur si un dommage est causé à l’espace de costockage
                                </li>
                                <li>
                                    remplacer tout bien qu’il endommage (ou est endommagé par d'autres personnes qu'il
                                    fait
                                    entrer dans
                                    l’espace) ;
                                </li>
                                <li>
                                    réparer tout dommage causé à l’espace s’il en est la cause (sauf en cas de
                                    couverture
                                    par
                                    l’assurance souscrite, notamment en cas de vol ou dégradation et dans la limite de
                                    la
                                    franchise) ;
                                </li>
                                <li>
                                    rembourser le Loueur de toute somme engagée en raison d'un dommage qu’il aurait
                                    causé.
                                </li>
                            </ul>

                            <p className="termsSubtitle">
                                En Dépôt :
                            </p>
                            <p className="termsParagraph">
                                Le Stockeur s'engage également à :
                            </p>
                            <ul className="termsList">
                                <li>
                                    réparer tout dommage causé à l’espace de costockage où il effectue le dépôt de ses
                                    biens
                                    s’il en est
                                    la cause (sauf en cas de couverture par l’assurance souscrite, dans la limite de la
                                    franchise) ;
                                </li>
                                <li>
                                    rembourser le Loueur de toute somme engagée en raison d'un dommage qu’il aurait
                                    causé.
                                </li>
                            </ul>

                            <h3 className="title3"><b>Biens “costockables”</b></h3>
                            <p className="termsParagraph">
                                Il est formellement interdit de costocker des biens illicites, dangereux, des plantes,
                                des
                                animaux, des
                                biens périssables, des biens susceptibles d’un classement au titre de la réglementation
                                sur
                                les
                                installations classées pour la protection de l’environnement (I.C.P.E.) et toute autre
                                chose
                                mentionnée
                                dans les conditions générales d’utilisation.
                                <br/><br/>
                                Le Loueur peut également restreindre le costockage à certains biens en l’indiquant sur
                                le
                                contrat.
                            </p>

                            <h3 className="title3"><b>Durée du Contrat de location ou de dépôt</b></h3>
                            <p className="termsSubtitle">
                                Période
                            </p>
                            <p className="termsParagraph">
                                Sauf stipulation contraire dans les conditions spéciales, le contrat est conclu pour
                                pour
                                une
                                période
                                initiale d'au minimum 1 mois. Après cette période initiale, le contrat se poursuivra
                                pour
                                une
                                période
                                indéterminée et pourra être résilié à tout moment via le bouton "Interrompre mon
                                contrat"
                                dans
                                l'onglet
                                "Mes locations" avec un préavis de 15 jours.
                            </p>

                            <h3 className="title3">
                                Révision du tarif de costockage
                            </h3>
                            <p className="termsParagraph">
                                Le propriétaire de l'espace de stockage se réserve le droit de revoir le loyer de
                                l'espace
                                de
                                stockage
                                dès 6 mois de location dans la limite de 20% d'augmentation. La révision s'applique dans
                                les
                                30
                                jours
                                suivant la réception du mail via la plateforme Costockage de la part du propriétaire de
                                l'espace
                                de
                                stockage. A défaut de notification écrite, le tarif initial sera maintenu.
                            </p>

                            <h3 className="title3">
                                Dénonciation du contrat
                            </h3>
                            <p className="termsParagraph">
                                A tout moment, le Stockeur peut mettre fin au contrat et ce, sans motivation
                                particulière,
                                avec
                                deux
                                semaines de préavis (passée la période minimale de location). La démarche doit se faire
                                via
                                le
                                site
                                stock-ici.ch, depuis la page "Mes Locations".
                            </p>


                            <h3 className="title3">
                                Défaut de libération de l’espace
                            </h3>
                            <p className="termsParagraph">
                                A défaut de libération de l’espace de costockage par le Stockeur à la date d’expiration
                                du
                                Contrat, le
                                Loueur pourra prolonger le Contrat pour une durée de 3 mois, dans la limite de 36 mois,
                                le
                                loyer
                                ou la
                                redevance étant du(e) mensuellement et d’avance. Une fois régularisée, le Contrat pourra
                                être
                                interrompu
                                avec 15 jours de préavis.
                            </p>


                            <h3 className="title3"><b>Modalités de paiement du loyer (Location) ou de la redevance
                                (Dépôt)</b>
                            </h3>
                            <p className="termsSubtitle">
                                Paiement
                            </p>
                            <p className="termsParagraph">
                                Le montant prélevé du loyer ou de la redevance représente un mois de costockage
                                (incluant
                                les
                                frais de
                                service mensuels) et sera prélevé deux jours avant la date de début du Contrat, puis
                                versé
                                au
                                Loueur
                                quatre jours après la date de début du Contrat.<br/>
                                Par la suite, un paiement sera effectué tous les mois jusqu’à échéance.
                                Le montant fixé dans le Contrat est applicable dès le premier jour de costockage et,
                                sauf
                                communication
                                contraire, aux périodes suivantes.
                            </p>

                            <h3 className="title3">
                                Dépôt de garantie
                            </h3>
                            <p className="termsParagraph">
                                Le Contrat de costockage ne donne pas lieu au paiement d’un dépôt de garantie via la
                                plateforme.
                                Si ce
                                dépôt de garantie est mentionnée dans les conditions spécifiques de l’annonce, il pourra
                                être
                                effectué
                                directement entre les parties.
                            </p>

                            <h3 className="title3">
                                Défaut de paiement
                            </h3>
                            <p className="termsParagraph">
                                Il sera fait application des stipulations de l’article « Résiliation du contrat »
                                ci-après.
                            </p>

                            <h3 className="title3">
                                Cession / Sous-location
                            </h3>
                            <p className="termsParagraph">
                                Le Contrat de costockage ne peut être cédé à quiconque par le Stockeur.<br/>
                                Toute sous-location ou mise à disposition de l’espace de costockage par le Stockeur à un
                                tiers
                                est
                                strictement interdite.
                            </p>

                            <h3 className="title3"><b>Résiliation du contrat</b></h3>
                            <p className="termsSubtitle">
                                Résiliation si le Client ne respecte pas ses engagements :
                            </p>
                            <p className="termsParagraph">
                                Le Loueur peut résilier le contrat de façon immédiate et unilatérale, dans les cas où le
                                Stockeur :
                            </p>
                            <ul className="termsList">
                                <li>
                                    ne paie pas le loyer ou la redevance et l’ensemble des sommes dues au titre du
                                    Contrat
                                    de
                                    Location
                                    ou de Dépôt, depuis au moins 1 mois
                                </li>
                                <li>
                                    costocke des biens non-autorisés,
                                </li>
                                <li>
                                    ne respecte pas les Conditions Générales présentes dans le contrat.
                                </li>
                            </ul>

                            <p className="termsParagraph">
                                Dans cette hypothèse, le Loueur informera le Stockeur de la résiliation du Contrat et de
                                la
                                possibilité
                                pour le Stockeur de récupérer ses biens. Il envoie alors une mise en demeure au Stockeur
                                via
                                une
                                «
                                Lettre Recommandée avec Avis de Réception » en lui accordant un délai de 10 jours à
                                partir
                                de la
                                réception du récépissé. A défaut pour le Stockeur de récupérer ses biens stockés, il
                                sera
                                réputé
                                avoir
                                renoncé à les reprendre, ils pourront alors être conservés ou mises à la benne par le
                                Loueur,
                                aux frais
                                exclusifs du Stockeur.
                                <br/><br/>
                                Alors, tout loyer ou redevance versé(e) pour la période en cours ainsi que la caution
                                éventuelle
                                restent
                                acquises au Stockeur en déduction des sommes dues.<br/>
                                Le courrier de mise en demeure est envoyé par stock-ici au propriétaire, afin qu'il
                                l'adresse au
                                locataire. Si le courrier n'est pas adressé par le propriétaire au locataire dans un
                                délai
                                d'un
                                mois
                                après l'envoi par stock-ici, stock-ici cesse les relance et considère le dossier
                                d'impayé
                                comme
                                classé.<br/>
                                Tout impayé permet au propriétaire d’interdire l’accès au box / aux affaires tant que la
                                dette
                                n’est pas
                                réglée.
                            </p>

                            <h3 className="title3">
                                Modification du contrat
                            </h3>
                            <p className="termsParagraph">
                                Toute modification du Contrat prend la forme d'un avenant rédigé et signé par leLoueur
                                et le
                                Stockeur et
                                devra être notifiée par mail à stock-ici.
                            </p>

                            <h3 className="title3"><b>Fin du contrat</b></h3>
                            <p className="termsParagraph">
                                La date d'interruption du contrat fixe la date de fin du contrat, en respect du préavis
                                exigé
                                (15 jours,
                                sauf conditions particulières à l'annonce). L'interruption du contrat s'effectue
                                directement
                                sur
                                le site
                                dans l'onglet Messagerie en cliquant sur "Interrompre le contrat". Arrivé au terme du
                                Contrat,
                                le
                                Stockeur doit restituer l’espace vide et propre. A cette date, le Stockeur doit avoir
                                payé
                                l'ensemble
                                des sommes dues au propriétaire ainsi que d'éventuelles pénalités et autres frais. C'est
                                l'interruption
                                du contrat via le site stock-ici uniquement qui fait foi concernant l'interruption des
                                réglements. Sans
                                interruption, le contrat continuera de courir et les paiements seront dus.<br/><br/>
                                Dans l'hypothèse où, à l'expiration du Contrat, et si le Loueur a notifié le Stockeur de
                                la
                                non-prolongation du Contrat au plus tard 15 jours avant l’expiration, le Stockeur qui ne
                                déménagerait
                                pas l’ensemble de ses biens sera présumé les avoir abandonnés. Le propriétaire pourra,
                                10
                                jours
                                après
                                l’envoi d’une lettre recommandée avec demande d’avis de réception, pourra les conserver
                                ou
                                les
                                mettre à
                                la benne aux frais exclusifs du Stockeur.
                            </p>


                            <h3 className="title3"><b>En cas de force majeure</b></h3>
                            <p className="termsParagraph">
                                En cas de force majeure et d'extrême urgence, l'emplacement peut être évacué si le
                                propriétaire
                                le juge
                                nécessaire afin de protéger les personnes et les biens costockés. Le Stockeur accepte
                                alors
                                que
                                son
                                espace soit ouvert et que ses biens soient déplacés.
                            </p>

                            <h3 className="title3"><b>En cas de litige</b></h3>
                            <p className="termsParagraph">
                                En cas de litige, les tribunaux compétents dépendent de la localisation de l’espace de
                                costockage.
                            </p>

                            <h3 className="title3">
                                P. Relation entre les parties
                            </h3>
                            <p className="termsParagraph">
                                Stock-ici et les Utilisateurs ou membres sont des parties indépendantes, chacune
                                agissant en
                                son
                                nom et
                                pour son propre compte. Les présentes Conditions Générales ne créent donc aucun lien de
                                subordination,
                                d’agence, de mandat, société en participation, entreprise commune, de relations
                                employeur/employé ou
                                franchiseur entre stock-ici SA, d’une part, et l’Utilisateur ou le membre, d’autre part.
                            </p>
                        </div>

                        <li className="title2">Assurance</li>

                        <p className="termsParagraph">
                            Stock-ici a souscrit pour le compte des Stockeurs un contrat d’assurance afin de garantir
                            (I)
                            les
                            dommages matériels aux biens assurés en cas d’incendie, de dégâts des eaux, d’événements
                            climatiques, de
                            catastrophes naturelles, de vols et dégradations matérielles volontaires, d’attentats et
                            actes
                            de
                            terrorisme, (II) la responsabilité civile du Stockeur vis-à-vis du Loueur pour les dommages
                            matériels et
                            immatériels consécutifs causés à l’espace de stockage du fait du stockage des biens
                            assurés.<br/><br/>
                            Les Conditions Générales qui fixent l’étendue et les conditions de ce contrat d’assurance
                            pour
                            compte
                            sont disponibles ici.<br/><br/>
                            Les biens suivants ne sont pas assurés :
                        </p>
                        <ul className="termsList">
                            <li>
                                <b>Denrées alimentaires ;</b>
                            </li>
                            <li>
                                <b>Liquides ;</b>
                            </li>
                            <li>
                                <b>Médicaments ;</b>
                            </li>
                            <li>
                                <b>Cigarettes, tabac et produits du tabac ;</b>
                            </li>
                            <li>
                                <b>Vins et spiritueux ;</b>
                            </li>
                            <li>
                                <b>Armes, explosifs, combustibles et feux d'artifice ;</b>
                            </li>
                            <li>
                                <b>Produits chimiques, produits toxiques ou dangereux ;</b>
                            </li>
                            <li>
                                <b>Espèces (ou tout autre document ayant valeur d’argent) et valeurs (pièces et lingots
                                    de
                                    métaux
                                    précieux), cartes bancaires et autres moyens de paiements ;</b>
                            </li>
                            <li>
                                <b>Valeurs mobilières et autres titres et documents financiers ;</b>
                            </li>
                            <li>
                                <b>Bijoux, pierres précieuses, métaux précieux ;</b>
                            </li>
                            <li>
                                <b>Objets de valeur d’un montant supérieur à 500 € ;</b>
                            </li>
                            <li>
                                <b>Collections dont la valeur globale est supérieure à 500 € ;</b>
                            </li>
                            <li>
                                <b>Fourrures ;</b>
                            </li>
                            <li>
                                <b>Œuvres d’art </b>
                            </li>
                            <li>
                                <b>Carte d’identité, passeport, permis de conduire, titres de propriété, et autres
                                    documents
                                    officiels </b>
                            </li>
                            <li>
                                <b>Objets moisis ou contaminés ;</b>
                            </li>
                            <li>
                                <b>Biens volés ou détenus illégalement ;</b>
                            </li>
                            <li>
                                <b>Animaux (hors animaux empaillés) ;</b>
                            </li>
                            <li>
                                <b>Véhicules terrestres à moteur.</b>
                            </li>
                        </ul>

                        <p className="termsParagraph">
                            Les plafonds de garantie sont les suivants :<br/><br/>
                            a) Dommages matériels aux biens assurés<br/><br/>
                            <b>La garantie est limitée à un seul sinistre pendant toute la durée du contrat de location
                                du
                                local
                                (périodes de prolongation comprises).</b>
                        </p>

                        <table className="termsParagraph">
                            <th>
                                <td>Plafond de garantie par sinistre</td>
                            </th>
                            <tr>
                                <td>Biens non professionnels</td>
                                <td/>
                                <td>3 000 CHF</td>
                            </tr>
                            <tr>
                                <td>Biens professionnels</td>
                                <td/>
                                <td>7 500 CHF</td>
                            </tr>
                            <tr>
                                <td>Biens professionnels dont Marchandises et vol</td>
                                <td/>
                                <td>5 000 CHF</td>
                            </tr>
                            <tr>
                                <td>Franchise par Sinistre biens non professionnels</td>
                                <td/>
                                <td>300 CHF</td>
                            </tr>
                            <tr>
                                <td>Franchise par Sinistre biens professionnels</td>
                                <td/>
                                <td>500 CHF</td>
                            </tr>
                        </table>
                        <p className="termsParagraph"><b>* Sauf catastrophe naturelle (franchise fixée par arrêté
                            ministériel)<br/><br/>
                            En cas de dommages causés à la fois aux biens professionnels et aux biens non professionnels
                            au
                            cours
                            d’un seul et même sinistre le plafond de la garantie dommage aux biens professionnels sera
                            appliqué.<br/><br/>
                            Vétusté appliqué :
                        </b></p>
                        <ul className="termsList">
                            <li>
                                <b>
                                    10 % par an à compter de la date d’achat du bien endommagé sur présentation de la
                                    facture
                                    d’achat du bien,
                                </b>
                            </li>
                            <li>
                                <b>
                                    50 % à défaut de présentation de la facture d’achat sur la base du prix d’achat TTC
                                    au
                                    jour du
                                    Sinistre.
                                </b>
                            </li>
                        </ul>

                        <p className="termsParagraph">
                            b) <b>Responsabilité civile du Stockeur (risques locatifs)</b>
                            <br/><br/>
                            L’engagement maximum de l’assureur pour l’ensemble des Dommages matériels et dommages
                            immatériels
                            consécutifs garantis, Frais de défense compris, est fixé à 150 000 CHF par Sinistre et à un
                            seul
                            Sinistre pendant toute la durée du Contrat de dépôt / mise à disposition du local (périodes
                            de
                            prolongation comprises).<br/>
                            Plafond de garantie par Sinistre et par durée de Contrat de dépôt / mise à disposition :
                        </p>
                        <table className="termsParagraph">
                            <tr>
                                <td>Risques locatifs</td>
                                <td>150 000 CHF</td>
                            </tr>
                            <tr>
                                <td>Franchise par Sinistre</td>
                                <td>250 CHF</td>
                            </tr>
                        </table>

                        <p className="termsParagraph">
                            Vous devez juger par vous-même si les garanties du contrat souscrit par stock-ici vous
                            suffisent
                            et il
                            vous incombe de souscrire toute assurance complémentaire qui vous semblerait utile ou
                            nécessaire.<br/>
                            En cas de doute ou pour toute question, vous pouvez contacter le courtier X , sur , Sarl de
                            courtage au
                            capital de 7500 €, sise 42 rue Pascal, 75013 Paris, RCS Paris 791 655 665, enregistré auprès
                            de
                            l’Orias
                            sous le n°13 003 020.
                        </p>


                        <li className="title2">Limitation de responsabilité et non responsabilité</li>
                        <p className="termsParagraph">
                            CHAQUE UTILISATEUR DU SITE ET DES SERVICES LE FAIT À SES RISQUES ET PÉRILS. <br/><br/>
                            STOCK-ICI N’EST PAS PARTIE AU CONTRAT DE LOCATION OU DE DEPOT QUI INTERVIENT EXCLUSIVEMENT
                            ENTRE
                            LE
                            LOUEUR ET LE STOCKEUR.<br/><br/>
                            SANS PRÉJUDICE DE TOUTE AUTRE DISPOSITION DES PRÉSENTES, L’UTILISATEUR ACCEPTE QUE LE
                            MONTANT
                            MAXIMUM
                            QU'IL EST EN DROIT DE RECOUVRER POUR TOUTE PERTE DÉCOULANT D’UNE ACTION EN RESPONSABILITÉ
                            CONTRE
                            STOCK-ICI EST LIMITÉ AU PRIX EFFECTIVEMENT PAYÉ À STOCK-ICI. LORSQU’AUCUN PRIX N’A ÉTÉ PAYÉ
                            À
                            STOCK-ICI,
                            LE PLAFOND DE LA RESPONSABILITÉ EST FIXÉ À 100 FRANCS SUISSE.<br/><br/>
                            LES INFORMATIONS FOURNIES PAR L'INTERMÉDIAIRE DU SITE PEUVENT COMPORTER DES INEXACTITUDES
                            TECHNIQUES OU
                            FACTUELLES OU DES ERREURS TYPOGRAPHIQUES. STOCK-ICI DÉCLINE TOUTE RESPONSABILITÉ POUR DE
                            TELLES
                            INEXACTITUDES ET ERREURS ET SE RÉSERVE LE DROIT DE FAIRE DES MODIFICATIONS ET DES
                            CORRECTIONS À
                            TOUT
                            MOMENT SANS PRÉAVIS.<br/><br/>
                            STOCK-ICI NE GARANTIT JAMAIS LA SOLVABILITÉ DES UTILISATEURS Y COMPRIS DES
                            STOCKEUR.<br/><br/>
                            STOCK-ICI NE VÉRIFIE PAS L'IDENTITÉ DES MEMBRES NI L’IDENTIFICATION DES ESPACES. IL
                            APPARTIENT
                            AU
                            STOCKEUR ET AU LOUEUR DE VÉRIFIER LEURS IDENTITÉS RESPECTIVES.
                        </p>


                        <li className="title2">Impôts et Taxes</li>
                        <p className="termsParagraph">
                            Il appartient aux Utilisateurs de déclarer et payer en tant que de besoin tous les impôts et
                            toutes les
                            taxes en relation avec la Location d’un espace sou le Dépôt de biens.
                            <br/><br/>
                            Ils pourront trouver des informations sur le site :
                            <br/><br/>
                            Et de la documentation administrative sur le site du Bulletin Officiel des Finances
                            Publiques-Impôts :
                            <br/><br/>
                            Conformément à la législation, stock-ici fournit aux Loueurs un document récapitulant le
                            montant
                            brut de
                            ses transactions.
                        </p>


                        <li className="title2">Modifications des conditions générales d’utilisation</li>
                        <p className="termsParagraph">
                            L’utilisation du site stock-ici et de ses services est subordonnée à l’acceptation des
                            présentes.
                            Stock-ici se réserve le droit de modifier à tout moment les présentes.<br/>
                            La modification prendra effet immédiatement dès la mise en ligne des nouvelles conditions,
                            dont
                            les
                            utilisateurs seront prévenus dès leur entrée en force.
                        </p>

                        <li className="title2">Propriété du site</li>
                        <p className="termsParagraph">
                            Le site stock-ici.ch est la propriété de la stock-ici SA. Toute reproduction même partielle
                            est
                            subordonnée à l’autorisation préalable et écrite de la stock-ici SA.
                        </p>

                        <li className="title2">CNIL</li>
                        <p className="termsParagraph">
                            Les informations et données personnelles recueillies à partir des formulaires présents sur
                            le
                            site font
                            l’objet d’un traitement informatique destiné à stock-ici dont certains des services ou
                            filiales
                            peuvent
                            être situés en dehors de l’Union Européenne. La finalité en est la gestion de la clientèle
                            et
                            des
                            partenaires de stock-ici.<br/>
                            Le ou les destinataire(s) des données sont les différents services en charge de la
                            relation-clients
                            Ces destinataires auront communication des données suivantes : Noms, prénoms et autres
                            données
                            relatives
                            aux comptes des utilisateurs sur le site.<br/>
                            Les garanties suivantes ont été prises pour s’assurer d’un niveau de protection suffisant
                            des
                            données
                            personnelles : les éventuels pays de destination offrent un niveau de protection adéquat.
                            Les données que collectées par stock-ici relèvent à la fois :

                        </p>


                        <li className="title2">Contenu du site rédigé par les utilisateurs</li>
                        <p className="termsParagraph">
                            Stock-ici n’est pas responsable du contenu saisi sur le Site par ses Utilisateurs.
                            Cependant,
                            dès que
                            stock-ici est informé qu’un contenu porterait atteinte aux droits de tiers, Stock-ici fera
                            tout
                            son
                            possible pour supprimer du site, sans délai, le contenu litigieux.
                        </p>


                        <li className="title2"> Global information</li>

                        <p className="termsParagraph">Developed by Alexandre Piguet, Brice Berclaz, Gabrielle Freno,
                            Marc Vial</p>

                        <p className="termsParagraph">Boosted by:</p>
                        <img className="center" width="200px" height="60px" src={LogoHES}
                             alt="Logo HES-SO Valais-Wallis"></img>
                    </ol>
                </CardContent>
            </Card>


        </div>

    );
}