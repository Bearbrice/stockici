import React, {useContext, useState} from "react";
import {Card, CardContent, GridListTileBar, IconButton, ListSubheader, Tooltip} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {ButtonGroup, Form} from "react-bootstrap";
import Label from '@material-ui/core/InputLabel';
import {Formik} from "formik";
import * as Yup from "yup";
import "../App.css";
import firebase from "../firebase";
import {MapContainer, TileLayer, useMapEvent} from "react-leaflet";
import {firebaseAuth} from "../provider/AuthProvider";
import {useAlert} from "react-alert";
import ImageUploading from 'react-images-uploading';
import {v4 as uuidv4} from "uuid";
import {UserLocationContext} from "../context/UserLocationContext";
import {makeStyles} from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import {DeleteForever, Info} from "@material-ui/icons";
import Image from "../assets/imageIcon.png";
import {ThemeContext, themes} from "../context/ThemeContext";
import {useHistory} from "react-router-dom";

//Images uploaded preview layout
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        marginTop: '20px',
        borderRadius: '5px',
    },
    gridList: {
        width: '100%',
        height: '330px',

    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

//Form to add a place from a renter
function FormPlace() {

    const themeContext = useContext(ThemeContext);
    const history = useHistory();
    const classes = useStyles();

    //Map variables
    const [address, setAddress] = useState("");
    const [lat, setLat] = useState("");
    const [lng, setLng] = useState("");
    // const [mapCenter, setMapCenter] = useState([46.35, 7.15]);

    //ImageUploader variables
    const [photoURLs, setPhotoURLs] = useState([]);
    const [imagesPreview, setImagesPreview] = useState([]);

    //Global id used for Firestore and Storage
    const [id, setId] = useState("");

    //User variable
    const {user} = useContext(firebaseAuth);
    const {latLng} = useContext(UserLocationContext);
    const alert = useAlert();

    //Reference to the Cloud Database's collection
    const ref = firebase.firestore().collection("users").doc(user.uid).collection("places");

    // ADD FUNCTION
    async function addPlace(newPlace) {
        await ref
            .doc(id)
            .set(newPlace)
            .catch((err) => {
                console.error(err);
            });

        //then reset the variables below
        setImagesPreview([]);
        setPhotoURLs([]);
        setAddress("");
        alert.show("Votre place a bien été ajoutée !");
    }

    //Schema used for the validation of the form by Yup
    const placeSchema = Yup.object().shape({
        // address: Yup.string()
        //     .min(2, 'Trop court')
        //     .max(50, 'Too Long')
        //     .required('Obligatoire'),
        type: Yup.string()
            .oneOf(["Garage", "Cave", "Local", "Place de parking ext.", "Place de parking int.", "Entrepôt", "Autre"])
            .required('Obligatoire'),
        areaAvailable: Yup.number()
            .required('Obligatoire')
            .max(1000, 'Trop grand'),
        height: Yup.number()
            .required('Obligatoire')
            .max(1000, 'Trop grand'),
        description: Yup.string()
            .max(100, 'Trop grand'),
        price: Yup.number()
            .required('Obligatoire'),
        duration: Yup.string()
            .required('Obligatoire')
            .oneOf(["jour(s)", "semaine(s)", "mois", "année(s)"]),
        floor: Yup.string()
            .required('Obligatoire')
            .oneOf(["-3", "-2", "-1", "rez-de-chausée(0)", "1er étage", "2ème étage", "3ème étage"]),
        minDuration: Yup.string()
            .required('Obligatoire'),
    })

    //Initialize all values in the form
    const initialValues = {
        titleOffer: "",
        type: "",
        address: "",
        areaAvailable: "",
        price: "",
        duration: "",
        minDuration: "",
        height: "",
        hasAccessCar: false,
        floor: "",
        description: "",
        isSecured: false,
        hasElevator: false,
        isPro: "false",
        isVerified: false,
        rating: "",
        lat: "",
        lng: "",
        photoURLs: "",
        owner: user.uid,
    }

    //Function to get coordonates by clicking
    function ClickMapComponent() {
        const map = useMapEvent({
                click: (e) => {
                    GetAddressFromLatLng(e.latlng.lat, e.latlng.lng);
                }
            }
        )
        return null
    }

    //function to get address from longitude and latitude
    //using LocationIQ's API
    async function GetAddressFromLatLng(lat, lng) {
        let url = `https://us1.locationiq.com/v1/reverse.php?key=pk.66121da29d61040d838ccd691bee0b3c&normalizeaddress=1&format=json&lat=${lat}&lon=${lng}`;
        let response = await fetch(url, {});
        let data = await response.json();
        if (data !== null || data !== undefined) {
            setLat(lat);
            setLng(lng);
            setAddress(data.address.road + ", " + data.address.postcode + ", " + data.address.city);
        }
    }

    //Get the coordinates from the address given by the user
    //using LocationIQ's API
    async function GetLatLngFromAddress(addressGiven) {

        const uriAddress = encodeURIComponent(addressGiven);
        let url = `https://us1.locationiq.com/v1/search.php?key=pk.66121da29d61040d838ccd691bee0b3c&normalizeaddress=1&format=json&q=${uriAddress}`;
        let response = await fetch(url, {});
        let data = await response.json();
        if (data !== null && data[0] !== undefined) {
            setLat(data[0].lat);
            setLng(data[0].lon);
            setAddress(addressGiven);
            alert.show("Adresse vérifiée !");
        } else {
            alert.show("Adresse non-valide !");
        }
    }

    async function uploadImages(id) {

        //Upload the image(s) in the user'id, place's id
        const storageRef = firebase.storage().ref('places').child('/' + user.uid).child('/' + id);
        imagesPreview.forEach((image) => (
            storageRef.child(image.file.name)
                .put(image.file)
                .then(async (snap) => {
                    console.log('upload successful', snap)
                    const url = await storageRef.child(image.file.name).getDownloadURL();
                    setPhotoURLs(photoURLs => [...photoURLs, url]);
                })
                .catch((err) => console.error('error uploading file', err))
        ))
    }

    //function to see the new place is the user profile
    function goToProfile() {
        history.push('/account/my-places');
    }

    async function handleValidate(imageList) {
        console.log(imageList);
        //set the images preview with the list
        setImagesPreview(imageList);

        //create here the id for the storage path
        //and for the future created place
        let id = await uuidv4();
        setId(id);

        await uploadImages(id);
        alert.show("Photo(s) validée(s) !");
    }

    const onChangeImage = (imageList, addUpdateIndex) => {
        console.log(imageList);
        //set the images preview with the list
        setImagesPreview(imageList);
    };

    return (
        <div className="formPage">
            <div className="faqTitle mb-3" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadowTitle
            }}>
                <h1 className="title1">Je possède un espace louable</h1>
            </div>

            <div className="mapFormulaireContainer" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadow
            }}>
                <div className="formContainer">
                    <Card>
                        <CardContent className="formCard">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={placeSchema}
                                onSubmit={async (values, {setSubmitting, resetForm}) => {

                                    //Check if the user has uploaded images
                                    if (imagesPreview.length > 0) {

                                        await addPlace({
                                                titleOffer: values.type + " de " + values.areaAvailable + " m2",
                                                type: values.type,
                                                lng: lng,
                                                lat: lat,
                                                address: address,
                                                areaAvailable: values.areaAvailable,
                                                height: values.height,
                                                description: values.description,
                                                floor: values.floor,
                                                hasElevator: values.hasElevator,
                                                hasAccessCar: values.hasAccessCar,
                                                isPro: values.isPro,
                                                isSecured: values.isSecured,
                                                isVerified: values.isVerified,
                                                minDuration: values.minDuration,
                                                price: values.price,
                                                duration: values.duration,
                                                rating: values.rating,
                                                owner: values.owner,
                                                photoURLs: photoURLs,
                                                id: id,
                                                isRefused: false
                                            },
                                            goToProfile());

                                        //reset all fields about pictures/address
                                        setImagesPreview([]);
                                        setPhotoURLs([]);
                                        setAddress("");

                                        //reset the Formik Form
                                        setSubmitting(true);
                                        resetForm();

                                        setTimeout(() => {
                                            setSubmitting(false);
                                        }, 500);
                                    } else {
                                        setSubmitting(false);
                                        alert.show("Vous devez remplir TOUS les champs obligatoires* !")

                                    }
                                }}
                            >
                                {({
                                      errors,
                                      touched,
                                      values,
                                      handleSubmit,
                                      handleChange,
                                      setFieldValue,
                                      handleBlur,
                                  }) => (
                                    <Form onSubmit={handleSubmit} name="formPlace">
                                        <div className="formGroupDiv">
                                            <Form.Group className="fg">
                                                <Label>Type*</Label>
                                                <Form.Control as="select"
                                                              margin="dense" name="type" value={values.type}
                                                              onChange={handleChange}>
                                                    <option style={{color: '#919399'}}>Selectionner</option>
                                                    <option>Garage</option>
                                                    <option>Cave</option>
                                                    <option>Local</option>
                                                    <option>Entrepôt</option>
                                                    <option>Place de parking ext.</option>
                                                    <option>Place de parking int.</option>
                                                    <option>Autre</option>
                                                </Form.Control>
                                                {errors.type && touched.type ?
                                                    <h5 className="errors">{errors.type}</h5>
                                                    : null}
                                            </Form.Group>
                                            <Form.Group className="fg">
                                                <Label>Taille* en m2</Label>
                                                <Form.Control
                                                    type="number"
                                                    name="areaAvailable"
                                                    placeholder="20"
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.areaAvailable}
                                                    style={{width: "80px"}}
                                                />
                                                {errors.areaAvailable && touched.areaAvailable ?
                                                    <h5 className="errors">{errors.areaAvailable}</h5>
                                                    : null}
                                            </Form.Group>
                                            <Form.Group className="fg">
                                                <Label>Hauteur* en mètre(s)</Label>
                                                <Form.Control
                                                    type="number"
                                                    name="height"
                                                    // placeholder="4"
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.height}
                                                    style={{width: "80px"}}
                                                />
                                                {errors.height && touched.height ?
                                                    <h5 className="errors">{errors.height}</h5>
                                                    : null}
                                            </Form.Group>
                                        </div>
                                        <Form.Group className="fg">
                                            <Label>Cliquez sur la carte pour obtenir
                                                directement
                                                l'adresse* ! </Label>
                                            <Form.Control
                                                type="text"
                                                name="address"
                                                placeholder={address}
                                                value={address}
                                                onChange={(e) => setAddress(e.target.value)}
                                                onBlur={handleBlur}
                                            />
                                            <Button
                                                onClick={function (event) {
                                                    GetLatLngFromAddress(address);
                                                }}
                                                className="bg-success mt-3"
                                            >Vérifier l'adresse</Button>
                                        </Form.Group>
                                        <div className="formGroupDiv">
                                            <Form.Group className="fg">
                                                <Label>Prix (CHF)*</Label>
                                                <Form.Control
                                                    type="number"
                                                    name="price"
                                                    placeholder="200"
                                                    variant="outlined"
                                                    margin="dense"
                                                    value={values.price}
                                                    style={{width: "100px"}}
                                                    onChange={handleChange}
                                                />
                                                {errors.price && touched.price ?
                                                    <h5 className="errors">{errors.price}</h5>
                                                    : null}
                                            </Form.Group>
                                            <Form.Group className="fg">
                                                <Label>Durée*</Label>
                                                <Form.Control as="select"
                                                              margin="dense" name="duration"
                                                              value={values.duration}
                                                              onChange={handleChange}>
                                                    <option style={{color: '#919399'}}>Selectionner</option>
                                                    <option>jour(s)</option>
                                                    <option>semaine(s)</option>
                                                    <option>mois</option>
                                                    <option>année(s)</option>
                                                </Form.Control>
                                                {errors.duration && touched.duration ?
                                                    <h5 className="errors">{errors.duration}</h5>
                                                    : null}
                                            </Form.Group>
                                        </div>
                                        <div className="formGroupDiv2">
                                            <Label className="label_minDuration">Durée minimum de location*</Label>
                                            <div className="d-flex flex-row">
                                                <Form.Group className="fg">
                                                    <Form.Control
                                                        type="number"
                                                        name="minDuration"
                                                        placeholder="1"
                                                        variant="outlined"
                                                        margin="dense"
                                                        value={values.minDuration}
                                                        style={{width: "100px"}}
                                                        onChange={handleChange}/>
                                                    {errors.minDuration && touched.minDuration ?
                                                        <h5 className="errors">{errors.minDuration}</h5>
                                                        : null}
                                                </Form.Group>
                                                <Form.Group className="fg">
                                                    <Form.Control as="select"
                                                                  margin="dense" name="duration"
                                                                  value={values.duration}
                                                                  onChange={handleChange}>
                                                        <option style={{color: '#919399'}}>Selectionner</option>
                                                        <option>jour(s)</option>
                                                        <option>semaine(s)</option>
                                                        <option>mois</option>
                                                        <option>année(s)</option>
                                                    </Form.Control>
                                                    {errors.duration && touched.duration ?
                                                        <h5 className="errors">{errors.duration}</h5>
                                                        : null}
                                                </Form.Group>
                                            </div>
                                        </div>
                                        <Form.Group className="label_description">
                                            <Label className="text-left mt-3">Description</Label>
                                            <Form.Control as="textarea"
                                                          type="text"
                                                          name="description"
                                                          placeholder="description"
                                                          variant="outlined"
                                                          value={values.description}
                                                          margin="dense"
                                                          onChange={handleChange}/>
                                            {errors.description && touched.description ? (
                                                <h5 className="errors">{errors.description}</h5>
                                            ) : null}
                                        </Form.Group>
                                        <div className="formGroupDiv">
                                            <Form.Group className="fg mt-0">
                                                <Label className="text-left mt-3">Etage*</Label>
                                                <Form.Control as="select"
                                                              margin="dense" name="floor"
                                                              variant="outlined"
                                                              value={values.floor}
                                                              onChange={handleChange}>
                                                    <option style={{color: '#919399'}}>Selectionner</option>
                                                    <option>-3</option>
                                                    <option>-2</option>
                                                    <option>-1</option>
                                                    <option>rez-de-chausée(0)</option>
                                                    <option>1er étage</option>
                                                    <option>2ème étage</option>
                                                    <option>3ème étage</option>
                                                </Form.Control>
                                                {errors.floor && touched.floor ? (
                                                    <h5 className="errors">{errors.floor}</h5>
                                                ) : null}
                                            </Form.Group>
                                        </div>
                                        <div className="ml-3">
                                            <Form.Group className="form_checkbox">
                                                <Label className="pt-3 pr-2">Ascenseur</Label>
                                                <Form.Check
                                                    name="hasElevator"
                                                    checked={values.hasElevator}
                                                    value={values.hasElevator}
                                                    onChange={handleChange}
                                                />
                                                <Label style={{marginLeft: '20px'}} className="pt-3 pr-2">Accès
                                                    voiture</Label>
                                                <Form.Check
                                                    name="hasAccessCar"
                                                    checked={values.hasAccessCar}
                                                    value={values.hasAccessCar}
                                                    onChange={handleChange}
                                                />
                                                <Label className="pt-3 pr-2"
                                                       style={{marginLeft: '20px'}}>Securisé
                                                </Label>
                                                <Tooltip
                                                    title={<h5>Caméra, porte verrouillée,... A definir dans la
                                                        description !</h5>}
                                                    enterTouchDelay="50"
                                                    className="pt-2">
                                                    <Info/>
                                                </Tooltip>

                                                <Form.Check
                                                    name="isSecured"
                                                    checked={values.isSecured}
                                                    value={values.isSecured}
                                                    onChange={handleChange}/>
                                            </Form.Group>
                                        </div>
                                        <div className="d-flex flex-row ml-3">
                                            <Form.Group className="formGroupDiv">
                                                <Label className="pt-3 pr-2">Particulier</Label>
                                                <Form.Check
                                                    type="radio"
                                                    name="isPro"
                                                    value={!values.isPro}
                                                    checked={values.isPro === "false"}
                                                    onChange={() => setFieldValue("isPro", "false")}
                                                />
                                            </Form.Group>
                                            <Form.Group className="formGroupDiv ml-3">
                                                <Label className="pt-3 pr-2">Professionnel</Label>
                                                <Form.Check
                                                    type="radio"
                                                    name="isPro"
                                                    value={values.isPro}
                                                    checked={values.isPro === "true"}
                                                    onChange={() => setFieldValue("isPro", "true")}
                                                />
                                            </Form.Group>
                                        </div>


                                        <div style={{textAlign: "center"}}>
                                            <ButtonGroup vertical style={{alignSelf: "center"}}>
                                                <Button variant="success" type="submit"
                                                        className="bg-success" disabled={!imagesPreview.length > 0}

                                                >
                                                    Confirmer
                                                </Button>
                                            </ButtonGroup>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </CardContent>
                    </Card>
                </div>
                <div className="containerMap">
                    <Card className="mapAddPlaceFormBottom">
                        <CardContent>
                            <MapContainer
                                center={latLng}
                                zoom={15}
                                style={{
                                    marginTop: '15px',
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    height: '400px',
                                    width: '100%',
                                }}
                            >
                                <TileLayer
                                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                />
                                <ClickMapComponent/>
                            </MapContainer>
                        </CardContent>
                    </Card>
                    <br/>
                    <ImageUploading
                        multiple
                        value={imagesPreview}
                        onChange={onChangeImage}
                        maxNumber={10}
                        dataURLKey="data_url"
                        maxFileSize={5242880}
                        onError="Le fichier ne peut pas être uploadé"
                        acceptType={["jpg", "png"]}
                    >
                        {({
                              imageList,
                              onImageUpload,
                              onImageRemoveAll,
                              onImageUpdate,
                              onImageRemove,
                              isDragging,
                              dragProps
                          }) => (
                            <Card className="mb-5">
                                <div className="upload__image-wrapper">
                                    <div className="mx-auto">
                                        <Button
                                            className="bg-success mb-2"
                                            style={isDragging ? {color: "red"} : null}
                                            onClick={onImageUpload}
                                            {...dragProps}
                                        >
                                            Ajouter des images (max 10)*
                                        </Button>
                                        &nbsp;
                                        <Button className="bg-success mb-2" onClick={onImageRemoveAll}>Annuler</Button>
                                    </div>
                                    {imageList.length > 0 ?
                                        <div>
                                            <div className={classes.root}>
                                                <GridList cellHeight={300} spacing={30} className={classes.gridList}>
                                                    <GridListTile key="Subheader" cols={4} style={{height: 'auto'}}>
                                                        <ListSubheader component="div"/>
                                                    </GridListTile>
                                                    {imageList.map((image, index) => (
                                                        <GridListTile key={index}>
                                                            <img src={image.data_url} alt="" width="300px"
                                                                 height='300px'
                                                                 onClick={() => onImageUpdate(index)}/>
                                                            <GridListTileBar
                                                                actionIcon={
                                                                    <IconButton className={classes.icon}>
                                                                        <DeleteForever
                                                                            onClick={() => onImageRemove(index)}/>
                                                                    </IconButton>
                                                                }
                                                            />
                                                        </GridListTile>
                                                    ))}
                                                </GridList>
                                            </div>
                                            <div>
                                                <Button className="bg-success mt-2"
                                                        onClick={() => handleValidate(imageList)}>Veuillez
                                                    confirmer</Button>
                                            </div>
                                        </div>
                                        :
                                        <img style={{
                                            marginTop: '40px',
                                            marginLeft: 'auto',
                                            marginRight: 'auto',
                                            marginBottom: '35px',
                                            height: "200px",
                                            width: "200px",
                                        }} src={Image}/>

                                    }
                                </div>
                            </Card>
                        )}
                    </ImageUploading>
                </div>
            </div>
        </div>
    );
};

export default FormPlace;