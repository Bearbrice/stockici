// FAQ page
import React, {useContext} from "react";
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import logo from "../assets/StockIci-Logo-small.png";
import garage from "../assets/garage.png";
import calculation from "../assets/calculate.png";
import DescriptionIcon from "@material-ui/icons/Description";
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import "../App.css";
import {ThemeContext, themes} from "../context/ThemeContext";

//style for the accordion
const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        width: '90%',
        margin: '0 auto 0 auto',
        backgroundColor: '#E5E3E2',
        padding: '20px'
    },
    table: {
        minWidth: 650,
    },
}));

export default function FAQ() {

    const themeContext = useContext(ThemeContext);

    //Create the row of the table
    function createData(name, surface, volume, prix) {
        return {name, surface, volume, prix};
    }

    //data of the table
    const rows = [
        createData('Garage', '-', '-', '130 CHF'),
        createData('Cave, entrepôt, local', '1 à 2 m2', '2 à 6 m3', '40 CHF'),
        createData('', '3 à 4 m2', '7 à 12 m3', '60 CHF'),
        createData('', '5 à 7 m2', '13 à 20 m3', '90 CHF'),
        createData('', '8 à 12 m2', '21 à 35 m3', '130 CHF'),
    ];

    //use the style we created
    const classes = useStyles();

    return (
        <div className="faqPage">
            <div className="faqTitle" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadowTitle
            }}>
                <h1 className="title1">FAQ - Questions & Réponses</h1>
            </div>
            <div className="faqAccordion" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadow
            }}>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title">Qui sommes-nous ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="faqTypo">
                            <div className="textPart">
                                <p>Stock ici propose des solutions de stockage faciles. Notre plateforme de co-stockage
                                    aide nos
                                    partenaires qui veulent entreposer, louer ou optimiser de la place libre. En toute
                                    confiance,
                                    Stock ici souhaite valoriser la proximité, la durabilité et le partage avec vous.
                                </p>
                                <p style={{color: 'green'}}> Rent easy, Stock ici !</p>
                                <p>Nous sommes une plateforme communautaire de location et réservation d’espaces de
                                    stockage.</p>
                                <p>L’inscription, les demandes de réservation et le dépôt d’annonces sont gratuits et se
                                    font par
                                    le biais d’une inscription sur notre site !</p>
                            </div>
                            <div className="mx-auto">
                                <img src={logo} height="150" alt="logo"/>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title">Quels types d’espace peut-on trouver en location sur Stock
                            ici
                            ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="faqTypo">
                            <div className="textPart">
                                <p>Les espaces mis en ligne sont aussi variés que les biens à stocker. Les espaces sont
                                    des
                                    espaces
                                    de co-stockage et non des boxes. Nous recevons fréquemment des demandes pour des
                                    locations
                                    de
                                    <b> caves</b>, de <b>garages</b> ou <b>place de parc</b>. Les <b>hangars, ateliers,
                                        entrepôts</b> ou <b>pièces vides</b> de votre
                                    maison peuvent aussi être mis en location.</p>
                            </div>
                            <div className="mx-auto">
                                <img src={garage} height="100" alt="garage"/>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title">Comment déposer une annonce sur stock ici ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="faqTypo">
                            <div className="textPart">
                                <p>Rendez-vous sur la page d’accueil et cliquez sur le bouton J’ai un espace à louer.
                                    Remplissez
                                    le
                                    formulaire en ligne, avec une description précise et des photos de votre espace de
                                    stockage.</p>
                            </div>
                            <div className="mx-auto">
                                <DescriptionIcon style={{height: '100px', width: '100px', color: 'lightgreen'}}/>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title">Qui peut déposer une annonce ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="faqTypo">
                            <div className="textPart">
                                <p>Toute personne âgée de 18 ans et plus peut déposer une annonce. Vous n’avez rien à
                                    payer,
                                    l’inscription et la mise en ligne d’annonces sont gratuites. Si vous n’êtes <b>pas
                                        propriétaire</b> de l’espace que vous souhaitez mettre en ligne, vous devez vous
                                    assurer
                                    de bénéficier de <b>l’accord
                                        express et écrit du propriétaire.</b></p>
                            </div>
                            <div className="mx-auto">
                                <SupervisorAccountIcon style={{height: '100px', width: '100px', color: 'lightgreen'}}/>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title">Comment les tarifs garde-meuble sont-ils calculés
                            ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="faqTypo">
                            <div className="textPart">
                                <p>Les prix sont librement <b>fixés par les propriétaires.</b> Vous connaissez l’espace
                                    que
                                    vous
                                    mettez en
                                    location, nous vous laissons arbitrer cette question.<br/><br/>

                                    Vous devez déterminer un prix garde-meuble au mois, quelle que soit la durée
                                    minimale et
                                    maximale de location que vous fixez par ailleurs. Pour vous aider, nous vous
                                    indiquons
                                    un
                                    prix
                                    moyen en <b>fonction de la situation géographique</b> de votre espace au moment du
                                    dépôt
                                    d’annonce.
                                    Vous pouvez également consulter notre page tarif garde-meuble.</p>
                            </div>
                            <div className="mx-auto">
                                <img src={calculation} height="100" alt="calculator"/>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title"> Combien coûte le dépôt d’une annonce ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="faqTypo">
                            <div className="textPart">
                                <p>Déposer une annonce est parfaitement gratuit tout comme l’inscription sur le site.
                                    Créez vous un compte dès maintenant.</p>
                            </div>
                            <div className="mx-auto">
                                <MonetizationOnIcon style={{height: '100px', width: '100px', color: 'lightgreen'}}/>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className="faqCard_title"> Combien peut-on gagner grâce à la location ?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div>
                                <p>Les prix sont librement <b>fixés par les propriétaires.</b> Vous connaissez l’espace
                                    que
                                    vous
                                    mettez en
                                    location, nous vous laissons arbitrer cette question.<br/><br/>

                                    Vous devez déterminer un prix garde-meuble au mois, quelle que soit la durée
                                    minimale et
                                    maximale de location que vous fixez par ailleurs. Pour vous aider, nous vous
                                    indiquons
                                    un
                                    prix
                                    moyen en <b>fonction de la situation géographique</b> de votre espace au moment du
                                    dépôt
                                    d’annonce.
                                    Vous pouvez également consulter notre page tarif garde-meuble.</p>
                            </div>
                            <div className="scrollableTable">
                                <TableContainer component={Paper} className="tableFaq">
                                    <Table className={classes.table} aria-label="simple table">
                                        <TableHead>
                                            <TableRow className="tableTitle">
                                                <TableCell>Emplacements</TableCell>
                                                <TableCell align="right">Surface</TableCell>
                                                <TableCell align="right">Volume</TableCell>
                                                <TableCell align="right">Prix</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {rows.map((row) => (
                                                <TableRow key={row.name}>
                                                    <TableCell component="th" scope="row">
                                                        {row.name}
                                                    </TableCell>
                                                    <TableCell align="right">{row.surface}</TableCell>
                                                    <TableCell align="right">{row.volume}</TableCell>
                                                    <TableCell align="right">{row.prix}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            </div>
        </div>

    )


}
