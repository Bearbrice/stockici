// details page for a place
import React, {useContext, useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import firebase from "../firebase";
import {firebaseAuth} from "../provider/AuthProvider";
import {Card, CardContent, GridListTileBar, IconButton, ListSubheader, Tooltip} from "@material-ui/core";
import {Formik} from "formik";
import {Form} from "react-bootstrap";
import Label from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import {MapContainer, TileLayer, useMapEvent} from "react-leaflet";
import {useAlert} from "react-alert";
import ImageUploading from "react-images-uploading";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import {DeleteForever, Info} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core/styles";
import {ThemeContext, themes} from "../context/ThemeContext";

//Style for the grid layout of the pictures
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        marginTop: '20px',
        borderRadius: '5px',
    },
    gridList: {
        width: '100%',
        height: '330px',

    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

const EditForm = () => {

    const themeContext = useContext(ThemeContext);
    const classes = useStyles();
    const {user} = useContext(firebaseAuth);
    const alert = useAlert();
    let {slugIdPlace} = useParams();
    let history = useHistory();

    //useState for the data
    const [address, setAddress] = useState("");
    const [type, setType] = useState("");
    const [areaAvailable, setAreaAvailable] = useState("");
    const [height, setHeight] = useState("");
    const [description, setDescription] = useState("");
    const [floor, setFloor] = useState("");
    const [hasElevator, setHasElevator] = useState(false);
    const [hasAccessCar, setHasAccessCar] = useState(false);
    const [isPro, setIsPro] = useState(false);
    const [isSecured, setIsSecured] = useState(false);
    const [minDuration, setMinDuration] = useState("");
    const [price, setPrice] = useState("");
    const [duration, setDuration] = useState("");
    const [lat, setLat] = useState("46.35");
    const [lng, setLng] = useState("7.15");
    const [imagesPreview, setImagesPreview] = useState([]);
    const [photoURLs, setPhotoURLs] = useState([]);
    const isVerified = true;

    //Database reference
    const ref = firebase.firestore().collection('users').doc(user.uid).collection('places');

    //initial value of the form are current value of the database
    let initialValues = {
        type: type,
        area: areaAvailable,
        address: address,
        price: price,
        duration: duration,
        minDuration: minDuration,
        description: description,
        floor: floor,
        isSecured: isSecured,
        hasElevator: hasElevator,
        isPro: isPro,
        lat: lat,
        lng: lng,
    }

    //Function to create a new marker and get coordonates by clicking
    function AddMarker() {
        const map = useMapEvent({
                click: (e) => {
                    GetAddressFromlatlng(e.latlng.lat, e.latlng.lng);
                }
            }
        )
        return null
    }

    async function GetLatLngFromAddress(addressGiven) {

        const uriAddress = encodeURIComponent(addressGiven);

        let url = `https://us1.locationiq.com/v1/search.php?key=pk.66121da29d61040d838ccd691bee0b3c&normalizeaddress=1&format=json&q=${uriAddress}`;
        let response = await fetch(url, {});
        let data = await response.json();
        if (data !== null && data[0] !== undefined) {
            setLat(data[0].lat);
            setLng(data[0].lon);
            setAddress(addressGiven);
            alert.show("Adresse vérifiée !");
        } else {
            alert.show("Adresse non-valide !");
        }
    }

    //function to get address from longitude and latitude
    //using LocationIQ's API
    async function GetAddressFromlatlng(lat, lng) {
        let url = `https://us1.locationiq.com/v1/reverse.php?key=pk.66121da29d61040d838ccd691bee0b3c&format=json&lat=${lat}&lon=${lng}`;
        let response = await fetch(url, {});
        let data = await response.json();
        if (data !== null || data !== undefined) {
            setLat(lat);
            setLng(lng);
            setAddress(data.address.road + ", " + data.address.postcode + ", " + data.address.city);
        }
    }


    //GET FUNCTION
    async function getPlace() {
        let snapshot = await ref.get();
        const items = [];
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }

        snapshot.forEach(doc => {
            if (doc.id === slugIdPlace) {
                items.push(doc.data());
            }

        });

        //get place's information
        setAddress(items[0].address);
        setType(items[0].type);
        setAreaAvailable(items[0].areaAvailable);
        setDescription(items[0].description);
        setFloor(items[0].floor);
        setHasElevator(items[0].hasElevator);
        setHasAccessCar(items[0].hasAccessCar);
        setIsPro(items[0].isPro);
        setIsSecured(items[0].isSecured);
        setMinDuration(items[0].minDuration);
        setPrice(items[0].price);
        setDuration(items[0].duration);
        setLat(items[0].lat);
        setLng(items[0].lng);
        setImagesPreview(items[0].photoURLs);
        setPhotoURLs(items[0].photoURLs);
        setHeight(items[0].height);
    }


    //Function to edit the place
    function editPlace(updatedPlace) {

        ref
            .doc(slugIdPlace)
            .update(updatedPlace)
            .catch((err) => {
                console.error(err);
            });

        // Simulate submitting to database, shows us values submitted, resets form
        setTimeout(() => {
            alert.show("Vos modifications ont été enregistrées");
        }, 500);
    }


    async function handleValidate(imageList) {
        console.log(imageList);
        //set the images preview with the list
        setImagesPreview(imageList);

        await uploadImages(slugIdPlace);
        alert.show("Photo(s) validée(s) !");
    }

    async function uploadImages(id) {
        //Upload the image(s) in the user'id, place's id
        const storageRef = firebase.storage().ref('places').child('/' + user.uid).child('/' + id);
        imagesPreview.forEach((image) => {
            if (image.file !== undefined) {
                storageRef.child(image.file.name)
                    .put(image.file)
                    .then(async (snap) => {
                        console.log('upload successful', snap)
                        const url = await storageRef.child(image.file.name).getDownloadURL();
                        setPhotoURLs(photoURLs => [...photoURLs, url]);
                    })
                    .catch((err) => console.error('error uploading file', err))
            } else {
                return;
            }
        })
    }


    const onChangeImage = (imageList, addUpdateIndex) => {
        console.log(imageList);
        //set the images preview with the list
        setImagesPreview(imageList);
    };

    useEffect(() => {
        getPlace();
    }, [])

    return (
        <div className="editContainer">
            <div className="faqTitle mb-3" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadowTitle
            }}>
                <h1 className="title1">Modifier mon emplacement</h1>
            </div>
            <div className="mapFormulaireContainer" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadow
            }}>
                <div className="formContainer">
                    <Card>
                        <CardContent className="formCard">
                            <div>
                                <Formik initialValues={initialValues}>
                                    <Form>
                                        <div className="formGroupDiv">
                                            <Form.Group className="fg">
                                                <Label>Type*</Label>
                                                <Form.Control as="select"
                                                              margin="dense" name="type" value={type}
                                                              onChange={(e) => {
                                                                  setType(e)
                                                              }}>
                                                    <option>Garage</option>
                                                    <option>Cave</option>
                                                    <option>Local</option>
                                                    <option>Entrepôt</option>
                                                    <option>Place de parking ext.</option>
                                                    <option>Place de parking int.</option>
                                                    <option>Autre</option>
                                                </Form.Control>
                                            </Form.Group>
                                            <Form.Group className="fg">
                                                <Label>Taille en m2*</Label>
                                                <Form.Control
                                                    type="number"
                                                    name="areaAvailable"
                                                    placeholder="20"
                                                    onChange={(e) => setAreaAvailable(e.target.value)}
                                                    value={areaAvailable}
                                                />
                                            </Form.Group>
                                            <Form.Group className="fg">
                                                <Label>Hauteur* en mètre(s)</Label>
                                                <Form.Control
                                                    type="number"
                                                    name="height"
                                                    onChange={(e) => setHeight(e.target.value)}
                                                    value={height}
                                                />
                                            </Form.Group>
                                        </div>
                                        <Form.Group className="fg">
                                            <Label style={{paddingTop: '20px'}}>Cliquez sur la carte pour obtenir
                                                directement
                                                l'adresse !</Label>
                                            <Form.Control
                                                type="text"
                                                name="address"
                                                variant="outlined"
                                                value={address}
                                                margin="dense"
                                                onChange={(e) => setAddress(e.target.value)}
                                            />
                                            <Button
                                                onClick={function (event) {
                                                    GetLatLngFromAddress(address);
                                                }}
                                                className="bg-success mt-3"
                                            >Vérifier l'adresse</Button>
                                        </Form.Group>
                                        <div className="formGroupDiv">
                                            <Form.Group className="fg">
                                                <Label>Prix (CHF)*</Label>
                                                <Form.Control
                                                    type="number"
                                                    name="price"
                                                    value={price}
                                                    onChange={(e) => setPrice(e.target.value)}
                                                />
                                            </Form.Group>
                                            <Form.Group className="fg">
                                                <Label> Durée*</Label>
                                                <Form.Control as="select"
                                                              margin="dense" name="duration"
                                                              value={duration}
                                                              onChange={(e) => setDuration(e.target.value)}>
                                                    <option style={{color: '#919399'}}>Selectionner</option>
                                                    <option>jour(s)</option>
                                                    <option>semaine(s)</option>
                                                    <option>mois</option>
                                                    <option>année(s)</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </div>
                                        <div className="d-flex flex-column mt-3">
                                            <Label className="text-left pl-3">Durée minimale* de location</Label>
                                            <div className="d-flex flex-row">
                                                <Form.Group className="fg-width">
                                                    <Form.Control
                                                        type="number"
                                                        name="minDuration"
                                                        value={minDuration}
                                                        onChange={(e) => setMinDuration(e.target.value)}/>
                                                </Form.Group>
                                                <Form.Group className="fg-width">
                                                    <Form.Control as="select"
                                                                  margin="dense" name="duration"
                                                                  value={duration}
                                                                  onChange={(e) => setDuration(e.target.value)}>
                                                        <option style={{color: '#919399'}}>Selectionner</option>
                                                        <option>jour(s)</option>
                                                        <option>semaine(s)</option>
                                                        <option>mois</option>
                                                        <option>année(s)</option>
                                                    </Form.Control>
                                                </Form.Group>
                                            </div>
                                        </div>
                                        <Form.Group className="ml-3 mt-3">
                                            <Label className="text-left mt-3">Description</Label>
                                            <Form.Control as="textarea"
                                                          type="text"
                                                          name="description"
                                                          placeholder="description"
                                                          variant="outlined"
                                                          value={description}
                                                          margin="dense"
                                                          onChange={(e) => setDescription(e.target.value)}/>
                                        </Form.Group>
                                        <Form.Group className="fg-width">
                                            <Label className="text-left mt-3">Etage*</Label>
                                            <Form.Control as="select"
                                                          margin="dense" name="floor"
                                                          variant="outlined"
                                                          value={floor}
                                                          onChange={(e) => setFloor(e.target.value)}>
                                                <option style={{color: '#919399'}}>Selectionner</option>
                                                <option>-3</option>
                                                <option>-2</option>
                                                <option>-1</option>
                                                <option>rez-de-chausée(0)</option>
                                                <option>1er étage</option>
                                                <option>2ème étage</option>
                                                <option>3ème étage</option>
                                            </Form.Control>
                                        </Form.Group>
                                        <div className="ml-3">
                                            <Form.Group className="form_checkbox">
                                                <Label className="pt-3 pr-2">Ascenseur</Label>
                                                <Form.Check
                                                    name="hasElevator"
                                                    checked={hasElevator}
                                                    value={hasElevator}
                                                    onChange={(e) => setHasElevator(e.target.checked)}
                                                />
                                                <Label style={{marginLeft: '20px'}} className="pt-3 pr-2">Accès
                                                    voiture</Label>
                                                <Form.Check
                                                    name="hasAccessCar"
                                                    checked={hasAccessCar}
                                                    value={hasAccessCar}
                                                    onChange={(e) => setHasAccessCar(e.target.checked)}
                                                />
                                                <Label className="pt-3 pr-2"
                                                       style={{marginLeft: '20px'}}>Securisé
                                                </Label>
                                                <Tooltip
                                                    title={<h5>Caméra, porte verrouillée,... A definir dans la
                                                        description !</h5>}
                                                    enterTouchDelay="50"
                                                    className="pt-2">
                                                    <Info/>
                                                </Tooltip>

                                                <Form.Check
                                                    name="isSecured"
                                                    checked={isSecured}
                                                    value={isSecured}
                                                    onChange={(e) => setIsSecured(e.target.checked)}/>
                                            </Form.Group>
                                        </div>
                                        <div className="d-flex flex-row ml-3">
                                            <Form.Group className="formGroupDiv">
                                                <Label className="pt-3 pr-2">Particulier</Label>
                                                <Form.Check
                                                    type="radio"
                                                    name="isPro"
                                                    checked={isPro === "false"}
                                                    onChange={() => setIsPro(false)}
                                                />
                                            </Form.Group>
                                            <Form.Group className="formGroupDiv ml-3">
                                                <Label className="pt-3 pr-2">Professionnel</Label>
                                                <Form.Check
                                                    type="radio"
                                                    name="isPro"
                                                    checked={isPro}
                                                    onChange={() => setIsPro(true)}
                                                />
                                            </Form.Group>
                                        </div>
                                        <Button type="submit" className="bg-success"
                                                disabled={!address || !price || duration === "Selectionner" || !minDuration || !floor || type === "Selectionner"}
                                                onClick={async function (event) {
                                                    event.preventDefault();
                                                    await editPlace({
                                                        titleOffer: type + " de " + areaAvailable + " m2",
                                                        type,
                                                        lng,
                                                        lat,
                                                        address,
                                                        areaAvailable,
                                                        description,
                                                        floor,
                                                        hasElevator,
                                                        isPro,
                                                        isSecured,
                                                        isVerified,
                                                        minDuration,
                                                        price,
                                                        duration,
                                                        height,
                                                        hasAccessCar,
                                                        owner: user.uid,
                                                        photoURLs: photoURLs
                                                    });
                                                    history.push('/account/my-places');
                                                }
                                                }
                                        >
                                            Confirmer
                                        </Button>
                                    </Form>
                                </Formik>
                            </div>
                        </CardContent>
                    </Card>
                </div>
                <div className="containerMap">
                    <Card>
                        <CardContent>
                            <MapContainer
                                center={[lat, lng]}
                                zoom={15}
                                style={{
                                    marginTop: '15px',
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    height: '400px',
                                    width: '100%',
                                }}
                            >
                                <TileLayer
                                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                />
                                <AddMarker/>
                            </MapContainer>
                        </CardContent>
                    </Card>
                    <br/>
                    <ImageUploading
                        multiple
                        value={imagesPreview}
                        onChange={onChangeImage}
                        maxNumber={7}
                        dataURLKey="data_url"
                        maxFileSize={5242880}
                        onError="Le fichier ne peut pas être uploadé"
                        acceptType={["jpg", "png"]}
                    >
                        {({
                              imageList,
                              onImageUpload,
                              onImageRemoveAll,
                              onImageUpdate,
                              onImageRemove,
                              isDragging,
                              dragProps
                          }) => (
                            // write your building UI
                            <div className="upload__image-wrapper">
                                <div className="mx-auto">
                                    <Button
                                        className="bg-success mb-2"
                                        style={isDragging ? {color: "red"} : null}
                                        onClick={onImageUpload}
                                        {...dragProps}
                                    >
                                        Ajouter des images (max 10)*
                                    </Button>
                                    &nbsp;
                                    <Button className="bg-success mb-2" onClick={onImageRemoveAll}>Annuler</Button>
                                </div>
                                <div className={classes.root}>
                                    <GridList cellHeight={300} spacing={30} className={classes.gridList}>
                                        <GridListTile key="Subheader" cols={4} style={{height: 'auto'}}>
                                            <ListSubheader component="div"/>
                                        </GridListTile>
                                        {imageList.map((image, index) => (
                                            <GridListTile key={index}>
                                                {image.data_url ?
                                                    <img src={image.data_url} alt="" width="300px" height='300px'
                                                         onClick={() => onImageUpdate(index)}/>
                                                    :
                                                    <img src={image} alt="" width="300px" height='300px'
                                                         onClick={() => onImageUpdate(index)}/>
                                                }
                                                <GridListTileBar
                                                    actionIcon={
                                                        <IconButton className={classes.icon}>
                                                            <DeleteForever onClick={() => onImageRemove(index)}/>
                                                        </IconButton>
                                                    }
                                                />
                                            </GridListTile>
                                        ))}
                                    </GridList>
                                </div>
                                <div>
                                    {imageList.length > 0 ?
                                        <Button className="bg-success mt-2" onClick={() => handleValidate(imageList)}>Veuillez
                                            confirmer</Button>
                                        : null}
                                </div>

                            </div>
                        )}
                    </ImageUploading>
                </div>

            </div>
        </div>
    )
}

export default EditForm;
