import {Card, CardContent} from "@material-ui/core";
import {Button, CardTitle} from "reactstrap";
import {FaHandshake} from 'react-icons/fa';
import {useHistory} from "react-router";
import {ThemeContext, themes} from "../context/ThemeContext";
import {useContext} from "react";

export default function ConfirmationPage() {

    let history = useHistory();
    const themeContext = useContext(ThemeContext);

    return (
        <div className="confirmationPage">
            <Card className="confirmationCard" style={{
                backgroundColor: themes[themeContext.theme].cardContent,
                boxShadow: themes[themeContext.theme].cardShadow
            }}>
                <FaHandshake style={{height: '150px', width: '150px', color: 'green'}}/>
                <CardTitle>
                    <h1 className="title1">Votre demande de location a été transmise au propriétaire !</h1>
                </CardTitle>
                <CardContent>
                    <h2 className="title2 confirmationSubtitle">Vous recevrez une notification lorsque la demande sera
                        validée.</h2>
                    <Button className="bg-success border-success m-3" onClick={() => {
                        history.push('/home');
                    }}>Retour à la page d'accueil</Button>
                </CardContent>
            </Card>
        </div>
    )

}
