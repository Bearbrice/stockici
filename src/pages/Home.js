// Home page
import React from "react";
import VidHome from "../assets/videoHome.mp4"
import {NavBtnCalculator, NavBtnHome} from '../components/Navbar/NavbarElements';

const VideoJS = () => {
    return (
        <div className="videoDiv">
            <video controls loop muted={true} autoPlay={true}>
                <source src={VidHome} type="video/mp4"/>
            </video>
        </div>
    )
}

export default function Home() {
    // const currentUser = useContext(UserContext);

    return (
        <div className="homePage flex-fill">
            {/*<p>INFO LOGIN</p>
            {/*{currentUser &&  <p>{currentUser.displayName}</p>}*/}
            <div className="d-flex flex-row justify-content-center">
                <NavBtnHome
                    className=""
                    to="/map"
                    // style={{color: '#efeaea'}}
                >J'ai besoin d'espace ! <span>🔍</span>
                </NavBtnHome>
                <NavBtnHome
                    className=""
                    to="/form"
                    // style={{color: '#efeaea'}}
                >Je possède un bien louable ! <span>🏷️</span>
                </NavBtnHome>
            </div>
            <VideoJS/>
            <div className="divBg">
                <div className="d-flex flex-row justify-content-center ">
                    <NavBtnCalculator
                        to="/calculator"
                        style={{width: '45%'}}
                    >Calculateur de prix <span>💲</span>
                    </NavBtnCalculator>
                    <NavBtnCalculator
                        to="/surface_calculator"
                        style={{width: '45%'}}
                    >Calculateur de surface <span>📦</span>
                    </NavBtnCalculator>
                </div>
            </div>
        </div>
    )
}