// Map and list
import React, {useContext, useEffect, useState} from "react";
import {CircleMarker, MapContainer, Popup, TileLayer} from "react-leaflet";
import label, {Button, Dropdown} from "react-bootstrap";
import {Slider} from "@material-ui/core";
import firebase from "../../firebase";
import {SplashScreen} from "../../components/SplashScreen";
import {UserLocationContext} from "../../context/UserLocationContext";
import moment from "moment";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";
import {ThemeContext, themes} from "../../context/ThemeContext";
import {useHistory} from "react-router-dom";

export default function MapSearch() {

    //Context to get the user localization
    const {latLng} = useContext(UserLocationContext);
    //Context to get the theme
    const themeContext = useContext(ThemeContext);
    const history = useHistory();

    //function that handle the map & the filters
    const Map = () => {

        //useState for the search filters
        const [price, setPrice] = useState([10, 1000]);
        const [space, setSpace] = useState([2, 500]);
        const [npa, setNpa] = useState('');
        const [date, setDate] = useState();
        const [radioB, setRadioB] = useState();
        const [valueDD, setValueDD] = useState("Tout");
        const [loading, setLoading] = useState(false);

        //useState to display filtered places
        const [places, setPlaces] = useState();
        //useState for all the places to reset the filters
        const [allPlaces, setAllPlaces] = useState();
        //useState to show the filter or not in mobile screen
        const [showFilters, setShowFilters] = useState();
        //useState to get unvailable dates for each place
        const [disabledDays, setDisabledDays] = useState();
        //useState for the place we selected
        const [clicked, setClicked] = useState(false);
        const [selectedPlace, setSelectedPlace] = useState([]);

        //Reference to Firestore, all the places that are verified
        const ref = firebase.firestore().collectionGroup('places').where('isVerified', '==', true);
        //Reference to Firestore, all the rentals
        const refRentals = firebase.firestore().collection("rentals");

        //function to show/hide the filters on mobile screen
        const showHideFilters = () => setShowFilters(!showFilters);

        //function to handle the selected place
        const handleSelected = (place) => {
            setSelectedPlace(place);

            const timer = setTimeout(() => {
                setClicked(true);
            }, 500);

            return () => clearTimeout(timer);
        }

        //function to handle the search filters
        const handleSelect = (e) => {
            setValueDD(e)
        }

        const handlePrice = (e, newValue) => {
            setPrice(newValue);
        }

        const handleSpace = (e, newValue) => {
            setSpace(newValue);
        }

        const handleNPA = (e) => {
            setNpa(e.target.value);
        }

        const handleDate = (e) => {
            setDate(e.target.value);
        }

        const handleRadioB = (e) => {
            setRadioB(e.target.value);
        }

        //function to search with de chosen filters
        const search = (e) => {
            setNpa(npa);
            setDate(date);
            setValueDD(valueDD);
            setPrice(price);
            setSpace(space);
            setRadioB(radioB);
            e.preventDefault();

            //then filter the result
            getFilteredPlaces();
        }

        //Function to filter the result
        const getFilteredPlaces = () => {
            let pro = false;
            let filteredItems = [];

            if (radioB === 'Professionel') {
                pro = true;
            }

            //filter price & space
            filteredItems = allPlaces.filter(p => p.data.price <= price[1] && p.data.price >= price[0] && p.data.areaAvailable >= space[0] && p.data.areaAvailable <= space[1]);

            //filter radio button
            if (radioB !== undefined) {
                filteredItems = filteredItems.filter(p => p.data.isPro === pro)
            }

            //filter dropdown value
            if (valueDD !== 'Tout') {
                filteredItems = filteredItems.filter(p => p.data.type === valueDD);
            }

            //filter the localization
            if (npa !== '') {
                filteredItems = filteredItems.filter(p => (p.data.address).toLowerCase().indexOf(npa.toLowerCase()) !== -1);
            }

            //filter the date
            if (date !== undefined) {
                {
                    disabledDays.map((dd) => {
                        if (!(dd.from.toLocaleDateString() === moment(date).format("DD/MM/yyyy"))) {
                            filteredItems = filteredItems.filter(p => p.id === dd.id);
                        }
                    })
                }
            }

            setPlaces(filteredItems);
        }

        //GET FUNCTION

        //Get all the verified places
        function getAllPlaces() {
            ref.onSnapshot((querySnapshot) => {
                const items = [];
                querySnapshot.forEach((doc) => {
                    getRating(doc.id).then(r =>
                        items.push({
                            id: doc.id,
                            data: doc.data(),
                            rating: r
                        })
                    )
                });
                setPlaces(items);
                setAllPlaces(items);
            });
        }

        //Get the rate of each place
        async function getRating(id) {
            const refComments = firebase.firestore().collectionGroup("comments");
            let somme = 0;
            let moyenne = 0;
            let cpt = 0;
            const snapshot = await refComments.where('placeId', '==', id).where('isValidate', '==', true).get();

            if (snapshot.empty) {
                console.log('No matching documents.');
                return;
            }
            snapshot.forEach((doc) => {
                //average rate
                if (doc.data().rating !== 0) {
                    cpt++;
                    somme = somme + doc.data().rating;
                }

            });

            //calculate the average of each place
            moyenne = somme / cpt;
            return Math.round(moyenne);
        }


        //Get existing reservation to have unavailable dates
        async function getRentals() {
            refRentals.onSnapshot((querySnapshot) => {
                const items = [];
                querySnapshot.forEach((doc) => {
                    items.push({
                        id: doc.data().placeId,
                        from: doc.data().startDate.toDate(),
                        to: doc.data().endDate.toDate()
                    });
                });
                setDisabledDays(items);
            });
        }

        //Function to reinitialize filters
        const reinitialize = () => {
            setValueDD('Tout');
            setPrice([10, 1000]);
            setSpace([2, 500]);
            setNpa('');
            setClicked(false);
            setPlaces(allPlaces);
        }

        //function to see the details page of the selected place
        function goToDetails(id) {
            history.push(`/details/${id}`);
        }


        useEffect(() => {
            getAllPlaces();
            getRentals();
            const timer = setTimeout(() => {
                setLoading(true);
            }, 1000);

            return () => clearTimeout(timer);

        }, []);


        return (
            <div className="mapPage">
                {loading ?
                    <>
                        <Button className="displayFilters" onClick={showHideFilters}>Filtrer</Button>
                        {showFilters || window.innerWidth > 900 ?
                            <div className="filtersBox card" style={{
                                backgroundColor: themes[themeContext.theme].cardContent,
                                boxShadow: themes[themeContext.theme].cardShadow
                            }}>
                                <form onSubmit={search}>
                                    <div className="filters">
                                        <div className="form-group mr-5">
                                            <label className="labelFilters" htmlFor="cityLabel">Ville, NPA</label>
                                            <input className="form-control" id="cityInput" placeholder="1920"
                                                   type='text'
                                                   onChange={handleNPA}/>
                                        </div>
                                        <div className="form-group mr-5">
                                            <label className="labelFilters" htmlFor="dateLabel">Date</label>
                                            <input className="form-control" type="date" id="dateInput"
                                                   onChange={handleDate}/>
                                        </div>
                                        <div className="form-group mr-5">
                                            <label className="labelFilters mb-0" htmlFor="typeLabel"
                                                   style={{width: '190px'}}>Type
                                                d'emplacement</label>
                                            <Dropdown onSelect={handleSelect}>
                                                <Dropdown.Toggle id="dropdown-basic"
                                                                 style={{width: '190px', textAlign: 'left'}}
                                                                 className="bg-success border-success">
                                                    {valueDD}
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu>
                                                    <Dropdown.Item eventKey="Tout">Tout</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Autre">Autre</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Cave">Cave</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Entrepôt">Entrepôt</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Garage">Garage</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Local">Local</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Place de parking ext.">Place de
                                                        parking ext.</Dropdown.Item>
                                                    <Dropdown.Item eventKey="Place de parking int.">Place de
                                                        parking int.</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </div>
                                        <div className="form-group mr-5" style={{width: '280px'}}>
                                            <label className="labelFilters" htmlFor="priceLabel">Prix/mois</label>
                                            <br/>
                                            <Slider
                                                value={price}
                                                min={0}
                                                max={1000}
                                                step={5}
                                                onChange={handlePrice}
                                            />
                                            {price[0] === price[1] ?
                                                <p className="labelFilters">{price[1]} CHF/mois</p>
                                                :
                                                <p className="labelFilters">entre {price[0]} et {price[1]} CHF/mois</p>
                                            }
                                        </div>
                                        <div className="form-group mr-5 mb-0" style={{width: '280px'}}>
                                            <label className="labelFilters" htmlFor="spaceLabel">Espace</label>
                                            <br/>
                                            <Slider
                                                value={space}
                                                min={2}
                                                max={500}
                                                onChange={handleSpace}
                                                valueLabelDisplay="auto"
                                                aria-labelledby="range-slider"
                                            />
                                            {space[0] === space[1] ?
                                                <p className="labelFilters">{space[1]} m2</p>
                                                :
                                                <p className="labelFilters">entre {space[0]} et {space[1]} m2</p>
                                            }
                                        </div>
                                    </div>
                                    <div className="filters">
                                        <div className="labelFilters mt-2 mr-5 mb-2">
                                            <input type="radio" value="Particulier" name="pType"
                                                   onChange={handleRadioB}/> Particulier
                                        </div>
                                        <div className="labelFilters mt-2 mr-5 mb-2">
                                            <input type="radio" value="Professionel" name="pType"
                                                   onChange={handleRadioB}/> Professionel
                                        </div>
                                        <Button style={{height: '40px'}} type="submit"
                                                className="btn-success">Rechercher</Button>
                                        <Button style={{height: '40px', marginLeft: '50px'}}
                                                className="bg-primary border-primary"
                                                onClick={reinitialize}>Réinitialiser les filtres</Button>
                                    </div>
                                </form>
                            </div> : null}
                        <div className="leaflet-container" style={{
                            backgroundColor: themes[themeContext.theme].cardContent,
                            boxShadow: themes[themeContext.theme].cardShadow
                        }}>
                            <div className="mapPreview card">
                                {clicked ?
                                    <div className="placeInfoMap">
                                        <div className="d-flex flex-row justify-content-between">
                                            <h5><b>{selectedPlace.data.titleOffer}</b></h5>
                                            {selectedPlace.rating !== undefined ?
                                                <div className="ratingContainer">
                                                    <Box component="fieldset" mb={3} borderColor="transparent">
                                                        <Rating
                                                            value={selectedPlace.rating}
                                                            readOnly
                                                            size='large'
                                                        />
                                                    </Box>
                                                </div>
                                                :
                                                null
                                            }
                                        </div>
                                        <Carousel infiniteLoop="true" renderThumbs={function () {
                                            return null
                                        }}>
                                            {selectedPlace.data.photoURLs.map((pic) => (
                                                <img src={pic} height='250px' width='350px' alt="pic"/>
                                            ))}
                                        </Carousel>
                                        <div className="placeBoxMap text-left">
                                            <div className="placeInfoMap ">
                                                <p className="mapDetailsInfo">{selectedPlace.data.price} CHF/{selectedPlace.data.duration}, {selectedPlace.data.areaAvailable} m2</p>
                                                <p className="mapDetailsInfo">{selectedPlace.data.description}</p>
                                                <Button className="text-white btn-success"
                                                        onClick={() => goToDetails(selectedPlace.id)}
                                                >Voir plus de
                                                    détails</Button>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }
                                <>
                                    <h3 className="card-title pt-4 pl-4 border-bottom ">{places.length} Résultats</h3>
                                    <div style={{backgroundColor: themes[themeContext.theme].cardContent}}>
                                        {places.map((place) => (
                                            <div className="placeInfoMap" key={place.id}>
                                                <div className="d-flex flex-row justify-content-between">
                                                    <h5><b>{place.data.titleOffer}</b></h5>
                                                    {place.rating !== undefined ?
                                                        <div className="ratingContainer">
                                                            <Box component="fieldset" mb={3} borderColor="transparent">
                                                                <Rating
                                                                    value={place.rating}
                                                                    readOnly
                                                                    size='large'
                                                                />
                                                            </Box>
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                </div>
                                                <Carousel infiniteLoop="true" renderThumbs={function () {
                                                    return null
                                                }}>
                                                    {place.data.photoURLs.map((pic) => (
                                                        <img src={pic} height='250px' width='350px' alt="pic"/>
                                                    ))}
                                                </Carousel>

                                                <p className="mapDetailsInfo">{place.data.price} CHF/{place.data.duration}, {place.data.areaAvailable} m2</p>
                                                <p className="mapDetailsInfo">{place.data.description}</p>

                                                <Button className="text-white btn-success"
                                                        onClick={() => goToDetails(place.id)}
                                                    // href={`/details/${place.id}`}
                                                >Voir plus
                                                    de
                                                    détails</Button>
                                            </div>
                                        ))}
                                    </div>
                                </>
                            </div>
                            <MapContainer className="mapSearch"
                                          center={latLng}
                                          zoom={13}
                            >
                                <TileLayer
                                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                />
                                {places.map((place) => (
                                    <CircleMarker center={[place.data.lat, place.data.lng]} radius={100}
                                                  eventHandlers={{
                                                      click: () => {
                                                          handleSelected(place)
                                                      },
                                                  }}>
                                        <Popup>{place.data.titleOffer}, {place.data.price}</Popup>
                                    </CircleMarker>
                                ))}
                            </MapContainer>
                        </div>
                    </>
                    :
                    <div className="text-center">
                        <SplashScreen/>
                    </div>
                }
            </div>

        );
    }
;

return (
    <div className="w-100 d-flex flex-column flex-fill">
        <div className="faqTitle" style={{
            backgroundColor: themes[themeContext.theme].cardContent,
            boxShadow: themes[themeContext.theme].cardShadowTitle
        }}>
            <h1 className="title1">J'ai besoin d'espace</h1>
        </div>

        <Map/>
    </div>
);


}

