import React, {useContext, useEffect, useState} from "react";
import firebase from "../firebase";
import {v4 as uuidv4} from "uuid";
import {firebaseAuth} from "../provider/AuthProvider";

//It provides the operations (CRUD) from FireStore
export function PlaceController() {

    //State hooks
    const [places, setPlaces] = useState([]);
    const [loading, setLoading] = useState(false);
    const [address, setAddress] = useState("");
    const [areaAvailable, setAreaAvailable] = useState("");
    const [description, setDescription] = useState("");
    const [floor, setFloor] = useState("");
    const [isElevator, setIsElevator] = useState(false);
    const [isPro, setIsPro] = useState(false);
    const [isSecured, setIsSecured] = useState(false);
    const [isVerified, setIsVerified] = useState(false);
    const [minDuration, setMinDuration] = useState("");
    const [price, setPrice] = useState("");
    const [rating, setRating] = useState("");
    const [titleOffer, setTitleOffer] = useState("");

    //Reference to the Cloud Database's collection
    const ref = firebase.firestore().collection("place");

    const {user} = useContext(firebaseAuth);

    //GET FUNCTION
    function getAllPlaces() {
        setLoading(true);
        ref.onSnapshot((querySnapshot) => {
            const items = [];
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setPlaces(items);
            setLoading(false);
        });
    }

    useEffect(() => {
        getAllPlaces();
        // eslint-disable-next-line
    }, []);

    // ADD FUNCTION
    function addPlace(newPlace) {
        ref
            .doc(newPlace.id)
            .set(newPlace)
            .catch((err) => {
                console.error(err);
            });
    }

    //DELETE FUNCTION
    function deletePlace(place) {
        ref
            .doc(place.id)
            .delete()
            .catch((err) => {
                console.error(err);
            });
    }

    // EDIT FUNCTION
    function editPlace(updatedPlace) {
        setLoading();
        ref
            .doc(updatedPlace.id)
            .update(updatedPlace)
            .catch((err) => {
                console.error(err);
            });
    }


    return (
        <div className="formPlace-container">
            <div className="form-left">
                <h2>Ajouter mon bien disponible</h2>
                <input
                    type="text"
                    value={titleOffer}
                    placeholder="garage de 50m2 à Riddes"
                    onChange={(e) => setTitleOffer(e.target.value)}
                />
                <input
                    type="text"
                    value={address}
                    placeholder="Av. de la Gare 52"
                    onChange={(e) => setAddress(e.target.value)}
                />
                <input
                    type="text"
                    value={areaAvailable}
                    placeholder="50m2"
                    onChange={(e) => setAreaAvailable(e.target.value)}
                />
                <textarea
                    value={description}
                    placeholder="garage situé à 15min de la gare, porte large de 3m de haut et 4m de large"
                    onChange={(e) => setDescription(e.target.value)}
                />
                <input
                    type="text"
                    value={floor}
                    placeholder="Rez-de-Chaussée"
                    onChange={(e) => setFloor(e.target.value)}
                />
                <input
                    type="text"
                    value={isElevator}
                    // placeholder="garage de 50m2 à Riddes"
                    onChange={(e) => setIsElevator(e.target.value)}
                />
                <input
                    type="text"
                    value={isPro}
                    onChange={(e) => setIsPro(e.target.value)}
                />
                <input
                    type="text"
                    value={isSecured}
                    onChange={(e) => setIsSecured(e.target.value)}
                />
                <input
                    type="text"
                    value={isVerified}
                    onChange={(e) => setIsVerified(e.target.value)}
                />
                <input
                    type="text"
                    value={minDuration}
                    placeholder="1 semaine"
                    onChange={(e) => setMinDuration(e.target.value)}
                />
                <input
                    type="text"
                    value={price}
                    placeholder="150.-/mois"
                    onChange={(e) => setPrice(e.target.value)}
                />
                <input
                    type="text"
                    value={rating}
                    placeholder="4.4"
                    onChange={(e) => setRating(e.target.value)}
                />
                <hr/>
                <button onClick={() => addPlace({
                    titleOffer,
                    address,
                    areaAvailable,
                    description,
                    floor,
                    isElevator,
                    isPro,
                    isSecured,
                    isVerified,
                    minDuration,
                    price,
                    rating,
                    id: uuidv4(),
                    owner: user.uid
                })}>
                    Submit
                </button>
            </div>
            <div className="form-right">

                {loading ? <h1>Loading...</h1> : null}

                {places.map((place) => (
                    <div className="places" key={place.id}>
                        <h2>{place.titleOffer}</h2>
                        <p>{place.description}</p>
                        <div>
                            <button onClick={() => deletePlace(place)}>X</button>
                            {/* <button
                                onClick={() =>
                                    editPlace({ titleOffer: place.titleOffer, address: place.address, areaAvailable: place.areaAvailable, description: place.description, floor: place.floor, isElevator: place.isElevator, isPro: place.isPro, isSecured: place.isSecured, isVerified: place.isVerified, minDuration: place.minDuration, price: place.price, rating: place.rating, id: place.id })
                                }
                            >
                                Edit
                            </button> */}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}


export default PlaceController;