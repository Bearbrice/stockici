import firebase, {auth, firestore} from "../firebase";

// --- Delete methods --- //
export async function deleteUserData(uid, photoURL) {

    await deleteProfilPicture(photoURL);
    // await deletePlacesPicture(uid);
    // await deleteFromUsers(uid);
    // await deleteFromPlaces(uid)
}


const deletePlacesPicture = async (photoURL) => {
    //

}

export const deleteProfilPicture = async (photoURL, uid) => {
    if (!uid) {
        uid = auth.currentUser.uid;
    }

    if (photoURL == null) {
        return;
    }

    const userRef = firestore.doc(`users/${uid}`);
    await userRef.update({
        photoURL: ""
    });


    firebase.storage().refFromURL(photoURL)
        .delete()
        .then((res) => console.log(res))
        .catch((err) => console.log(err));

}

const deleteFromUsers = async (uid) => {
    const userRef = firestore.collection("users").doc(uid);

    await userRef
        .delete()
        .then((res) => console.log("User succesfully deleted" + res))
        .catch((err) => console.log("User NOT deleted" + err))
}

const deleteFromPlaces = async (uid) => {
    firestore.collection("place").where("owner", "==", uid).get()
        .then(querySnapshot => {
            // querySnapshot.docs[0].ref.delete();

            const batch = firestore.batch();

            querySnapshot.forEach(function (doc) {
                // For each doc, add a delete operation to the batch
                batch.delete(doc.ref);
            });

            // Commit the batch
            return batch.commit();
        })
        .catch((err) => console.log(err));
}