import firebase, {firestore} from "../firebase";

export const validateRental = (id) => {

    const ref = firestore.collection("rentals").doc(id)

    ref
        .update({
            isAccepted: true,
            accepted: firebase.firestore.Timestamp.fromDate(new Date())
        })
        .catch((err) => {
            console.error(err);
        });

}

export const deleteRental = (id) => {

    const ref = firestore.collection("rentals").doc(id);

    ref
        .delete()
        .catch((err) => {
            console.error(err);
        });
}