import {firestore} from "../../firebase";

//Public
export function addNotification(uid, title, message, type) {

    // const receiver = auth.currentUser.uid;

    const data = {
        title: title,
        message: message,
        type: type,
        read: false,
        time: Date().toLocaleString()// Date().toLocaleString(),
    }

    pushNotificationToDatabase(uid, data)
}


//Private
const pushNotificationToDatabase = (receiver, data) => {

    const ref = firestore.collection("users").doc(receiver).collection('notifications');

    ref
        .add(data)
        .catch((err) => {
            console.error(err);
        });

}

export const markAsRead = (receiver, id) => {

    const ref = firestore.collection("users").doc(receiver).collection('notifications').doc(id);

    ref
        .update({
            read: true
        })
        .catch((err) => {
            console.error(err);
        });

}

export const markAllAsRead = (receiver) => {
    firestore.collection("users").doc(receiver).collection('notifications')
        .get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                doc.ref
                    .update({
                        read: true
                    }).catch((err) => {
                    console.error(err);
                });
            });
        });
}

export const deleteNotification = (receiver, id) => {

    const ref = firestore.collection("users").doc(receiver).collection('notifications').doc(id);

    ref
        .delete()
        .catch((err) => {
            console.error(err);
        });
}

export async function deleteAllNotifications(receiver) {

    const query = firestore.collection("users").doc(receiver).collection('notifications')

    return new Promise((resolve, reject) => {
        deleteQueryBatch(query, resolve).catch(reject);
    });
}

async function deleteQueryBatch(query, resolve) {

    const snapshot = await query.get();


    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }

    // Delete documents in a batch
    const batch = firestore.batch();
    snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref);
    });
    await batch.commit();

    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        deleteQueryBatch(query, resolve);
    });
}
