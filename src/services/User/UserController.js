import {auth, firestore} from "../../firebase";

export const generateUserDocument = async (user, additionalData) => {

    if (!user) {
        return;
    }

    if (user.NPA != null) {
        return;
    }

    const userRef = firestore.doc(`users/${user.uid}`);
    const snapshot = await userRef.get();

    if (!snapshot.exists) {
        const {email, displayName, photoURL} = user;

        //Default
        const role = "subscriber";
        const firstname = "";
        const lastname = "";
        const address = "";
        const NPA = "";
        const city = "";
        const iban = "";
        const bank = "";
        const provider = auth.currentUser.providerData[0].providerId;
        const completeProfile = false;

        try {
            await userRef.set({
                displayName,
                email,
                photoURL,
                role,
                firstname,
                lastname,
                address,
                NPA,
                city,
                iban,
                bank,
                provider,
                completeProfile,
                ...additionalData
            });
        } catch (error) {
            console.error("Error creating user document", error);
        }
    }
    return getUserDocument(user.uid);
};

const getUserDocument = async uid => {
    if (!uid) return null;

    try {
        const userDocument = await firestore.doc(`users/${uid}`).get();

        return {
            uid,
            ...userDocument.data()
        };
    } catch (error) {
        console.error("Error fetching user", error);
    }
};

export const modifyUserDocument = async (user, additionalData) => {
    if (!user) {
        return;
    }

    const userRef = firestore.doc(`users/${user.uid}`);
    // const snapshot = await userRef.get();

    // if (!snapshot.exists) {
    const {
        photoURL,
        displayName,
        email,
        role,
        firstname,
        lastname,
        address,
        NPA,
        city,
        iban,
        bank,
        completeProfile,
        provider
    } = user;

    // //Default
    // const role = "subscriber";
    // const firstname = "";
    // const lastname = "";
    // const address = "";
    // const NPA = "";
    // const city = "";

    try {
        await userRef.set({
            displayName,
            email,
            photoURL,
            role,
            firstname,
            lastname,
            address,
            NPA,
            city,
            iban,
            bank,
            provider,
            completeProfile,
            ...additionalData
        });
    } catch (error) {
        console.error("Error creating user document", error);
    }
    // }
    return getUserDocument(user.uid);
};







