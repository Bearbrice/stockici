// Manage places
import firebase, {firestore} from "../firebase";
import {addNotification} from "./User/NotificationsController";

// -- Get places --
export const getPendingPlaces = async () => {
    const places = [];

    await firestore.collectionGroup('places')
        .where('isVerified', '==', false)
        .where('isRefused', '==', false)
        // .where('comment', '==', '')
        .get().then(querySnapshot => {
            querySnapshot.forEach(function (doc) {
                places.push({
                    ...doc.data(),
                    isVerified: doc.data().isVerified.toString(),
                    hasAccessCar: doc.data().hasAccessCar.toString(),
                    hasElevator: doc.data().hasElevator.toString(),
                    isSecured: doc.data().isSecured.toString(),
                });
            });
        });

    return places;
}

export async function getPendingComments() {
    const comment = [];
    const snapshot = await firestore.collectionGroup('comments').where('isValidate', "==", false).get();
    if (snapshot.empty) {
        console.log('No matching documents.');
        return;
    }
    snapshot.forEach(doc => {
        const data = doc.data();
        const arr = {
            id: doc.id,
            ...data
        }
        comment.push(arr);
    });
    return comment;
}

// -- Make a place verified (isVerified==true) --
export const verifyPlace = (owner, id) => {
    const ref = firestore.collection("users").doc(owner).collection('places').doc(id);

    ref
        .update({
            isVerified: true,
            comment: ""
        })
        .catch((err) => {
            console.error(err);
        });

    addNotification(owner, "Emplacement accepté", "Un de vos emplacements a été accepté et est désormais disponible à la location", 'places');
}

// -- Make a place go back to validation
export const sendPlaceToValidation = (owner, id) => {
    const ref = firestore.collection("users").doc(owner).collection('places').doc(id);

    ref
        .update({
            isRefused: false,
        })
        .catch((err) => {
            console.error(err);
        });

    // addNotification(owner, "Votre emplacement n'a pas été accepté", comment, 'places');
}

// -- Make a place verified (isVerified==false) + comment --
export const invalidatePlace = (owner, id, comment) => {
    const ref = firestore.collection("users").doc(owner).collection('places').doc(id);

    ref
        .update({
            isRefused: true,
            comment: comment
        })
        .catch((err) => {
            console.error(err);
        });

    addNotification(owner, "Votre emplacement n'a pas été accepté, modifier le et aller dans votre profil pour le soumettre à nouveau", comment, 'places');
}

// -- Make a comment verified (isVerified==true) --
export const verifyComment = (value) => {

    const ref = firebase.firestore().collection("users").doc(value.placeOwner).collection("places").doc(value.placeId).collection("comments").doc(value.id);

    ref
        .update({
            isValidate: true,
            validateDate: firebase.firestore.Timestamp.fromDate(new Date())
        })
        .catch((err) => {
            console.error(err);
        });

    addNotification(value.writterId, "Commentaire accepté", "Un de vos commentaires a été accepté", 'comment');
}

// -- Make a place verified (isVerified==false) + comment --
export const invalidateComment = (value) => {
    const ref = firebase.firestore().collection("users").doc(value.placeOwner).collection("places").doc(value.placeId).collection("comments").doc(value.id);

    ref.delete().catch((err) => {
        console.error(err);
    });

    addNotification(value.writterId, "Votre commentaire n'a pas été accepté", "Un de vos commentaires n'a pas été accepté et est supprimé car il ne respecte pas les conditions de commentaires, vous pouvez réesayer de mettre un nouveau commentaire.", 'comment');
}



