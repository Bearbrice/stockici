import {useEffect, useState} from "react";

const useGeolocation = () => {
    const [location, setLocation] = useState({
        loaded: false,
        coordinates: {lat: "", lng: ""},
    });

    const onSuccess = (loc) => {
        setLocation({
            loaded: true,
            coordinates: {
                lat: loc.coords.latitude,
                lng: loc.coords.longitude,
            },
        });
    };

    const onError = (error) => {
        setLocation({
            loaded: true,
            coordinates: {
                lat: "46.29240209806783",
                lng: "7.532984415067804",
            },
        });
    };

    useEffect(() => {
        if (!("geolocation" in navigator)) {
            onError({
                code: 0,
                message: "Geolocation not supported",
            });
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }, []);

    return location;
};

export default useGeolocation;