import firebase from "./firebase";
import App from "./App"

describe('Firebase Authentication test', ()=>{
    test('isAuthenticated should return false if not authenticated', () => {
        expect(firebase.auth().currentUser).toBe(null);
    })
})


