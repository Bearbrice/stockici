import React, {useContext, useEffect, useState} from 'react';
import {Page, Text, View, Document, StyleSheet, Image} from '@react-pdf/renderer';
import {PDFViewer} from '@react-pdf/renderer';
import logo from "../StockIci-Logo.png";
import {firebaseAuth} from "../../provider/AuthProvider";
import {SplashScreen} from "../../components/SplashScreen";


// Create styles
const styles = StyleSheet.create({
    page: {
        flexDirection: 'column',
        backgroundColor: '#E4E4E4',
        padding: 60,
        paddingTop: 15
    },
    section: {
        padding: 10,
    },
    header: {
        fontSize: 10,
        color: 'grey',
    },
    signatureDiv: {
        flexDirection: 'row',
        margin: 20
    },
    signature1: {
        flexDirection: 'column',
        marginRight: 100
    },
    signature2: {
        flexDirection: 'column',
    },
    logo: {
        height: 50
    },
    title1: {
        fontSize: 18,
        textAlign: 'center',
        margin: 10
    },
    title2: {
        fontSize: 13,
        margin: 10,
        marginLeft: 0
    },
    title3: {
        fontSize: 10,
        margin: 10,
        marginLeft: 0,
        textDecoration: 'underline',
        textUnderlinePosition: 'under'
    },
    paragraph: {
        textAlign: 'justify',
        fontSize: 10,
        marginBottom: 10
    },
    list: {
        marginLeft: 10,
        textAlign: 'justify',
        fontSize: 10,
    },
    paragraphSignature: {
        fontSize: 10,
        marginBottom: 70
    }

});

function Doc() {

    return (
        <Document>
            <Page size="A4" style={styles.page}>
                <Text style={styles.header}></Text>
                <Text style={styles.title1}>Contrat Stock Ici</Text>
                <View style={styles.section}>
                    <Text style={styles.title2}>Entendons-nous sur les mots </Text>
                    <Text style={styles.paragraph}>Le propriétaire réel des lieux ou la personne physique ou
                        morale
                        dûment habilitée par le propriétaire, et auprès de qui s’effectue la location ou le
                        dépôt, sera
                        désigné par la suite par le terme « Loueur ». </Text>
                    <Text style={styles.paragraph}>La personne réservant un espace complet au titre d’un bail de
                        location standard ou déposant des affaires dans un espace de costockage au titre d’un
                        dépôt sera
                        désignée par la suite par le terme «Stockeur ». </Text>
                    <Text style={styles.paragraph}>L’acceptation des CGU de stock-ici.ch vaut signature du
                        Contrat entre
                        le Loueur et le Stockeur (sauf dans le cas ou un autre modèle de contrat a été choisi
                        par les
                        parties), Contrat qui pourra néanmoins être signé par les parties. </Text>
                    <Text style={styles.paragraph}>Qu’il s’agisse d’une Location ou d’un Dépôt, un contrat est
                        proposé
                        automatiquement afin de simplifier et sécuriser le costockage. Ce contrat pré-rempli est
                        téléchargeable dans l’onglet « Mes emplacements loués » accessible depuis le compte de
                        l’utilisateur, une fois l’espace réservé. </Text>
                    <Text style={styles.paragraph}>Le Loueur doit imprimer le contrat en 2 exemplaires, qui
                        seront
                        signés par les 2 parties au plus tard le premier jour de costockage. Il est important
                        que les
                        informations fournies sur le site soient exactes pour que l'assurance soit activée.
                        N’oubliez
                        pas de conserver le contrat au moins 12 mois après la fin du costockage !</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Clauses du contrat pour une Location ou un Dépôt </Text>
                    <Text style={styles.paragraph}>Chaque Location ou Dépôt est également conclu(e) aux
                        conditions
                        spécifiques de l’annonce qui prévaudront sur le présent contrat de costockage. </Text>
                    <Text style={styles.title3}>Location : </Text>
                    <Text style={styles.paragraph}>Le Stockeur est le seul à pouvoir utiliser l’espace de
                        stockage. En
                        contrepartie de la Location de l’espace de costockage, le Stockeur s’engage à payer un
                        loyer au
                        Fournisseur, via le site stock-ici.ch. Ce loyer inclut toutes charges, impôts et taxes
                        afférant
                        à l’espace de stockage. </Text>
                    <Text style={styles.paragraph}>Seul le Stockeur est responsable de l’espace de stockage et
                        de ses
                        biens costockés. Il s’engage par ailleurs à n’exercer aucune activité commerciale dans
                        l’espace
                        de costockage et ne peut utiliser l’adresse de ce dernier dans une optique commerciale,
                        pour y
                        domicilier une entreprise ou pour y recevoir du courrier. </Text>
                    <Text style={styles.title3}>Dépôt : </Text>
                    <Text style={styles.paragraph}>Le Loueur est responsable de la garde des biens costockés.
                        L’espace
                        peut être partagé avec d’autres stockeurs, ou avec le Loueur. En contrepartie du dépôt
                        de ses
                        affaires dans l’espace de costockage, le Stockeur s’engage à payer au Loueur la
                        redevance
                        convenue, via le site stock-ici.ch. </Text>
                    <Text style={styles.paragraph}>Le Stockeur s’engage à être aussi diligent et prudent dans la
                        garde
                        des biens des Clients que pour celle de ses propres biens afin de conserver les biens
                        costockés
                        dans l’état dans lequel ils ont été déposés. </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Etat de l’espace </Text>
                    <Text style={styles.title3}>Location : </Text>
                    <Text style={styles.paragraph}>Au moment de la signature du Contrat et après avoir adhéré
                        aux
                        Conditions Générales d’Utilisation, le Stockeur reconnaît avoir visité l’espace de
                        costockage.
                        Il s'engage à utiliser l’espace en l’état et à le restituer lors de la fin du costockage
                        dans le
                        même état qu'au jour de la signature du contrat. </Text>
                    <Text style={styles.title3}>Dépôt : </Text>
                    <Text style={styles.paragraph}>Au moment de la signature du contrat et après avoir adhéré
                        aux
                        Conditions Générales d’Utilisation, le Stockeur peut demander à visiter l’espace de
                        costockage.
                        Il s'engage à utiliser l’espace en l’état et renonce à exiger du Loueur un quelconque
                        aménagement de l’espace de costockage. Il s’engage également à ne pas dégrader l’espace
                        de
                        costockage lors du dépôt ou du retrait de ses biens. </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Accès à l’espace </Text>
                    <Text style={styles.title3}>Location : </Text>
                    <Text style={styles.paragraph}>Le Stockeur, avec le Loueur, sont les seuls détenteurs de la
                        clé qui
                        verrouille l’espace mis à sa disposition.
                        L’accès à l’espace personnel de costockage se fera exclusivement aux conditions
                        spécifiques
                        convenues avec le Loueur.
                        Le Loueur n'est pas responsable de l'accès à l’espace par une autre personne que le
                        Stockeur, ni
                        d'éventuelles disparitions de biens que le Stockeur aurait pu constater.
                        Le Loueur peut procéder à l'ouverture de l’espace de costockage sans accord préalable du
                        Stockeur dans les cas suivants : </Text>
                    <Text style={styles.list}>- requête de la Police, des Pompiers, de la Gendarmerie, des
                        Douanes ou
                        sur décision de justice,</Text>
                    <Text style={styles.list}>- en cas de force majeure (Cf. Article Cas de force
                        majeure) </Text>
                    <Text style={styles.title3}>Dépôt : </Text>
                    <Text style={styles.paragraph}>Le Loueur, ou son représentant, est le seul détenteur de la
                        clé de la
                        porte qui verrouille l’espace de costockage. Elle ne pourra être confiée au Stockeur.
                        Loueur et
                        Stockeur pourront signer un document listant l’inventaire simplifié des biens costockés.
                        Cet
                        inventaire sera annexé au contrat et mis à jour dans le temps si nécessaire. </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Dommages causés par le Client </Text>
                    <Text style={styles.title3}>Location : </Text>
                    <Text style={styles.paragraph}>Lors de la signature du contrat, le Stockeur doit se
                        renseigner
                        auprès du Loueur de la limite de surcharge au sol supportée par l’espace de costockage.
                        Si le
                        Stockeur ne respecte pas la limite, il est alors tenu pour seul responsable des dommages
                        occasionnés par le dépassement de la charge autorisée : il assume seul les éventuelles
                        réparations de l’espace. </Text>
                    <Text style={styles.paragraph}>Le Stockeur s'engage également à : </Text>
                    <Text style={styles.list}>- informer le Loueur si un dommage est causé à l’espace de
                        costockage
                        ;</Text>
                    <Text style={styles.list}>- remplacer tout bien qu’il endommage (ou est endommagé par
                        d'autres
                        personnes qu'il fait entrer dans l’espace) ;</Text>
                    <Text style={styles.list}>- réparer tout dommage causé à l’espace s’il en est la cause (sauf
                        en cas
                        de couverture par l’assurance souscrite, notamment en cas de vol ou dégradation et dans
                        la
                        limite de la franchise) ; </Text>
                    <Text style={styles.list}>- rembourser le Loueur de toute somme engagée en raison d'un
                        dommage qu’il
                        aurait causé</Text>
                    <Text style={styles.title3}>Dépôt : </Text>
                    <Text style={styles.paragraph}>Le Stockeur s'engage à : </Text>
                    <Text style={styles.list}>- réparer tout dommage causé à l’espace de costockage où il
                        effectue le
                        dépôt de ses biens s’il en est la cause (sauf en cas de couverture par l’assurance
                        souscrite,
                        dans la limite de la franchise) ; </Text>
                    <Text style={styles.list}>- rembourser le Loueur de toute somme engagée en raison d'un
                        dommage qu’il
                        aurait causé.</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Durée du Contrat de location ou de dépôt</Text>
                    <Text style={styles.title3}>Période : </Text>
                    <Text style={styles.paragraph}>XXXXXXXX</Text>
                    <Text style={styles.title3}>Révision du tarif de costockage : </Text>
                    <Text style={styles.paragraph}>Le propriétaire de l'espace de stockage se réserve le droit
                        de revoir
                        le loyer de l'espace de stockage dès 6 mois de location dans la limite de 20%
                        d'augmentation. La
                        révision s'applique dans les 30 jours suivant la réception du mail via la plateforme
                        Costockage
                        de la part du propriétaire de l'espace de stockage. A défaut de notification écrite, le
                        tarif
                        initial sera maintenu. </Text>
                    <Text style={styles.title3}>Dénonciation du contrat : </Text>
                    <Text style={styles.paragraph}>A tout moment, le Stockeur peut mettre fin au contrat et ce,
                        sans
                        motivation particulière, avec deux semaines de préavis (passée la période minimale de
                        location). La démarche doit se faire via le site stock-ici.ch, depuis la page "Mes
                        Locations".
                    </Text>
                    <Text style={styles.title3}>Défaut de libération de l’espace : </Text>
                    <Text style={styles.paragraph}>A défaut de libération de l’espace de costockage par le
                        Stockeur à la
                        date d’expiration du Contrat, le Loueur pourra prolonger le Contrat pour une durée de 3
                        mois,
                        dans la limite de 36 mois, le loyer ou la redevance étant du(e) mensuellement et
                        d’avance. Une
                        fois régularisée, le Contrat pourra être interrompu avec 15 jours de préavis.
                    </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Modalités de paiement du loyer (Location) ou de la redevance
                        (Dépôt) </Text>
                    <Text style={styles.title3}>Paiement : </Text>
                    <Text style={styles.paragraph}>Le montant prélevé du loyer ou de la redevance représente un
                        mois de
                        costockage (incluant les frais de service mensuels) et sera prélevé deux jours avant la
                        date de
                        début du Contrat, puis versé au Loueur quatre jours après la date de début du Contrat.
                        Par la suite, un paiement sera effectué tous les mois jusqu’à échéance.
                        Le montant fixé dans le Contrat est applicable dès le premier jour de costockage et,
                        sauf
                        communication contraire, aux périodes suivantes. </Text>
                    <Text style={styles.title3}>Dépôt de garantie : </Text>
                    <Text style={styles.paragraph}>Le Contrat de costockage ne donne pas lieu au paiement d’un
                        dépôt de
                        garantie via la plateforme. Si ce dépôt de garantie est mentionnée dans les conditions
                        spécifiques de l’annonce, il pourra être effectué directement entre les parties.
                    </Text>
                    <Text style={styles.title3}>Défaut de paiement </Text>
                    <Text style={styles.paragraph}>Il sera fait application des stipulations de l’article «
                        Résiliation
                        du contrat » ci-après.
                    </Text>
                    <Text style={styles.title3}>Cession / Sous-location : </Text>
                    <Text style={styles.paragraph}>Le Contrat de costockage ne peut être cédé à quiconque par le
                        Stockeur.
                        Toute sous-location ou mise à disposition de l’espace de costockage par le Stockeur à un
                        tiers
                        est strictement interdite.
                    </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Résiliation du contrat </Text>
                    <Text style={styles.title3}>Résiliation si le Client ne respecte pas ses engagements
                        : </Text>
                    <Text style={styles.paragraph}>Le Loueur peut résilier le contrat de façon immédiate et
                        unilatérale,
                        dans les cas où le Stockeur : </Text>
                    <Text style={styles.list}>- ne paie pas le loyer ou la redevance et l’ensemble des sommes
                        dues au
                        titre du Contrat de Location ou de Dépôt, depuis au moins 1 mois </Text>
                    <Text style={styles.list}>- costocke des biens non-autorisés, </Text>
                    <Text style={styles.list}>- ne respecte pas les Conditions Générales présentes dans le
                        contrat. </Text>
                    <Text style={styles.paragraph}>Dans cette hypothèse, le Loueur informera le Stockeur de la
                        résiliation du Contrat et de la possibilité pour le Stockeur de récupérer ses biens. Il
                        envoie
                        alors une mise en demeure au Stockeur via une « Lettre Recommandée avec Avis de
                        Réception » en
                        lui accordant un délai de 10 jours à partir de la réception du récépissé. A défaut pour
                        le
                        Stockeur de récupérer ses biens stockés, il sera réputé avoir renoncé à les reprendre,
                        ils
                        pourront alors être conservés ou mises à la benne par le Loueur, aux frais exclusifs du
                        Stockeur. </Text>
                    <Text style={styles.paragraph}>Alors, tout loyer ou redevance versé(e) pour la période en
                        cours
                        ainsi que la caution éventuelle restent acquises au Stockeur en déduction des sommes
                        dues.

                        Le courrier de mise en demeure est envoyé par stock-ici au propriétaire, afin qu'il
                        l'adresse au
                        locataire. Si le courrier n'est pas adressé par le propriétaire au locataire dans un
                        délai d'un
                        mois après l'envoi par stock-ici, stock-ici cesse les relance et considère le dossier
                        d'impayé
                        comme classé. </Text>
                    <Text style={styles.paragraph}>Tout impayé permet au propriétaire d’interdire l’accès au box
                        / aux
                        affaires tant que la dette n’est pas réglée. </Text>
                    <Text style={styles.title3}>Modification du contrat : </Text>
                    <Text style={styles.paragraph}>Toute modification du Contrat prend la forme d'un avenant
                        rédigé et
                        signé par le Loueur et le Stockeur et devra être notifiée par mail à stock-ici.
                    </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>Fin du contrat </Text>
                    <Text style={styles.paragraph}>La date d'interruption du contrat fixe la date de fin du
                        contrat, en
                        respect du préavis exigé (15 jours, sauf conditions particulières à l'annonce).
                        L'interruption
                        du contrat s'effectue directement sur le site dans l'onglet Messagerie en cliquant sur
                        "Interrompre le contrat". Arrivé au terme du Contrat, le Stockeur doit restituer
                        l’espace vide
                        et propre. A cette date, le Stockeur doit avoir payé l'ensemble des sommes dues au
                        propriétaire
                        ainsi que d'éventuelles pénalités et autres frais. C'est l'interruption du contrat via
                        le site
                        stock-ici uniquement qui fait foi concernant l'interruption des réglements. Sans
                        interruption,
                        le contrat continuera de courir et les paiements seront dus.
                    </Text>
                    <Text style={styles.paragraph}>Dans l'hypothèse où, à l'expiration du Contrat, et si le
                        Loueur a
                        notifié le Stockeur de la non-prolongation du Contrat au plus tard 15 jours avant
                        l’expiration,
                        le Stockeur qui ne déménagerait pas l’ensemble de ses biens sera présumé les avoir
                        abandonnés.
                        Le propriétaire pourra, 10 jours après l’envoi d’une lettre recommandée avec demande
                        d’avis de
                        réception, pourra les conserver ou les mettre à la benne aux frais exclusifs du
                        Stockeur. </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>En cas de force majeure </Text>
                    <Text style={styles.paragraph}>En cas de force majeure et d'extrême urgence, l'emplacement
                        peut être
                        évacué si le propriétaire le juge nécessaire afin de protéger les personnes et les biens
                        costockés. Le Stockeur accepte alors que son espace soit ouvert et que ses biens soient
                        déplacés.
                    </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title2}>En cas de litige </Text>
                    <Text style={styles.paragraph}>En cas de litige, les tribunaux compétents dépendent de la
                        localisation de l’espace de costockage.
                    </Text>
                </View>
                <View style={styles.signatureDiv} debug={true}>
                    <View style={styles.signature1}>
                        <Text style={styles.paragraphSignature}>Signature du propriétaire : </Text>
                        <Text style={styles.paragraph}>Date : ______________________</Text>
                    </View>
                    <View style={styles.signature2}>
                        <Text style={styles.paragraphSignature}>Signature du loueur : </Text>
                        <Text style={styles.paragraph}>Date : ______________________ </Text>
                    </View>
                </View>

            </Page>
        </Document>

    )

}

// Create Document Component
export default function Contract() {

    return (
        <>
            <PDFViewer style={{height: '1000px', width: '100%'}}>
                <Doc/>
            </PDFViewer>
            <style type="text/css">
                {`.navbar {display: none}`}
                {`footer {display: none}`}
                {`body {padding: 0`}
            </style>
        </>
    )
}

