export const objectsData = [
    {
        "type": "bed1",
        "sizem2": "1.7",
        "sizem3" : "0.4",
    },
    {
        "type": "bed2",
        "sizem2": "2.7",
        "sizem3" : "0.6",
    },
    {
        "type": "chair",
        "sizem2": "0.1",
        "sizem3" : "0.06",
    },
    {
        "type": "table",
        "sizem2": "1.62",
        "sizem3" : "1.25",
    },
    {
        "type": "shelf",
        "sizem2": "2.3",
        "sizem3" : "0.9",
    },
    {
        "type": "box",
        "sizem2": "0.25",
        "sizem3" : "0.1",
    },
    {
        "type": "bike",
        "sizem2": "0.75",
        "sizem3" : "0.7",
    },
    {
        "type": "motorbike",
        "sizem2": "0.9",
        "sizem3" : "1.2",
    },
    {
        "type": "car",
        "sizem2": "2",
        "sizem3" : "17.2",
    },
    {
        "type": "bus",
        "sizem2": "31.25",
        "sizem3" : "134.4",
    },
    {
        "type": "truck",
        "sizem2": "6",
        "sizem3" : "1",
    }
]