export const cantonData = [
    {
        "canton": "Valais",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Sion",
        "percentage": "-10"
    },
    {
        "canton": "Vaud",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Lausanne",
        "percentage": "10"
    },
    {
        "canton": "Fribourg",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Fribourg",
        "percentage": "-5"
    },
    {
        "canton": "Jura",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Delémont",
        "percentage": "-12"
    },
    {
        "canton": "Neuchâtel",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Neuchâtel",
        "percentage": "0"
    },
    {
        "canton": "Berne",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Berne",
        "percentage": "5"
    },
    {
        "canton": "Genève",
        "country": "Switzerland",
        "iso2": "CH",
        "capital": "Genève",
        "percentage": "15"
    }
]