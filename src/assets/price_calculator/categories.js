export const categoriesData = [
    {
        "type": "Garage",
        "price": "20"
    },
    {
        "type": "Cave",
        "price": "15"
    },
    {
        "type": "Entrepôt",
        "price": "10"
    },
    {
        "type": "Local",
        "price": "22"
    },
    {
        "type": "Place de parking",
        "price": "18"
    }
]