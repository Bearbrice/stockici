import React from 'react';
/* Import the ThemeContext from ThemeContext.js */
import {ThemeContext} from "../context/ThemeContext"

/* ThemeProvider to keep the Context value */
class ThemeProvider extends React.Component {
    /* Initialize state with a default theme */
    constructor(props) {
        super(props);
        this.state = {theme: (window.localStorage.getItem("theme") ?? 'light')};
    }

    setLocalStorage = (props) => {
        if (props === 'dark') {
            localStorage.setItem("theme", "light")
        } else {
            localStorage.setItem("theme", "dark")
        }
    }


    /* Toggle theme method */
    toggleTheme = () => {
        this.setLocalStorage(this.state.theme);
        this.setState((prevState) => ({
                theme: prevState.theme === 'dark' ? 'light' : 'dark',
            }
        ));

    };

    /*
    Render our App component, wrapped by a ThemeContext Provider:
    The value contains the theme (coming from state) and the
    toggleTheme method allowing consumers of the context to
    update the current theme.
     */
    render() {
        return (
            <ThemeContext.Provider
                value={{theme: this.state.theme, toggleTheme: this.toggleTheme}}
            >
                {this.props.children}
            </ThemeContext.Provider>
        );
    }
}

export default ThemeProvider;
