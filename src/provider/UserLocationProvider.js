import React, {useEffect, useState} from 'react';
import {UserLocationContext} from "../context/UserLocationContext";
import useGeolocation from "../services/useGeolocation";

export const userLoc = React.createContext(undefined);

const UserLocationProvider = (props) => {

    const [latLng, setLatLng] = useState();

    const location = useGeolocation();

    const getUserLocation = () => {
            setLatLng(() => ({
                lat: location.coordinates.lat,
                lng: location.coordinates.lng,
            }));


    }

    useEffect(() => {
        getUserLocation();
    }, [location]);

    return (
        <UserLocationContext.Provider value={{latLng}}>{props.children}</UserLocationContext.Provider>
    );

}

export default UserLocationProvider;

