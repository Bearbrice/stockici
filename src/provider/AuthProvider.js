/**
 * Resources
 * https://dev.to/itnext/user-auth-with-firebase-and-react-1725
 */
import React, {useEffect, useState} from 'react';
import {authMethods} from "../firebase/AuthMethods";
import {auth} from "../firebase";
import {generateUserDocument, modifyUserDocument} from "../services/User/UserController";
import {SplashScreen} from "../components/SplashScreen";

export const firebaseAuth = React.createContext(undefined)

const AuthProvider = (props) => {
    const initState = {email: '', password: ''}
    const [inputs, setInputs] = useState(initState)
    const [errors, setErrors] = useState([])
    const [user, setUser] = useState(null);
    const [initializing, setInitializing] = useState(true);

    // Everytime the user is updated we write it to the DB
    useEffect(() => {
        if (initializing === false) {
            modifyUserDocument(user).then()
        }

    }, [user]);

    // Handle user state changes
    async function onAuthStateChanged(userAuth) {

        //if user is empty you can assign it
        setUser(await generateUserDocument(userAuth));

        if (initializing) setInitializing(false);
    }

    useEffect(() => {
        const subscriber = auth.onAuthStateChanged(userAuth => {
            onAuthStateChanged(userAuth)
        });
        return subscriber; // unsubscribe on unmount
    }, []);

    if (initializing) return <SplashScreen/>;


    const handleDeleteAccount = () => {
        authMethods.deleteAccount(inputs.password, setErrors, user.photoURL)
        setInputs(initState);
    }

    const handleEmailChanging = () => {
        // middle man between firebase and signup
        // calling signup from firebase server
        authMethods.updateEmail(inputs.email, inputs.password, setErrors)
        setInputs(
            prevState => ({
                ...prevState,
                password: ''
            })
        );
    }
    const handleGoogleSignin = () => {
        // middle man between firebase and signup
        // calling signup from firebase server
        authMethods.signInWithGoogle(setErrors)
    }
    const handleSignup = () => {
        // middle man between firebase and signup
        // calling signup from firebase server
        authMethods.signup(inputs.email, inputs.password, setErrors)
        setInputs(initState);
    }
    const handleSignin = () => {
        //changed to handleSingin
        // made signup signin
        authMethods.signin(inputs.email, inputs.password, setErrors)
        setInputs(initState);
    }

    const handleSignout = () => {
        authMethods.signout(setErrors, setUser)
        setInputs(initState);
    }

    const updateUser = (name, value) => {
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const updateUserInfo = async (firstname, lastname, address, NPA, city, iban, bank, completeProfile) => {
        await setUser(prevState => ({
            ...prevState,
            firstname: firstname,
            lastname: lastname,
            address: address,
            NPA: NPA,
            city: city,
            iban: iban,
            bank: bank,
            completeProfile: completeProfile,
        }));

    }

    return (
        <firebaseAuth.Provider
            value={{
                //replaced test with handleSignup
                handleDeleteAccount,
                handleEmailChanging,
                handleGoogleSignin,
                handleSignup,
                handleSignin,
                inputs,
                setInputs,
                errors,
                handleSignout,
                user,
                initializing,
                updateUser,
                updateUserInfo
            }}>
            {props.children}
        </firebaseAuth.Provider>
    );
};

export default AuthProvider;