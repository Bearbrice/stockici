import React from "react";

export const ErrorView = (props) => {

    const errorCode = props.error;
    const errorMessage = props.message;
    console.log(props)
    console.log(props.error)

    return (
        <div style={{textAlign: "center"}}>
            <h1>Erreur {errorCode}</h1>
            <h2>Message: {errorMessage}</h2>
        </div>
    )
};