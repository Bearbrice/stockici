import {Spinner} from "react-bootstrap";
import React from "react";
import {motion} from "framer-motion"
import styled from "styled-components";
import {ThemeBackground, ThemeForeground} from "./Styled-Reusable/ThemeForCSS";

export const Splash = styled.div`
  text-align: center;
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  background-color: inherit;
  color: inherit;
`;

export const Container = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: ${ThemeBackground};
  color: ${ThemeForeground};
`;

export function SplashScreen() {
    return <>
        <Container>
            <Splash>
                <motion.div
                    animate={{scale: 2}}
                    transition={{
                        repeat: Infinity,
                        duration: 4,
                    }}
                >
                    <Spinner variant="success" animation="grow" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>

                    <h1>Chargement...</h1>
                </motion.div>
            </Splash>
        </Container>
    </>
}
