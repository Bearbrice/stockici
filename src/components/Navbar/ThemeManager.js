/* Index component to switch theme */
import React from "react";
import {ThemeContext} from "../../context/ThemeContext";
import {motion} from "framer-motion"

export class ThemeManager extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};

        // This link is necessary in order to enable
        // the use of `this` in the callback function.
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = () => {
        this.setState(state => ({isToggleOn: !state.isToggleOn}));
    }

    toggleTheme = () => {
        this.context.toggleTheme();
    }

    render() {
        return (
            <motion.button
                id="motion-button"
                whileHover={{scale: 1.1}}
                whileTap={{
                    scale: [1, 2, 2, 1, 1],
                }}
                type="button"
                title="Switch Theme"
                onClick={() => {
                    this.handleClick();
                    this.toggleTheme();
                }}
            >
                {/* Remove the current theme value from the button text */}
                {this.state.isToggleOn ? <span>☀️</span> : <span>🌑</span>}
            </motion.button>
        );
    }
}

/* Set the contextType to ThemeContext*/
ThemeManager.contextType = ThemeContext;