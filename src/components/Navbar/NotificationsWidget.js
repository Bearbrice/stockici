import React, {useContext, useEffect, useState} from "react";
import {Badge} from "@material-ui/core";
import NotificationsIcon from '@material-ui/icons/Notifications';
import {Button, OverlayTrigger, Popover, Row} from "react-bootstrap";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import DeleteIcon from '@material-ui/icons/Delete';
import {auth, firestore} from "../../firebase";
import {
    deleteAllNotifications,
    deleteNotification,
    markAllAsRead,
    markAsRead,
} from "../../services/User/NotificationsController";
import {ThemeContext, themes} from "../../context/ThemeContext";
import styled from "styled-components";
import {useHistory} from 'react-router-dom';

export const NotificationDiv = styled.div`
  border-color: darkseagreen;
  border-width: thin;
  border-style: none none solid none;
  margin: auto;

  #clickable {
    cursor: pointer;
  }

  :hover {
    filter: brightness(90%);
  }
`;

const NotificationsWidget = () => {
    const [messages, setMessages] = useState([]);
    // const [loading, setLoading] = useState(true);
    const [unread, setUnread] = useState(null);
    const themeContext = useContext(ThemeContext);
    const history = useHistory();

    // Initialize with listening to our
    // messages collection. The second argument
    // with the empty array makes sure the
    // function only executes once
    useEffect(() => {
        listenForMessages();
    }, []);

    useEffect(() => {
        let count = 0;
        messages.forEach((message) => {
            if (message.read === false) {
                count++;
            }
        })
        setUnread(count);
    }, [messages]);

    // Use firestore to listen for changes within
    // our newly created collection
    const listenForMessages = () => {

        firestore.collection('users').doc(auth.currentUser.uid).collection('notifications').onSnapshot(
            (snapshot) => {
                // Loop through the snapshot and collect
                // the necessary info we need. Then push
                // it into our array
                const allMessages = [];
                snapshot.forEach((doc) => allMessages.push(
                    {
                        id: doc.id,
                        ...doc.data()
                    }
                ));

                allMessages.sort((a, b) => new Date(b.time) - new Date(a.time));

                // Set the collected array as our state
                setMessages(allMessages);
                // setLoading(false);
            },
            (error) => console.error(error)
        );
    };

    function redirect(type, id) {
        // Mark as read
        markAsRead(auth.currentUser.uid, id)

        //Redirect to the corresponding page
        console.log(type);

        switch (type) {
            // The owner receive the demand
            // Receiver: owner
            case 'places':
                history.push("/account/my-places");
                break;
            // The client sent a demand
            // When the offer is validated/refused by the owner
            // When the period of location end
            // Receiver: client
            case 'locations':
                history.push("/account/my-rentals");
                break;
            default:
                break;
        }
    }


    function showNotifications() {
        console.log(messages.length)

        if (messages.length > 0) {

            return (
                <>
                    {messages.map((data, idx) => {
                        let color;
                        if (data.read === true) {
                            color = "white";
                        } else {
                            color = "lightgray";
                        }
                        return (
                            <NotificationDiv
                                key={data.toString()}
                                style={{backgroundColor: color}}>
                                <Row className="m-auto p-0">
                                    <div id="clickable" style={{width: "70%"}} onClick={() => {
                                        redirect(data.type, data.id)
                                    }}>
                                        <p className="m-auto" style={{
                                            // fontSize: "0.7em",
                                            fontFamily: "Sora",
                                            fontWeight: "bold"
                                        }}>{data.title}</p>
                                        {/*<p>{JSON.stringify(data)}</p>*/}
                                        <p className="m-auto" style={{fontSize: "0.9em"}}>{data.message}</p>
                                        <p className="m-auto font-italic"
                                           style={{fontSize: "0.8em"}}>{new Date(data.time).toLocaleString()}</p>
                                    </div>
                                    <div style={{width: "15%"}}>
                                        {data.read === false ?
                                            <Button className="p-0 m-1" variant="outline-success" onClick={() => {
                                                markAsRead(auth.currentUser.uid, data.id)
                                            }}>
                                                <CheckCircleIcon/>
                                            </Button> : null}
                                    </div>

                                    <div style={{width: "15%"}}>
                                        <Button className="p-0 m-1" variant="outline-danger" onClick={() => {
                                            deleteNotification(auth.currentUser.uid, data.id)
                                        }}>
                                            <DeleteIcon/>
                                        </Button>

                                    </div>
                                </Row>
                            </NotificationDiv>
                        );
                    })}
                </>
            );
        } else {
            return (<p>Pas de notification</p>);
        }
    }

    const popoverClickRootClose = (
        <Popover id="popover-contained"
                 style={{
                     maxWidth: "400px",
                     minWidth: "20vw",
                     maxHeight: "50vh",
                     overflow: "auto",
                     scrollbarColor: themes[themeContext.theme].scroll1 + themes[themeContext.theme].scroll2
                 }}>
            <Popover.Title as="h3" className="m-auto"
                           style={{color: "black"}}>
                <Row className="m-auto p-0 align-items-center">
                    <div style={{width: "70%", textAlign: "center"}}>Notifications</div>
                    <div style={{width: "15%"}}>
                        <Button className="p-0 m-1" variant="outline-success" title="Tout mettre comme lu"
                                onClick={() => {
                                    markAllAsRead(auth.currentUser.uid)
                                }}>
                            <DoneAllIcon/>
                        </Button>
                    </div>
                    <div style={{width: "15%"}}>
                        <Button className="p-0 m-1" variant="outline-danger" title="Supprimer toutes les notifications"
                                onClick={() => {
                                    deleteAllNotifications(auth.currentUser.uid).then()
                                }}>
                            <DeleteForeverIcon/>
                        </Button>
                    </div>
                </Row>
            </Popover.Title>
            <Popover.Content>
                {showNotifications()}
            </Popover.Content>
        </Popover>
    );

    return (
        <>
            <OverlayTrigger
                trigger="click"
                rootClose
                placement="bottom"
                overlay={popoverClickRootClose}
            >
                <Button
                    style={{backgroundColor: themes[themeContext.theme].navbar, border: 'none'}}
                    variant="light">
                    <Badge badgeContent={unread} color="error">
                        <NotificationsIcon/>
                    </Badge>
                </Button>
            </OverlayTrigger>
        </>
    );
}

export default NotificationsWidget;