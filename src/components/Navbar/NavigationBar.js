import React, {useContext} from 'react'

import {firebaseAuth} from "../../provider/AuthProvider";
import {Button, Nav, Navbar} from "react-bootstrap";
import {useAlert} from "react-alert";
import StockIciLogo from "../../assets/StockIci-Logo.png"
import {NavBtnLink, NavLink, NavLinkProfile} from "./NavbarElements";
import {ThemeManager} from "./ThemeManager";
import "./NavigationBar.css"
import NotificationsWidget from "./NotificationsWidget";
import {Link} from "react-router-dom";
import {ThemeContext, themes} from "../../context/ThemeContext";

export const NavigationBar = () => {
    const {handleSignout, user} = useContext(firebaseAuth);
    const alert = useAlert();
    const themeContext = useContext(ThemeContext);

    return (
        <>
            <Navbar collapseOnSelect expand="lg" style={{backgroundColor: themes[themeContext.theme].navbar}}
                    fixed="top">
                <Navbar.Brand as={Link} to="/home">
                    <img
                        src={StockIciLogo}
                        width="160"
                        height="40"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto ml-auto">
                        <NavLink to="/home" activeStyle>
                            Accueil
                        </NavLink>
                        <NavLink to="/team" activeStyle>
                            Notre Team
                        </NavLink>
                        <NavLink to="/contact" activeStyle>
                            Contact
                        </NavLink>
                        <NavLink to="/faq" activeStyle>
                            FAQ
                        </NavLink>
                    </Nav>
                    <Nav id="right-navbar">
                        {
                            !user ?
                                null
                                :
                                <NotificationsWidget/>
                        }
                        <ThemeManager/>
                        {/*profile link*/}
                        {
                            !user ?
                                null
                                :
                                <NavLinkProfile id="nav-profile" to="/account/profile">
                                    Profil
                                </NavLinkProfile>
                        }
                        {/*Sign in or sign out button*/}
                        <div id="button-div">
                            {
                                !user ?
                                    <NavBtnLink to="/signin">Se connecter</NavBtnLink>
                                    :
                                    <Button id="button-login"
                                            onClick={() => {
                                                handleSignout();
                                                alert.success('Vous êtes déconnecté')
                                            }}>Se déconnecter
                                    </Button>
                            }
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    );
};