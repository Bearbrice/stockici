import styled from 'styled-components'
import {NavLink as Link} from 'react-router-dom'
import {FaBars} from 'react-icons/fa'

export const Nav = styled.nav`
  background: #e5e5e5;
  height: 80px;
  display: flex;
  justify-content: space-between;
  padding: 0.5rem calcul((100vw - 1000px) / 2);
  z-index: 10;
`

export const NavLinkProfile = styled(Link)`
  color: #0C0C0C;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  cursor: pointer;

  &.active {
    color: #7ed957;
  }

  &:hover {
    color: #7ed957;
    text-decoration: none;
  }
`

export const NavLink = styled(Link)`
  color: #0C0C0C;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  cursor: pointer;

  &.active {
    color: #7ed957;
  }

  &:hover {
    transition: all 0.5s ease-in-out;
    color: #7ed957;
    text-decoration: none;
    font-size: 20px;
  }
`

export const Bars = styled(FaBars)`
  display: none;
  color: #fff;

  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`

export const NavMenu = styled.div`
  display: flex;
  align-items: center;
  margin-right: -24px;

  @media screen and (max-width: 768px) {
    display: none;
  }
`

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  //margin-right: 24px;
  margin-right: 0.5em;

  @media screen and (max-width: 768px) {
    display: none;
`

export const NavBtnLink = styled(Link)`
  border-radius: 4px;
  background: #7ed957;
  padding: 10px 22px;
  color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #498F2B;
    color: #fff;
    text-decoration: none;
  }
`;

export const NavBtnHome = styled(Link)`
  border-radius: 3px;
  width: 50%;
  height: 100px;
  padding-top:30px;
  background: #7ed957;
  margin: 10px;
  color: #FFFFFF;
  border: 5px;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #498F2B;
    color: #fff;
    text-decoration: none;
  }
`;

export const NavBtnCalculator = styled(Link)`
  border-radius: 3px;
  width: 25%;
  height: 100px;
  padding-top:30px;
  background: #7ed957;
  margin: 10px;
  color: #FFFFFF;
  border: 5px;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #498F2B;
    color: #fff;
    text-decoration: none;
  }
`;









