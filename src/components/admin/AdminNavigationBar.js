import React, {useContext, useEffect} from 'react'
import {firebaseAuth} from "../../provider/AuthProvider";
import {Button, Nav, Navbar} from "react-bootstrap";
import {useAlert} from "react-alert";
import StockIciLogo from "../../assets/StockIci-Logo.png"
import {ThemeManager} from "../Navbar/ThemeManager";
import "../Navbar/NavigationBar.css"
import {useHistory} from "react-router-dom";
import {NavBtnLink} from "../Navbar/NavbarElements";

export const AdminNavigationBar = () => {
    const {handleSignout, user} = useContext(firebaseAuth);
    const alert = useAlert();
    const history = useHistory();

    useEffect(() => {
        history.push('/');
    }, []);

    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="light" fixed="top">
                <Navbar.Brand>
                    <img
                        src={StockIciLogo}
                        width="160"
                        height="40"
                        className="d-inline-block align-top"
                        alt="Stock Ici logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto ml-auto">
                        <h4>Console d'administration</h4>
                    </Nav>
                    <Nav id="right-navbar">
                        <ThemeManager/>
                        <div id="button-div">
                            {
                                !user ?
                                    <NavBtnLink to="/signin">Se connecter</NavBtnLink>
                                    :
                                    <Button id="button-login"
                                            onClick={() => {
                                                handleSignout();
                                                alert.success('Vous êtes déconnecté')
                                            }}>Se déconnecter
                                    </Button>
                            }
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

        </>
    );
};