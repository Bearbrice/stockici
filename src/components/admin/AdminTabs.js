import {Col, Row, Tab} from "react-bootstrap";
import {AdminAcceptPlaces} from "./AdminAcceptPlaces";
import {AdminAcceptComments} from "./AdminAcceptComments";
import {CustomTabs} from "../../pages/userManagement/AccountPage";
import React, {useContext} from "react";
import {ThemeContext, themes} from "../../context/ThemeContext";

export function AdminTabs() {
    const themeContext = useContext(ThemeContext);
    return (
        <Row>
            <Col>
                <CustomTabs defaultActiveKey="places"
                            id="controlled-tab-example">
                    <Tab eventKey="places" title="Nouveaux lieux" className="tabAdmin">
                        <AdminAcceptPlaces/>
                    </Tab>
                    <Tab eventKey="comments" title="Commentaires">
                        <AdminAcceptComments/>
                    </Tab>
                    {/*<Tab eventKey="requests" title="Suivi des demandes">*/}
                    {/*    <AdminFollowRequests/>*/}
                    {/*</Tab>*/}
                </CustomTabs>
            </Col>
        </Row>
    );
}