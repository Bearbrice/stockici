import React, {useContext, useEffect, useState} from "react";
import {getPendingPlaces, invalidatePlace, verifyPlace} from "../../services/PlaceController2";
import DataTable from 'react-data-table-component';
import {Button, Col, Form, Modal, Spinner} from "react-bootstrap";
import Zoom from 'react-medium-image-zoom'
import 'react-medium-image-zoom/dist/styles.css'
import {useAlert} from "react-alert";
import {ThemeContext, themes} from "../../context/ThemeContext";

export function AdminAcceptPlaces() {
    const alert = useAlert();
    const themeContext = useContext(ThemeContext);
    const [places, setPlaces] = useState([]);
    const [pending, setPending] = useState(true);
    //Modal
    const [show, setShow] = useState(false);
    const [reason, setReason] = useState("");
    const [errorReason, setErrorReason] = useState(false);
    const [place, setPlace] = useState("");

    const customStyles = {
        header: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
        headRow: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
        headCells: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
        cells: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
    };

    useEffect(() => {
        getPendingPlaces().then(res => {
            setPlaces(res);
            setPending(false)
        });
    }, []);

    function reload() {
        getPendingPlaces().then(res => {
            setPending(true)
            setPlaces(res);
            setPending(false)
        });
    }

    const columns = [
        {
            selector: 'type',
            name: 'Type',
            sortable: true
        },
        {
            selector: 'description',
            name: 'Description',
            // grow: '2'
        },
        {
            selector: 'comment',
            name: 'Commentaire'
        },
        {
            selector: 'areaAvailable',
            name: 'Surface'
        },
        {
            selector: 'hasAccessCar',
            name: 'Accès en voiture',
            format: (value) => (
                <span>
                    {value.hasAccessCar === "true" ? '✔️' : '❌'}
                </span>
            )
        },
        {
            selector: 'hasElevator',
            name: 'Ascenceur',
            format: (value) => (
                <span>
                    {value.hasElevator === "true" ? '✔️' : '❌'}
                </span>
            )
        },
        {
            selector: 'isPro',
            name: 'Professionnel',
            format: (value) => (
                <span>
                    {value.isPro === "true" ? '✔️' : '❌'}
                </span>
            )
        },
        {
            selector: 'isSecured',
            name: 'Sécurisé',
            format: (value) => (
                <span>
                    {value.isSecured === "true" ? '✔️' : '❌'}
                </span>
            )
        },
        {
            selector: 'isVerified',
            name: 'Verified',
            format: (value) => (
                <span>
                    {value.isVerified === "true" ? '✔️' : '❌'}
                </span>
            )
        },
        {
            name: 'Valider',
            cell: (value) => <Button variant="outline-success" color="primary"
                                     onClick={() => validatePlace(value)}><span>✔️</span></Button>,
            button: true,
        },
        {
            name: 'Refuser',
            cell: (value) => <Button variant="outline-danger" color="primary"
                                     onClick={() => refusePlace(value)}><span>❌</span></Button>,
            button: true,
        }
    ];

    const validatePlace = (value) => {

        // Make as verified in database
        verifyPlace(value.owner, value.id);

        // Remove the element from the data table
        const array = [...places]; // separate copy of the array
        const find = array.find((element) => element.id === value.id)
        const index = array.indexOf(find)

        if (index !== -1) {
            array.splice(index, 1);
            setPlaces(array);
        }

        alert.success("L'emplacement a été vérifié");
    }

    const refusePlace = (value) => {
        setReason(value.comment);
        setPlace(value);
        setShow(true);
    }

    // The row data is composed into your custom expandable component via the data prop
    const ExpandableComponent = ({data}) => {

            return (
                <div className="text-center " style={{backgroundColor: themes[themeContext.theme].cardContent}}>
                    {data.comment ?<p style={{color: "red", fontSize: "medium"}}><u>Actuellement refusé:</u> {data.comment}</p> : null}
                    <p>{data.titleOffer}</p>
                    {data.description ?
                        <>
                            <h6><b>Description : </b>{data.description}</h6>
                        </>
                        : <h6>Pas de description disponible</h6>}
                    {data.photoURLs.map(item => {
                        return (
                            <>

                                <Zoom>
                                    <img
                                        alt="Lieu"
                                        src={item}
                                        width="320"
                                        height="200"
                                    />
                                </Zoom>
                            </>
                        );
                    })
                    }
                </div>
            )
        }
    ;

    const Spin = () => {
        return (
            <Spinner variant="success" animation="grow" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        );
    }

    const BasicTableExpanderDisabled = () => {
            const data = places.map(item => {
                let disabled = false;
                // if (Number(item.year) < 2000) {
                //     disabled = true;
                // }
                return {...item, disabled};
            });
            return (
                <DataTable
                    title="Emplacements en attente d'approbation"
                    columns={columns}
                    data={data}
                    expandableRows
                    expandableRowDisabled={row => row.disabled}
                    highlightOnHover
                    defaultSortField="name"
                    expandableRowsComponent={<ExpandableComponent/>}
                    progressPending={pending}
                    progressComponent={<Spin/>}
                    noDataComponent="Aucune nouvelle place en attente d'approbation"
                    customStyles={customStyles}
                />
            );
        }
    ;

    /* --- MODAL --- */
    const handleClose = () => {
        setShow(false);
    }

    const handleValidate = async () => {
        if (reason === "") {
            setErrorReason(true);
            return;
        } else {
            setErrorReason(false);
        }


        invalidatePlace(place.owner, place.id, reason)

        // // Make as verified in database
        // verifyPlace(value.owner, value.id);

        // Remove the element from the data table
        // const array = [...places]; // separate copy of the array
        // const find = array.find((element) => element.id = place.id)
        // const index = array.indexOf(find)
        //
        // if (index !== -1) {
        //     array.splice(index, 1);
        //     setPlaces(array);
        // }

        setShow(false);
        reload();
        alert.info("L'emplacement a été refusé");
    }

    const handleChange = (event) => {
        // this.setState({value: event.target.value});
        setReason(event.target.value);
    }

    return (
        <>
            <BasicTableExpanderDisabled/>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Refuser l'emplacement</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Row className="align-items-center" style={{
                        alignItems: "center",
                        justifyContent: "center", marginTop: "40px"
                    }}>
                        <h4><b>Important</b></h4>
                        <p>Veuillez indiquer la raison de la suppression</p>
                        <h4 style={{color: "orangered"}}><b>Attention</b></h4>
                        <p style={{color: "red"}}>Le client <u>prendra connaissance</u> du message, donc écrivez quelque
                            chose de pertinent avec une raison valable.</p>

                        <Col xs="auto">
                            <Form.Control
                                style={{textAlign: "center"}}
                                as="textarea"
                                name="reason"
                                value={reason}
                                placeholder="Raison"
                                onChange={handleChange}
                            />
                            {errorReason ? <p style={{color: 'red'}}>Veuillez indiquer une raison</p> : null}
                        </Col>
                    </Form.Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Annuler
                    </Button>
                    <Button variant="success" onClick={handleValidate}>
                        Confirmer
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}