import React, {useContext, useEffect, useState} from "react";
import {getPendingComments, invalidateComment, verifyComment} from "../../services/PlaceController2";
import {Button} from "react-bootstrap";
import DataTable from "react-data-table-component";
import {useAlert} from "react-alert";
import {ThemeContext, themes} from "../../context/ThemeContext";



export function AdminAcceptComments() {
    const alert = useAlert();
    const themeContext = useContext(ThemeContext);

    const [comments, setComments] = useState([]);
    const [pending, setPending] = useState(true);
    // const [loading2, setLoading2] = useState(false);


    const customStyles = {
        header: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
        headRow: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
        headCells: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
        cells: {
            style: {
                backgroundColor: themes[themeContext.theme].cardContent,
            },
        },
    };

    useEffect(() => {
        reload();
    }, []);

    function reload() {
        getPendingComments().then(res => setComments(res));

        const timer = setTimeout(() => {
            setPending(true);
        }, 1000);

        return () => clearTimeout(timer);
    }


    const columns = [
        {
            selector: 'writterId',
            name: 'Utilisateur'
        },
        {
            selector: 'comment',
            name: 'Commentaire'
        },
        {
            selector: 'writtenDate',
            name: 'Écrit le'
        },
        {
            selector: 'isValid',
            name: 'Valider',
            format: (value) => (
                <span>
                    {value.isValid === "true" ? '✔️' : '❌'}
                </span>
            )
        },
        {
            name: 'Valider',
            cell: (value) => <Button variant="outline-success" color="primary"
                                     onClick={() => validateComment(value)}><span>✔️</span></Button>,
            button: true,
        },
        {
            name: 'Refuser',
            cell: (value) => <Button variant="outline-danger" color="primary"
                                     onClick={() => refuseComment(value)}><span>❌</span></Button>,
            button: true,
        }
    ];

    const validateComment = (value) => {
        console.log(value)

        verifyComment(value);

        // Remove the element from the data table
        const array = [...comments]; // separate copy of the array
        const find = array.find((element) => element.id === value.id)
        const index = array.indexOf(find)

        if (index !== -1) {
            array.splice(index, 1);
            setComments(array);
        }

        alert.success('Le commentaire a été accepté')
    }

    const refuseComment = (value) => {
        console.log(value)

        invalidateComment(value);

        reload();

        alert.info('Le commentaire a été refusé et supprimé')
    }


    const BasicTableExpanderDisabled = () => {
        const data = comments.map(item => {
            let disabled = false;
            return {...item, disabled};
        });
        return (
            <DataTable
                title="Commentaires en attente d'approbation"
                columns={columns}
                data={data}
                highlightOnHover
                defaultSortField="name"
                noDataComponent="Aucun nouveau commentaire en attente d'approbation"
                customStyles={customStyles}
            />
        );
    }

    return (
        <BasicTableExpanderDisabled/>
    );

}