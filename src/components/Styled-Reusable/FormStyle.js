import styled from "styled-components";
import {ThemeForeground} from "./ThemeForCSS";

export const Container = styled.div`
  label {
    color: ${ThemeForeground};
    border-radius: 10px;
    opacity: 0.9;
    padding-left: 5%;
    padding-right: 5%;
    margin-top: 15px;
    text-align: center;
  }

  .has-error {
    border: 2px solid #FF6565;
  }

  .error-message {
    color: #FF6565;
    padding: .5em .2em;
    height: 1em;
    position: absolute;
    font-size: .5em;
    text-align: center;
  }

  h2 {
    text-align: center;
    font-size: 1em;
    font-weight: 400;
    padding: 0.1em;
    border-radius: 4px;
    opacity: 0.95;
    background: #282c34;
    margin: 2em 0.8em 0.8em;
  }

  sub {
    color: darkred;
    background: grey;
    text-align: center;
  }

  .col {
    text-align: center;
  }

  input[type=checkbox] {
    border-radius: 50%;

    /* Double-sized Checkboxes */
    -ms-transform: scale(2); /* IE */
    -moz-transform: scale(2); /* FF */
    -webkit-transform: scale(2); /* Safari and Chrome */
    -o-transform: scale(2); /* Opera */
    padding: 10px;
  }
`;

