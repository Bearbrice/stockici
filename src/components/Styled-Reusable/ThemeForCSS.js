import {useContext} from "react";
import {ThemeContext, themes} from "../../context/ThemeContext";

export const ThemeForeground = () => {
    const themeContext = useContext(ThemeContext);
    return themes[themeContext.theme].foreground
}

export const ThemeBackground = () => {
    const themeContext = useContext(ThemeContext);
    return themes[themeContext.theme].background
}
