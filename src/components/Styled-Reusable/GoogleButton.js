import styled from "styled-components";
import GoogleButton from "react-google-button";
import {ThemeBackground, ThemeForeground} from "./ThemeForCSS";
import {ThemeContext, themes} from "../../context/ThemeContext";
import React, {useContext} from "react";
import {firebaseAuth} from "../../provider/AuthProvider";

export const ButtonGoogle = (props) => {
    const themeContext = useContext(ThemeContext);
    const {handleGoogleSignin} = useContext(firebaseAuth)

    return (
        <GButton className="ml-auto mr-auto"
                 onClick={handleGoogleSignin}
                 label={props.label}
                 style={{
                     margin: "1rem",
                     backgroundColor: themes[themeContext.theme].foreground,
                     color: themes[themeContext.theme].background
                 }}
        />
    )
};


const GButton = styled(GoogleButton)`
  svg {
    color: ${ThemeForeground};
    background-color: ${ThemeBackground};
  }

  span {
    font-size: small;
  }

`