import React, {useContext} from "react";
import {ThemeContext, themes} from "../context/ThemeContext";
import {Card} from "react-bootstrap";
import {NavBtnLink} from "./Navbar/NavbarElements";

export function Error404() {

    const themeContext = useContext(ThemeContext);

    return (
        <Card
            bg={themeContext.theme}
            text={themes[themeContext.theme].background}
            className="text-center"
        >
            <Card.Header>Erreur 404</Card.Header>
            <Card.Body>
                <Card.Text className="mb-4">
                    La page demandée n'existe pas ou a été déplacée ailleurs.
                </Card.Text>
                <NavBtnLink className="small" to="/home">Retour à l'accueil</NavBtnLink>
            </Card.Body>
        </Card>
    );
}