import React from 'react';

/* ThemeContext that will be used by the App */
export const ThemeContext = React.createContext({
    theme: null,
    toggleTheme: () => {
    },
});

/* Themes to use in the App */
export const themes = {
    light: {
        background: '#FFFFFF',
        foreground: '#0C0C0C',
        navbar: '#F7F7F6',
        cardContent: '#F7F7F6',
        cardShadowTitle: '5px 2px 3px #7ED957',
        cardShadow: '4px 2px 2px #9E8478',
        scroll1: '#072b07',
        scroll2: '#b2b8b2',
        green: '#7ED957',

    },
    dark: {
        background: '#282c34',
        foreground: 'white',
        navbar: '#525252',
        cardContent: '#DEDEDE',
        cardShadowTitle: '5px 2px 3px #7ED957',
        cardShadow: '4px 2px 2px #9E9E9E',
        scroll1: '#004200',
        scroll2: '#282c34',
        fab: '#004200',
    },
};
