import React from 'react';

//Sierre by default
export const loc = {
    coordinates: {
        lat: 46.292396387433165,
        lng: 7.532874353989861,
    },
};

export const UserLocationContext = React.createContext(
    loc.coordinates
);
