import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';

import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter} from "react-router-dom";
import AuthProvider from "./provider/AuthProvider";
import AlertTemplate from "react-alert-template-basic";
import {positions, Provider, transitions} from "react-alert";
import ThemeProvider from "./provider/ThemeProvider";
import UserLocationProvider from "./provider/UserLocationProvider";

const options = {
    timeout: 5000,
    position: positions.TOP_CENTER,
    transition: transitions.SCALE,
};

export const Application = () => (
    <ThemeProvider>
        <Provider template={AlertTemplate} {...options} containerStyle={{marginTop: "5em"}}>
            <AuthProvider>
                <UserLocationProvider>
                    <BrowserRouter>
                        <App/>
                    </BrowserRouter>
                </UserLocationProvider>
            </AuthProvider>
        </Provider>
    </ThemeProvider>

);

ReactDOM.render(
    <React.StrictMode>
        <Application/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
